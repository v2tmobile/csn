//
//  V2TTabBarViewController.swift
//  ChiaSeNhac
//
//  Created by Vũ Trung Thành on 1/9/16.
//  Copyright © 2016 V2T Multimedia. All rights reserved.
//

import UIKit
import FontAwesome_swift
import SwiftyJSON
import Alamofire
import RealmSwift
import SSSnackbar

class V2TTabBarViewController: UITabBarController, UIWebViewDelegate {
    
    var popUpVC = PopUpViewController(size: CGSize(width: 0.9, height: 0.5))

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.registerObserver(CSN.Notification.showDownloadMusic, object: nil, queue: nil) { (noti) -> Void in
            let music = noti.object as! Music
            CSN.showDownloadMusicQualityOptions(music, viewController: self)
        }
        
        self.registerObserver(CSN.Notification.showDownloadVideo, object: nil, queue: nil) { (noti) -> Void in
            let music = noti.object as! Music
            CSN.showDownloadVideoQualityOptions(music, viewController: self)
        }
        
        self.registerObserver(CSN.Notification.showExtraOptionMusicItem, object: nil, queue: nil) { (music) -> Void in
            if let music = music.object as? Music{
                self.showExtraOptionMusicItem(music)
            }
        }
        self.registerObserver(CSN.Notification.showExtraOptionVideoItem, object: nil, queue: nil) { (music) -> Void in
            if let music = music.object as? Music{
                self.showExtraOptionVideoItem(music)
            }
        }
        
        self.registerObserver(CSN.Notification.downloadDone, object: nil, queue: nil) { (noti) -> Void in
            let music = noti.object as! Music
            let snackBar = SSSnackbar.init(message: NSLocalizedString("Đã tải xong " + music.musicTitle, comment: ""), actionText: NSLocalizedString("OK", comment: ""), duration: 2.5, actionBlock: nil, dismissalBlock: nil)
            snackBar.show()
        }
        
        if let json = CSN.Setting.globalJSON {
            if json["update"].boolValue {
                if let version = NSBundle.mainBundle().infoDictionary?["CFBundleShortVersionString"] as? String {
                    if version != json["version"].stringValue {
                        self.showUpdateApp()
                    }
                }
            }
            if json["promote_app"]["status"].boolValue{
                self.showPromote()
            }
        }
        
        self.registerObserver(CSN.Notification.getAppConfigDone, object: nil, queue: nil) { (noti) -> Void in
            if let json = CSN.Setting.globalJSON {
                if json["update"].boolValue {
                    if let version = NSBundle.mainBundle().infoDictionary?["CFBundleShortVersionString"] as? String {
                        if version != json["version"].stringValue {
                            self.showUpdateApp()
                        }
                    }
                }
                if json["promote_app"]["status"].boolValue{
                    self.showPromote()
                }
            }
        }
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func showExtraOptionMusicItem(music: Music) {
        let iconSize = CGSize(width: 26, height: 26)
        var items = [
            BottomMenuViewItem(icon: UIImage.fontAwesomeIconWithName(FontAwesome.Play, textColor: UIColor.grayColor(), size: iconSize), title: NSLocalizedString("Chơi ngay", comment: ""), selectedAction: { (Void) -> Void in
                CSN.playMusic(music)
            }),
            BottomMenuViewItem(icon: UIImage.fontAwesomeIconWithName(FontAwesome.ListOL, textColor: UIColor.grayColor(), size: iconSize), title: NSLocalizedString("Thêm vào danh sách nghe", comment: ""), selectedAction: { (Void) -> Void in
                CSN.musicPlayer.addItemToQueue(music)
                NSNotificationCenter.defaultCenter().postNotificationName(CSN.Notification.updateTrackList, object: music)
            }),
            BottomMenuViewItem(icon: UIImage.fontAwesomeIconWithName(FontAwesome.Heart, textColor: UIColor.grayColor(), size: iconSize), title: NSLocalizedString("Thêm vào yêu thích", comment: ""), selectedAction: { (Void) -> Void in
                CSN.saveFavorite(music)
            }),
            BottomMenuViewItem(icon: UIImage.fontAwesomeIconWithName(FontAwesome.ShareAlt, textColor: UIColor.grayColor(), size: iconSize), title: NSLocalizedString("Chia sẻ...", comment: ""), selectedAction: { (Void) -> Void in
                self.share(music)
            }),
            BottomMenuViewItem(icon: UIImage.fontAwesomeIconWithName(FontAwesome.AngleDown, textColor: UIColor.grayColor(), size: iconSize), title: NSLocalizedString("Thôi", comment: ""), selectedAction: { (Void) -> Void in
                //
            })
        ]
        
        if CSN.Setting.isShowDownloadFunction {
            items.insert(BottomMenuViewItem(icon: UIImage.fontAwesomeIconWithName(FontAwesome.CloudDownload, textColor: UIColor.grayColor(), size: iconSize), title: NSLocalizedString("Tải về", comment: ""), selectedAction: { (Void) -> Void in
                if let musicWidth = music.musicWidth {
                    if Int(musicWidth) != 0 {
                        CSN.showDownloadVideoQualityOptions(music, viewController: self)
                    }
                    else {
                        CSN.showDownloadMusicQualityOptions(music, viewController: self)
                    }
                }
            }), atIndex: 3)
        }
        
        let bottomMenu = BottomMenuView(items: items, didSelectedHandler: nil)
        bottomMenu.showInViewController(self)
    }
    
    func showExtraOptionVideoItem(video: Music) {
        let iconSize = CGSize(width: 26, height: 26)
        var items = [
            BottomMenuViewItem(icon: UIImage.fontAwesomeIconWithName(FontAwesome.Play, textColor: UIColor.grayColor(), size: iconSize), title: NSLocalizedString("Chơi ngay", comment: ""), selectedAction: { (Void) -> Void in
                let storyboard = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle())
                let videoDetailsVC: VideoDetailsViewController! = storyboard.instantiateViewControllerWithIdentifier("videoDetails") as? VideoDetailsViewController
                videoDetailsVC?.music = video
                videoDetailsVC.showInViewController(self.selectedViewController!)
            }),
            BottomMenuViewItem(icon: UIImage.fontAwesomeIconWithName(FontAwesome.Heart, textColor: UIColor.grayColor(), size: iconSize), title: NSLocalizedString("Thêm vào yêu thích", comment: ""), selectedAction: { (Void) -> Void in
                CSN.saveFavorite(video)
            }),
            BottomMenuViewItem(icon: UIImage.fontAwesomeIconWithName(FontAwesome.ShareAlt, textColor: UIColor.grayColor(), size: iconSize), title: NSLocalizedString("Chia sẻ...", comment: ""), selectedAction: { (Void) -> Void in
                self.share(video)
            }),
            BottomMenuViewItem(icon: UIImage.fontAwesomeIconWithName(FontAwesome.AngleDown, textColor: UIColor.grayColor(), size: iconSize), title: NSLocalizedString("Thôi", comment: ""), selectedAction: { (Void) -> Void in
                //
            })
        ]
        
        if (CSN.Setting.isShowDownloadFunction) {
            items.insert(BottomMenuViewItem(icon: UIImage.fontAwesomeIconWithName(FontAwesome.CloudDownload, textColor: UIColor.grayColor(), size: iconSize), title: NSLocalizedString("Tải về", comment: ""), selectedAction: { (Void) -> Void in
                CSN.showDownloadVideoQualityOptions(video, viewController: self)
            }), atIndex: 2)
        }
        let bottomMenu = BottomMenuView(items: items, didSelectedHandler: nil)
        bottomMenu.showInViewController(self)
    }
    
    func share(music: Music) {
        CSN.API.getDetailsMusic(music: music, success: { (result) -> Void in
            if result["music_info"] != nil {
                let music = Music.fromDictionary(result["music_info"].dictionaryObject!)
                let shareString = NSLocalizedString("Mình đang nghe bài hát: ", comment: "") + CSN.Setting.shareVideoLink + music.fullUrl
                let activityViewController = UIActivityViewController(activityItems: [shareString], applicationActivities: nil)
                let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
                appDelegate.window?.rootViewController!.presentViewController(activityViewController, animated: true, completion: {})
            }
            })
            { (error) -> Void in
                //
        }
    }
    
    func showPromote() {
        
        popUpVC = PopUpViewController(size: CGSize(width: 0.9*UIScreen.mainScreen().bounds.width, height: 0.5*UIScreen.mainScreen().bounds.height ))
        popUpVC.viewContainer.backgroundColor = UIColor.clearColor()
        let webView = UIWebView()
        webView.backgroundColor = UIColor.clearColor()
        webView.translatesAutoresizingMaskIntoConstraints = false
        popUpVC.viewContainer.addSubview(webView)
        popUpVC.viewContainer.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|[webView]|", options: NSLayoutFormatOptions(), metrics: nil, views: ["webView": webView]))
        popUpVC.viewContainer.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:|[webView]|", options: NSLayoutFormatOptions(), metrics: nil, views: ["webView": webView]))
        let url = NSURL (string: CSN.Setting.globalJSON!["promote_app"]["link_ads"].stringValue);
        let requestObj = NSURLRequest(URL: url!);
        webView.loadRequest(requestObj);
        webView.delegate = self

    }
    
    func showUpdateApp() {
        popUpVC = PopUpViewController(size: CGSize(width: 0.9*UIScreen.mainScreen().bounds.width, height: 0.5*UIScreen.mainScreen().bounds.height))
        popUpVC.viewContainer.backgroundColor = UIColor.clearColor()
        let webView = UIWebView()
        webView.backgroundColor = UIColor.clearColor()
        webView.translatesAutoresizingMaskIntoConstraints = false
        popUpVC.viewContainer.addSubview(webView)
        popUpVC.viewContainer.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|[webView]|", options: NSLayoutFormatOptions(), metrics: nil, views: ["webView": webView]))
        popUpVC.viewContainer.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:|[webView]|", options: NSLayoutFormatOptions(), metrics: nil, views: ["webView": webView]))
        let url = NSURL (string: CSN.Setting.globalJSON!["update_banner"].stringValue);
        let requestObj = NSURLRequest(URL: url!);
        webView.loadRequest(requestObj);
        webView.delegate = self
        
    }
    
    func webViewDidFinishLoad(webView: UIWebView) {
        popUpVC.showInViewController(self)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
