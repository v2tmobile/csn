//
//  V2TTableView.swift
//  ChiaSeNhac
//
//  Created by Vũ Trung Thành on 12/5/15.
//  Copyright © 2015 V2T Multimedia. All rights reserved.
//

import UIKit

class V2TTableView: UITableView {
    
    @IBInspectable var headerViewHeight: CGFloat = 0 {
        didSet {
            self.layoutSubviews()
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        if let headerView = self.tableHeaderView {
            var headerFrame = headerView.frame
            
            // If we don't have this check, viewDidLayoutSubviews() will get
            // repeatedly, causing the app to hang.
            if headerViewHeight != headerFrame.size.height {
                headerFrame.size.height = headerViewHeight
                headerView.frame = headerFrame
                self.tableHeaderView = headerView
            }
        }
    }
    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */

}
