//
//  SearchViewController.swift
//  ChiaSeNhac
//
//  Created by Vũ Trung Thành on 2/15/16.
//  Copyright © 2016 V2T Multimedia. All rights reserved.
//

import UIKit
import CarbonKit
import SwiftyJSON

class SearchViewController: V2TViewController {
    
    @IBOutlet weak var resultView: UIView!
    @IBOutlet weak var searchOptionsSegment: UISegmentedControl!
    
    var query: String = ""
    
    let tabItems = [
        NSLocalizedString("Bài hát", comment: ""),
        NSLocalizedString("Ca sĩ", comment: ""),
        NSLocalizedString("Video", comment: ""),
        NSLocalizedString("Album", comment: "")
    ]

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let tabSwipeNavigation = CarbonTabSwipeNavigation(items: tabItems, delegate: self)
        tabSwipeNavigation.view.backgroundColor = UIColor(rgba: "#ffffff")
        tabSwipeNavigation.setIndicatorColor(UIColor(rgba: "#d32f2f"))
        tabSwipeNavigation.setSelectedColor(UIColor(rgba: "#d32f2f"))
        tabSwipeNavigation.setNormalColor(UIColor(rgba: "#d32f2f"))
        switch UIDevice.currentDevice().userInterfaceIdiom {
        case .Phone:
            // It's an iPhone
            tabSwipeNavigation.setTabBarHeight(32)
        case .Pad:
            // It's an iPad
            tabSwipeNavigation.setTabBarHeight(44)
        default:
            // Uh, oh! What could it be?
            tabSwipeNavigation.setTabBarHeight(32)
        }
        tabSwipeNavigation.carbonSegmentedControl?.backgroundColor = UIColor(rgba: "#ffffff")
        tabSwipeNavigation.carbonSegmentedControl?.setWidth(UIScreen.mainScreen().bounds.width/4, forSegmentAtIndex: 0)
        tabSwipeNavigation.carbonSegmentedControl?.setWidth(UIScreen.mainScreen().bounds.width/4, forSegmentAtIndex: 1)
        tabSwipeNavigation.carbonSegmentedControl?.setWidth(UIScreen.mainScreen().bounds.width/4, forSegmentAtIndex: 2)
        tabSwipeNavigation.carbonSegmentedControl?.setWidth(UIScreen.mainScreen().bounds.width/4, forSegmentAtIndex: 3)
        tabSwipeNavigation.setIndicatorHeight(2)
        tabSwipeNavigation.insertIntoRootViewController(self, andTargetView: self.view)

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - CarbonTabSwipeNavigationDelegate
    
    func carbonTabSwipeNavigation(carbonTabSwipeNavigation: CarbonTabSwipeNavigation, viewControllerAtIndex index: UInt) -> UIViewController {
        switch index {
        case 0:
            return searchSongListVC()
        case 1:
            return searchArtistListVC()
        case 2:
            return searchVideoListVC()
        case 3:
            return searchAlbumListVC(query)
        default:
            break
        }
        return UIViewController()
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    private func searchSongListVC() -> UIViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle())
        let songListVC = storyboard.instantiateViewControllerWithIdentifier("SongList") as! SongsListTableViewController
        
        songListVC.isOnline = true
        songListVC.showCategoryButton = false
        songListVC.showDetailButton = false

        songListVC.registerObserver(CSN.Notification.searchButtonDidTapped, object: nil, queue: nil) { (noti) -> Void in
            self.loadSongData(songListVC)
        }
        
        songListVC.loadMoreHandler = {
            songListVC.currentPage++
            songListVC.swipeRefreshControl.startRefreshing()
            CSN.API.search(query: self.query, category: .Song, page: songListVC.currentPage, loadCache: true, success: { (result) -> Void in
                if let musicArray = result["music_list"].array {
                    for item: JSON in musicArray {
                        songListVC.songsList.append(Music.fromDictionary(item.dictionaryObject!))
                    }
                    songListVC.tableView.reloadData()
                }
                songListVC.swipeRefreshControl.endRefreshing()
                }, failure: { (error) -> Void in
                    //
            })
        }
        
        songListVC.reloadHandler = {
            self.loadSongData(songListVC)
            songListVC.swipeRefreshControl.endRefreshing()
            sharedV2TAds.showInterstitialAdsFromRootViewController(songListVC)
        }
        
        return songListVC as UIViewController
    }
    
    func loadSongData(songListVC: SongsListTableViewController) {
        songListVC.swipeRefreshControl.startRefreshing()
        CSN.API.search(query: query, category: .Song, page: 1, loadCache: false, success: { (result) -> Void in
            songListVC.songsList = []
            if let musicArray = result["music_list"].array {
                for item: JSON in musicArray {
                    songListVC.songsList.append(Music.fromDictionary(item.dictionaryObject!))
                }
            }
            songListVC.tableView.reloadData()
            songListVC.swipeRefreshControl.endRefreshing()
            }, failure: { (error) -> Void in
                //
        })
    }
    
    private func searchVideoListVC() -> UIViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle())
        let videoListVC = storyboard.instantiateViewControllerWithIdentifier("VideoList") as! VideosListTableViewController
        
        videoListVC.isOnline = true
        videoListVC.showCategoryButton = false
        videoListVC.showDetailButton = false
        
        if query != "" {
            videoListVC.viewDidLoadHandler = {
                self.loadVideoData(videoListVC)
            }
        }
        
        videoListVC.registerObserver(CSN.Notification.searchButtonDidTapped, object: nil, queue: nil) { (noti) -> Void in
            self.loadVideoData(videoListVC)
        }
        
        videoListVC.reloadHandler = {
            self.loadVideoData(videoListVC)
            videoListVC.swipeRefreshControl.endRefreshing()
            sharedV2TAds.showInterstitialAdsFromRootViewController(videoListVC)
        }
        
        videoListVC.loadMoreHandler = {
            videoListVC.currentPage++
            videoListVC.swipeRefreshControl.startRefreshing()
            CSN.API.search(query: self.query, category: .Video, page: videoListVC.currentPage, loadCache: true, success: { (result) -> Void in
                if let musicArray = result["music_list"].array {
                    for item: JSON in musicArray {
                        videoListVC.videosList.append(Music.fromDictionary(item.dictionaryObject!))
                    }
                }
                videoListVC.tableView.reloadData()
                videoListVC.swipeRefreshControl.endRefreshing()
                }, failure: { (error) -> Void in
                    //
            })
        }

        return videoListVC as UIViewController
    }

    func loadVideoData(videoListVC: VideosListTableViewController) {
        videoListVC.swipeRefreshControl.startRefreshing()
        CSN.API.search(query: query, category: .Video, page: 1, loadCache: false, success: { (result) -> Void in
            videoListVC.videosList = []
            if let musicArray = result["music_list"].array {
                for item: JSON in musicArray {
                    videoListVC.videosList.append(Music.fromDictionary(item.dictionaryObject!))
                }
            }
            videoListVC.tableView.reloadData()
            videoListVC.swipeRefreshControl.endRefreshing()
            }, failure: { (error) -> Void in
                //
        })
   
    }
    
    private func searchAlbumListVC(query: String) -> UIViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle())
        let albumListVC = storyboard.instantiateViewControllerWithIdentifier("AlbumList") as! AlbumsCollectionViewController
        
        albumListVC.isSearchVC = true
        albumListVC.listType = "search"
        albumListVC.catUrlString = query

        albumListVC.registerObserver(CSN.Notification.searchButtonDidTapped, object: nil, queue: nil) { (noti) -> Void in
            self.loadAlbumData(albumListVC)
        }
        
        albumListVC.loadMoreHandler = {
            albumListVC.currentPage++
            albumListVC.swipeRefreshControl.startRefreshing()
            CSN.API.search(query: query, category: CategoryType.Album, page: albumListVC.currentPage, loadCache: true, success: {
                (result) -> Void in
                if result["album_list"] != nil {
                    if let albumArray = result["album_list"].array {
                        for item: JSON in albumArray {
                            albumListVC.albumsList.append(Album.fromDictionary(item.dictionaryObject!))
                        }
                        albumListVC.collectionView?.reloadData()
                    }
                }
                albumListVC.swipeRefreshControl.endRefreshing()
                }, failure: { (error) -> Void in
                    
            })
        }
        
        return albumListVC as UIViewController
    }
    
    func loadAlbumData(albumListVC: AlbumsCollectionViewController) {
        albumListVC.swipeRefreshControl.startRefreshing()
        CSN.API.search(query: query, category: CategoryType.Album, page: 1, loadCache: false, success: {
            (result) -> Void in
            albumListVC.albumsList = []
            if result["album_list"] != nil {
                if let albumArray = result["album_list"].array {
                    for item: JSON in albumArray {
                        albumListVC.albumsList.append(Album.fromDictionary(item.dictionaryObject!))
                    }
                    albumListVC.collectionView?.reloadData()
                }
            }
            albumListVC.swipeRefreshControl.endRefreshing()
            }, failure: { (error) -> Void in
                
        })
    }
    
    private func searchArtistListVC() -> UIViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle())
        let songListVC = storyboard.instantiateViewControllerWithIdentifier("SongList") as! SongsListTableViewController
        
        songListVC.isOnline = true
        songListVC.showCategoryButton = false
        songListVC.showDetailButton = false
        
        if query != "" {
            songListVC.viewDidLoadHandler = {
                self.loadArtistData(songListVC)
            }
        }
        
        songListVC.registerObserver(CSN.Notification.searchButtonDidTapped, object: nil, queue: nil) { (noti) -> Void in
            self.loadArtistData(songListVC)
        }
        
        songListVC.reloadHandler = {
            self.loadArtistData(songListVC)
            songListVC.swipeRefreshControl.endRefreshing()
            sharedV2TAds.showInterstitialAdsFromRootViewController(songListVC)
        }
        
        songListVC.loadMoreHandler = {
            songListVC.currentPage++
            songListVC.swipeRefreshControl.startRefreshing()
            CSN.API.search(query: self.query, category: .Artist, page: songListVC.currentPage, loadCache: true, success: { (result) -> Void in
                if let musicArray = result["music_list"].array {
                    for item: JSON in musicArray {
                        songListVC.songsList.append(Music.fromDictionary(item.dictionaryObject!))
                    }
                    songListVC.tableView.reloadData()
                }
                songListVC.swipeRefreshControl.endRefreshing()
                }, failure: { (error) -> Void in
                    //
            })

        }
        
        return songListVC as UIViewController
    }
    
    func loadArtistData(songListVC: SongsListTableViewController) {
        songListVC.swipeRefreshControl.startRefreshing()
        CSN.API.search(query: query, category: .Artist, page: 1, loadCache: false, success: { (result) -> Void in
            songListVC.songsList = []
            if let musicArray = result["music_list"].array {
                for item: JSON in musicArray {
                    songListVC.songsList.append(Music.fromDictionary(item.dictionaryObject!))
                }
                songListVC.tableView.reloadData()
            }
            songListVC.swipeRefreshControl.endRefreshing()
            }, failure: { (error) -> Void in
                //
        })
    }
}
