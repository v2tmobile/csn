//
//  MoreAppsViewController.swift
//  ChiaSeNhac
//
//  Created by Vũ Trung Thành on 1/14/16.
//  Copyright © 2016 V2T Multimedia. All rights reserved.
//

import UIKit
import SwiftyJSON
import Haneke
import FontAwesome_swift

class MoreAppsViewController: V2TTableViewController {
    
    private let reuseableCellIdentifier = "appItemCell"
    
    var appsList: [AppItem] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let apps = CSN.Setting.globalJSON?["more_apps"]["list_app"] {
            for app: SwiftyJSON.JSON in apps.arrayValue {
                appsList.append(AppItem.fromDictionary(app.dictionaryObject!))
            }
            debugPrint(appsList)
            self.tableView.reloadData()
        }
        
        self.title = NSLocalizedString("Game, ứng dụng hữu ích", comment: "")

        // Do any additional setup after loading the view.
    }
    
    func backButtonTapped(button: UIButton) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        self.navigationController?.interactivePopGestureRecognizer?.enabled = false
        // Left navigation button
        let backButton = UIButton(type: .Custom)
        backButton.setImage(UIImage.fontAwesomeIconWithName(FontAwesome.AngleLeft, textColor: UIColor.whiteColor(), size: CGSize(width: 26, height: 26)), forState: .Normal)
        backButton.frame = CGRect(x: 0, y: 0, width: 32, height: 32)
        backButton.layer.cornerRadius = 16
        backButton.layer.borderColor = UIColor.whiteColor().CGColor
        backButton.layer.borderWidth = 1
        backButton.addTarget(self, action: "backButtonTapped:", forControlEvents: .TouchUpInside)
        let backBarButtonItem = UIBarButtonItem(customView: backButton)
        self.navigationItem.leftBarButtonItem = backBarButtonItem
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return appsList.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier(reuseableCellIdentifier, forIndexPath: indexPath) as!
        AppItemTableViewCell
        cell.iconImageView.hnk_setImageFromURL(NSURL(string: appsList[indexPath.row].icon)!)
        cell.nameLabel.text = appsList[indexPath.row].name
        cell.descriptionLabel.text = appsList[indexPath.row].descriptions
        cell.appItem = appsList[indexPath.row]
        return cell
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
