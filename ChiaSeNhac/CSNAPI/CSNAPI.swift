//
//  CSNAPI.swift
//  ChiaSeNhac
//
//  Created by Vũ Trung Thành on 12/4/15.
//  Copyright © 2015 V2T Multimedia. All rights reserved.
//

import UIKit
import Haneke
import SwiftyJSON

public enum CategoryType {
    case Video
    case Song
    case Album
    case Artist
}

public class CSNAPI: NSObject {
    let baseURL: String = "http://chiasenhac.com/api/"
    var code: String = "quynhnv8668171013"
    let searchURL: String = "http://search.chiasenhac.com/api/"
    
    override init() {
        super.init()
        if let code = CSN.Setting.globalJSON?["chiasenhac_key"].string {
            self.code = code
        }
        self.registerObserver(CSN.Notification.getAppConfigDone, object: nil, queue: nil) { (noti) -> Void in
            if let code = CSN.Setting.globalJSON?["chiasenhac_key"].string {
                self.code = code
            }
        }
    }
    
    public func getAllMusic(loadCache cache: Bool, success: ((result: SwiftyJSON.JSON) -> Void)?, failure: ((error: NSError) -> Void)?) {
        let jsonCache = Shared.JSONCache
        let URL = NSURL(string: baseURL + "home.php?return=json&code=" + code)!
        if !cache {
            jsonCache.remove(key: URL.absoluteString)
        }
        jsonCache.fetch(URL: URL, formatName: HanekeGlobals.Cache.OriginalFormatName, failure: { (error) -> () in
            self.showErrorDialog()
            failure?(error: error!)
            }) { (responseJson) -> () in
                success?(result: SwiftyJSON.JSON(responseJson.dictionary))
        }
    }
    
    public func getCategory(name name: String, page: Int, loadCache cache: Bool, success: ((result: SwiftyJSON.JSON) -> Void)?, failure: ((error: NSError) -> Void)?) {
        let jsonCache = Shared.JSONCache
        let URL = NSURL(string: baseURL + "category.php?return=json&code=" + code + "&c=" + name + "&page=" + String(page))!
        if !cache {
            jsonCache.remove(key: URL.absoluteString)
        }
        jsonCache.fetch(URL: URL, formatName: HanekeGlobals.Cache.OriginalFormatName, failure: { (error) -> () in
            self.showErrorDialog()
            failure?(error: error!)
            }) { (responseJson) -> () in
                success?(result: SwiftyJSON.JSON(responseJson.dictionary))
        }
    }
    
    public func getDetailsMusic(music music: Music, loadCache cache: Bool, success: ((result: SwiftyJSON.JSON) -> Void)?, failure: ((error: NSError) -> Void)?) {
        let jsonCache = Shared.JSONCache
        let URL = NSURL(string: baseURL + "listen.php?return=json&m=" + music.musicId + "&url=" + music.musicTitleUrl + "&code=" + code)!
        if !cache {
            jsonCache.remove(key: URL.absoluteString)
        }
        jsonCache.fetch(URL: URL, formatName: HanekeGlobals.Cache.OriginalFormatName, failure: { (error) -> () in
            self.showErrorDialog()
            failure?(error: error!)
            }) { (responseJson) -> () in
                success?(result: SwiftyJSON.JSON(responseJson.dictionary))
        }
    }
    
    public func getDetailsMusic(album album: Album, loadCache cache: Bool, success: ((result: SwiftyJSON.JSON) -> Void)?, failure: ((error: NSError) -> Void)?) {
        let jsonCache = Shared.JSONCache
        let URL = NSURL(string: baseURL + "listen.php?return=json&m=" + album.musicId + "&url=" + album.musicTitleUrl + "&code=" + code)!
        if !cache {
            jsonCache.remove(key: URL.absoluteString)
        }
        jsonCache.fetch(URL: URL, formatName: HanekeGlobals.Cache.OriginalFormatName, failure: { (error) -> () in
            self.showErrorDialog()
            failure?(error: error!)
            }) { (responseJson) -> () in
                success?(result: SwiftyJSON.JSON(responseJson.dictionary))
        }
    }
    
    public func search(query query: String, category: CategoryType, page: Int, loadCache cache: Bool, success: ((result: SwiftyJSON.JSON) -> Void)?, failure: ((error: NSError) -> Void)? ) {
        var urlString = searchURL + "search.php?return=json&order=quality&s=" + query + "&page=" + String(page) + "&code=" + code
        if category == .Video {
            urlString += "&cat=video"
        }
        if category == .Album {
            urlString += "&mode=album"
        }
        if category == .Artist {
            urlString += "&mode=artist"
        }
        let jsonCache = Shared.JSONCache
        let URL = NSURL(string: urlString.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet())!)!
        if !cache {
            jsonCache.remove(key: URL.absoluteString)
        }
        jsonCache.fetch(URL: URL, formatName: HanekeGlobals.Cache.OriginalFormatName, failure: { (error) -> () in
            self.showErrorDialog()
            failure?(error: error!)
            }) { (responseJson) -> () in
                success?(result: SwiftyJSON.JSON(responseJson.dictionary))
        }
    }
    
    // add some function to keep old API after add cache option
    public func getAllMusic(success success: ((result: SwiftyJSON.JSON) -> Void)?, failure: ((error: NSError) -> Void)?) {
        self.getAllMusic(loadCache: true, success: { (result) -> Void in
            success?(result: result)
            }) { (error) -> Void in
                failure?(error: error)
        }
    }
    public func getCategory(name name: String, page: Int, success: ((result: SwiftyJSON.JSON) -> Void)?, failure: ((error: NSError) -> Void)?) {
        self.getCategory(name: name, page: page, loadCache: true, success: { (result) -> Void in
            success?(result: result)
            }) { (error) -> Void in
                failure?(error: error)
        }
    }
    public func getDetailsMusic(music music: Music, success: ((result: SwiftyJSON.JSON) -> Void)?, failure: ((error: NSError) -> Void)?) {
        self.getDetailsMusic(music: music, loadCache: true, success: { (result) -> Void in
            success?(result: result)
            }) { (error) -> Void in
                failure?(error: error)
        }
    }
    
    public func getDetailsMusic(album album: Album, success: ((result: SwiftyJSON.JSON) -> Void)?, failure: ((error: NSError) -> Void)?) {
        self.getDetailsMusic(album: album, loadCache: true, success: { (result) -> Void in
            success?(result: result)
            }) { (error) -> Void in
                failure?(error: error)
        }
    }
    
    public func search(query query: String, category: CategoryType, page: Int, success: ((result: SwiftyJSON.JSON) -> Void)?, failure: ((error: NSError) -> Void)? ) {
        self.search(query: query, category: category, page: page, loadCache: true, success: { (result) -> Void in
            success?(result: result)
            }) { (error) -> Void in
                failure?(error: error)
        }
    }
    
    private func showErrorDialog() {
//        let alert = UIAlertView()
//        alert.title = NSLocalizedString("Lỗi", comment: "")
//        alert.message = NSLocalizedString("Có thế đã xảy ra lỗi về kết nối mạng, vui lòng kiểm tra và thử lại sau.\nXin cảm ơn.", comment: "")
//        alert.addButtonWithTitle("OK")
//        alert.show()
    }
}
