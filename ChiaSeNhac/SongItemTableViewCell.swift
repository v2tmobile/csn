//
//  SongItemTableViewCell.swift
//  ChiaSeNhac
//
//  Created by Vũ Trung Thành on 12/4/15.
//  Copyright © 2015 V2T Multimedia. All rights reserved.
//

import UIKit
import FontAwesomeIconFactory
import UIColor_Hex_Swift

class SongItemTableViewCell: UITableViewCell {

    
    @IBOutlet weak var classifyLabel: UILabel!
    
    @IBOutlet weak var classifyLabelWidthConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var songNumberLabel: UILabel!
    
    @IBOutlet weak var nameLabel: UILabel!
    
    @IBOutlet weak var artistLabel: UILabel!
    
    @IBOutlet weak var bitrateLabel: UILabel!
    
    @IBOutlet weak var listenLabel: UILabel!
    
    @IBOutlet weak var downloadLabel: UILabel!
    
    @IBOutlet weak var durationLabel: UILabel!
    
    var isOnline: Bool!
    var extraButtonTappedHandler: (SongItemTableViewCell -> Void)?
    
    var music: Music? {
        didSet {
            if let music = music {
                
                nameLabel.text = music.musicTitle
                artistLabel.text = music.musicArtist
                if music.musicListen != nil {
                    listenLabel.text = music.musicListen
                }
                if let musicBitrate = music.musicBitrate {
                    bitrateLabel.text = Int(musicBitrate) != 1000 ? musicBitrate + "K" : "Lossless"
                    if Int(musicBitrate) == 1000 {
                        bitrateLabel.textColor = UIColor(rgba: "#f44336")
                    } else if Int(musicBitrate) == 320{
                        bitrateLabel.textColor = UIColor(rgba: "#4CAF50")
                    } else {
                        bitrateLabel.textColor = UIColor(rgba: "#BDBDBD")
                    }
                }
                
                downloadLabel.text = music.musicDownloads
                if let musicLength = music.musicLength {
                    durationLabel.text = CSN.formatTimeFromSeconds(Double(musicLength)!)
                }
                if classifyLabel != nil{
                    if let musicWidth = music.musicWidth {
                        if Int(musicWidth) != 0 {
                            classifyLabelWidthConstraint.constant = 20
                            classifyLabel.text = "MV"
                            classifyLabel.textColor = UIColor(rgba: "#4CAF50")
                            classifyLabel.hidden = false
                        }
                        else {
                            classifyLabelWidthConstraint.constant = 0
                            classifyLabel.hidden = true
                        }
                    }
                }
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .None
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func extraButtonTapped(sender: NIKFontAwesomeButton) {
        if let musicWidth = self.music?.musicWidth {
            if Int(musicWidth) != 0 {
                if !isOnline {
                  self.extraButtonTappedHandler?(self)
                } else {
                 NSNotificationCenter.defaultCenter().postNotificationName(CSN.Notification.showExtraOptionVideoItem, object: self.music)
                }
                
            }
            else {
                if !isOnline {
                    self.extraButtonTappedHandler?(self)
                } else {
                    NSNotificationCenter.defaultCenter().postNotificationName(CSN.Notification.showExtraOptionMusicItem, object: self.music)
                }
                
            }
        }

    }
}
