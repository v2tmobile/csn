//
//  DetailsAlbumViewController.swift
//  ChiaSeNhac
//
//  Created by Phan Hữu Thắng on 12/28/15.
//  Copyright © 2015 V2T Multimedia. All rights reserved.
//

import UIKit
import FontAwesomeIconFactory
import Haneke
import SwiftyJSON
import UIColor_Hex_Swift
import CarbonKit

class DetailsAlbumViewController: V2TViewController {
    
    private var currentEmbedViewController: UIViewController!
    private var itemViews = [
        NSLocalizedString("Chi tiết", comment: ""),
        NSLocalizedString("Liên quan", comment: ""),
        NSLocalizedString("Cùng ca sĩ", comment: "")
    ]
    
    @IBOutlet weak var coverImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var artistLabel: UILabel!
    @IBOutlet weak var relatedContainerView: UIView!
    @IBOutlet weak var playButton: NIKFontAwesomeButton!
    @IBOutlet weak var backButton: NIKFontAwesomeButton!
    
    var isLandscapte = false

    var albumData: Album?
    var songData: Music?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        UIApplication.sharedApplication().setStatusBarHidden(true, withAnimation: UIStatusBarAnimation.Fade)
        
        // Do any additional setup after loading the view.
        
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        
        let tabSwipeNavigation = CarbonTabSwipeNavigation(items: itemViews, delegate: self)
        tabSwipeNavigation.setIndicatorColor(UIColor(rgba: "#d32f2f"))
        tabSwipeNavigation.setSelectedColor(UIColor(rgba: "#d32f2f"))
        tabSwipeNavigation.setNormalColor(UIColor(rgba: "#424242"))
        tabSwipeNavigation.carbonSegmentedControl?.setWidth(UIScreen.mainScreen().bounds.width/3, forSegmentAtIndex: 0)
        tabSwipeNavigation.carbonSegmentedControl?.setWidth(UIScreen.mainScreen().bounds.width/3, forSegmentAtIndex: 1)
        tabSwipeNavigation.carbonSegmentedControl?.setWidth(UIScreen.mainScreen().bounds.width/3, forSegmentAtIndex: 2)
        tabSwipeNavigation.setIndicatorHeight(2)
        tabSwipeNavigation.insertIntoRootViewController(self, andTargetView: relatedContainerView)
        switch UIDevice.currentDevice().userInterfaceIdiom {
        case .Phone:
            // It's an iPhone
            tabSwipeNavigation.setTabBarHeight(32)
        case .Pad:
            // It's an iPad
            tabSwipeNavigation.setTabBarHeight(44)
        default:
            // Uh, oh! What could it be?
            tabSwipeNavigation.setTabBarHeight(32)
        }

    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        if let albumTitle = albumData?.musicAlbum {
            let tracker = GAI.sharedInstance().defaultTracker
            tracker.set(kGAIScreenName, value: "Album: \(albumTitle)")
            
            let builder = GAIDictionaryBuilder.createScreenView()
            tracker.send(builder.build() as [NSObject : AnyObject])
            
            // Set iconSize
            switch UIDevice.currentDevice().userInterfaceIdiom {
            case .Phone:
                // It's an iPhone
                backButton.layer.cornerRadius = 15
                backButton.size = 20
                playButton.layer.cornerRadius = 40
                playButton.size = 32
            case .Pad:
                // It's an iPad
                backButton.layer.cornerRadius = 20
                backButton.size = 29
                playButton.layer.cornerRadius = 50
                playButton.size = 40
            default:
                // Uh, oh! What could it be?
                backButton.layer.cornerRadius = 15
                backButton.size = 20
                playButton.layer.cornerRadius = 40
                playButton.size = 32
            }

        }
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        UIApplication.sharedApplication().setStatusBarHidden(false, withAnimation: UIStatusBarAnimation.Fade)
    }
    
    override func viewDidDisappear(animated: Bool) {
        super.viewDidDisappear(animated)
    }

    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func playButtonTapped(sender: AnyObject) {
        sharedV2TAds.showInterstitialAdsFromRootViewController(self)
        if self.albumData != nil {
            CSN.API.getDetailsMusic(album: self.albumData!,
                success: { (result) -> Void in
                    var trackList: [Music] = []
                    if result["track_list"] != nil {
                        if let musicList = result["track_list"].array {
                            for music: SwiftyJSON.JSON in musicList{
                                trackList.append(Music.fromDictionary(music.dictionaryObject!))
                            }
                        }
                    }
                    if result["music_info"] != nil {
                        let music = Music.fromDictionary(result["music_info"].dictionaryObject!)
                        if trackList.count > 0 {
                            trackList[0] = music
                            CSN.musicPlayer.playItems(trackList, startAtIndex: 0)
                        }
                    }
                },
                failure: { (error) -> Void in
                    
            })
        }
    }
    
    
    @IBAction func downloadButtonTapped(sender: AnyObject) {
    }
    
    @IBAction func likeButtonTapped(sender: AnyObject) {
    }
    
    @IBAction func backButtonTapped(sender: AnyObject) {
        self.navigationController?.popViewControllerAnimated(true)
    }
  
    // MARK: - CarbonTabSwipeNavigationDelegate
    
    func carbonTabSwipeNavigation(carbonTabSwipeNavigation: CarbonTabSwipeNavigation, viewControllerAtIndex index: UInt) -> UIViewController {
        
        let storyboard = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle())
        let albumDetailsVC = storyboard.instantiateViewControllerWithIdentifier("albumDetails") as!AlbumDetailsViewController
       
        switch index {
            case 0:
                albumDetailsVC.tabIndex = 0
                break
            case 1:
                albumDetailsVC.tabIndex = 1
            break
            case 2:
                albumDetailsVC.tabIndex = 2
                break
            default:
                break
        }
        
        CSN.API.getDetailsMusic(album: albumData!,
            success: { (result) -> Void in
                if let albumData = self.albumData {
                    self.nameLabel.text = albumData.musicAlbum
                    self.artistLabel.text = albumData.musicArtist
                    self.coverImageView.hnk_setImageFromURL(NSURL(string: albumData.coverImg)!)
                }
                if result["track_list"] != nil {
                    if let musicList = result["track_list"].array {
                        for music: SwiftyJSON.JSON in musicList{
                            albumDetailsVC.trackList.append(Music.fromDictionary(music.dictionaryObject!))
                        }
                    }
                }
                if result["related"] != nil {
                    if let musicList = result["related"]["music_list"].array {
                        for music: SwiftyJSON.JSON in musicList {
                            albumDetailsVC.relatedList.append(Music.fromDictionary(music.dictionaryObject!))
                        }
                    }
                }
                if result["artist"] != nil {
                    if let musicsList = result["artist"]["music_list"].array {
                        for music: SwiftyJSON.JSON in musicsList {
                            albumDetailsVC.artistList.append(Music.fromDictionary(music.dictionaryObject!))
                        }
                    }
                }
        
            },
            failure: { (error) -> Void in
            
        })

        return albumDetailsVC as UIViewController

    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
}