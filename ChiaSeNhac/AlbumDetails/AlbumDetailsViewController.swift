//
//  AlbumDetailsViewController.swift
//  ChiaSeNhac
//
//  Created by Vũ Trung Thành on 12/12/15.
//  Copyright © 2015 V2T Multimedia. All rights reserved.
//

import UIKit
import SwiftyJSON
import FontAwesomeIconFactory
import DZNEmptyDataSet
import FontAwesome_swift

class AlbumDetailsViewController: V2TTableViewController, DZNEmptyDataSetDelegate, DZNEmptyDataSetSource {
    
    let reuseCellIdentifier: String = "SongCell"
    var tabIndex: Int!
    @IBOutlet weak var adsView: UIView!
    
    var tabBarIndexOfSelectedSegment: Int = 0

    var trackList: [Music] = [] {
        didSet {
            self.tableView.reloadData()
        }

    }
    var relatedList: [Music] = [] {
        didSet {
            self.tableView.reloadData()
        }
        
    }

    var artistList: [Music] = [] {
        didSet {
            self.tableView.reloadData()
        }
        
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.rowHeight = 50
        self.tableView.delegate = self
        
        self.tableView.emptyDataSetDelegate = self
        self.tableView.emptyDataSetSource = self
        self.tableView.tableFooterView = UIView()
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        sharedV2TAds.showBannerAdsInView(sharedV2TAds.bannerReuseableCellAdsNetwork, view: self.tableView.tableHeaderView!, viewController: self) { (banner) -> Void in
            (self.tableView as! V2TTableView).headerViewHeight = 50
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        switch tabIndex {
        case 0:
            return trackList.count
        case 1:
            return relatedList.count
        case 2:
            return artistList.count
        default:
            return trackList.count
        }
    }
    

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell: SongItemTableViewCell = tableView.dequeueReusableCellWithIdentifier(reuseCellIdentifier, forIndexPath: indexPath) as! SongItemTableViewCell
        switch tabIndex {
        case 0:
            cell.music = trackList[indexPath.row]
        case 1:
            cell.music = relatedList[indexPath.row]
        case 2:
            cell.music = artistList[indexPath.row]
        default:
            cell.music = trackList[indexPath.row]
        }
        
        cell.songNumberLabel.text = String(indexPath.row + 1)
        return cell
    }
    
    
    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
    // Return false if you do not want the specified item to be editable.
    return true
    }
    */
    
    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
    if editingStyle == .Delete {
    // Delete the row from the data source
    tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
    } else if editingStyle == .Insert {
    // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }
    }
    */
    
    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {
    
    }
    */
    
    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
    // Return false if you do not want the item to be re-orderable.
    return true
    }
    */
    
    // MARK: - Tableview delegate
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {

        let songCell: SongItemTableViewCell = tableView.cellForRowAtIndexPath(indexPath) as! SongItemTableViewCell
        var music: Music = songCell.music!
        CSN.API.getDetailsMusic(music: music, success: { (result) -> Void in
            if result["music_info"] != nil {
                var list: [Music] = []
                switch self.tabIndex {
                case 0:
                    list = self.trackList
                case 1:
                    list = self.relatedList
                case 2:
                    list = self.artistList
                default:
                    list = self.trackList
                }
                music = Music.fromDictionary(result["music_info"].dictionaryObject!)
                CSN.musicPlayer.playItems(list, startAtIndex: indexPath.row)
            }
            }) { (error) -> Void in
                //
        }
        
        // Show interstitial ads
        if sharedV2TAds.shouldSHowInterstitalOnPlay {
            sharedV2TAds.showInterstitialAdsFromRootViewController(self)
        }

    }
    
    /*
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    // Get the new view controller using segue.destinationViewController.
    // Pass the selected object to the new view controller.
    }
    */
    
    // MARK: - DZNEmptyDataSetSource
    
    func imageForEmptyDataSet(scrollView: UIScrollView!) -> UIImage! {
        return UIImage.fontAwesomeIconWithName(FontAwesome.Music, textColor: UIColor(rgba: "#E0E0E0"), size: CGSize(width: 80, height: 80))
    }
}
