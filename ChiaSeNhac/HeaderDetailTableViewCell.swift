//
//  HeaderDetailTableViewCell.swift
//  ChiaSeNhac
//
//  Created by Vũ Trung Thành on 1/2/16.
//  Copyright © 2016 V2T Multimedia. All rights reserved.
//

import UIKit

class HeaderDetailTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var detailButton: UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
