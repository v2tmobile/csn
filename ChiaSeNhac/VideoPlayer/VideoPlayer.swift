//
//  VideoPlayer.swift
//  ChiaSeNhac
//
//  Created by Vũ Trung Thành on 12/29/15.
//  Copyright © 2015 V2T Multimedia. All rights reserved.
//

import UIKit

public class VideoPlayer: NSObject {
    public var playerController: VideoPlayerViewController?
    
    override init() {
        super.init()
        self.registerObserver(CSN.Notification.startPlayingSong, object: nil, queue: nil) { (noti) -> Void in
            self.playerController?.player?.pause()
        }
        self.registerObserver(CSN.Notification.musicPlayerStateChanged, object: nil, queue: nil) { (noti) -> Void in
            if CSN.musicPlayer.state == AudioPlayerState.Playing {
                self.playerController?.player?.pause()
            }
        }
    }
    
    public func initPlayer(url: String, parameters: [NSObject: AnyObject]?) -> VideoPlayerViewController?{
        self.playerController = VideoPlayerViewController.init(withURL: url, parameters: parameters)
        NSNotificationCenter.defaultCenter().postNotificationName(CSN.Notification.startPlayingVideo, object: nil)
        return self.playerController
    }
}
