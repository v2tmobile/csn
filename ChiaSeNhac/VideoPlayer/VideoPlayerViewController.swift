//
//  VideoPlayerViewController.swift
//  ChiaSeNhac
//
//  Created by Vũ Trung Thành on 12/31/15.
//  Copyright © 2015 V2T Multimedia. All rights reserved.
//

import UIKit

public class VideoPlayerViewController: UIViewController {
    
    let playerControl = VideoPlayerControl()
    
    var currentPlaying: Music?
    var url: String?
    var parameters: [NSObject: AnyObject]?
    var player: IJKMediaPlayback?
    
    init(withURL url: String, parameters: [NSObject: AnyObject]?) {
        super.init(nibName: nil, bundle: nil)
        self.url = url
        self.parameters = parameters
    }

    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    override public func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = UIColor.clearColor()
        
        IJKFFMoviePlayerController.setLogReport(false)
        IJKFFMoviePlayerController.setLogLevel(k_IJK_LOG_INFO)
        
        IJKFFMoviePlayerController.checkIfFFmpegVersionMatch(false)
        
        let options = IJKFFOptions.optionsByDefault()
        options.setFormatOptionValue("ijktcphook", forKey: "http-tcp-hook")
        
        self.player = IJKFFMoviePlayerController.init(contentURL: NSURL(string: url!)!, withOptions: options)
        self.player?.scalingMode = IJKMPMovieScalingMode.AspectFit
        self.player?.shouldAutoplay = true
        
        self.player?.setPauseInBackground(CSN.Setting.isPauseVideoInBackground)
        
        self.view.addSubview((self.player?.view)!)
        self.view.addSubview(self.playerControl)
        
        self.player?.view.translatesAutoresizingMaskIntoConstraints = false
        self.playerControl.translatesAutoresizingMaskIntoConstraints = false
        
        self.view.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|[playerView]|", options: NSLayoutFormatOptions(), metrics: nil, views: ["playerView": self.player!.view]))
        self.view.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:|[playerView]|", options: NSLayoutFormatOptions(), metrics: nil, views: ["playerView": self.player!.view]))
        
        self.view.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|[playerControlView]|", options: NSLayoutFormatOptions(), metrics: nil, views: ["playerControlView": self.playerControl]))
        self.view.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:|[playerControlView]|", options: NSLayoutFormatOptions(), metrics: nil, views: ["playerControlView": self.playerControl]))
        
        self.playerControl.delegatePlayer = self.player;
//        self.playerControl.delegate = self.superclass as? VideoDetailsViewController
        self.playerControl.titleLabel.text = parameters?["title"] as? String
        self.playerControl.subTitleLabel.text = parameters?["artist"] as? String
    }
    
    public override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
         self.player?.prepareToPlay()
        NSNotificationCenter.defaultCenter().postNotificationName(CSN.Notification.startPlayingVideo, object: nil)
        UIApplication.sharedApplication().idleTimerDisabled = true
    }
    
    public override func viewDidDisappear(animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    public override func removeFromParentViewController() {
        super.removeFromParentViewController()
        UIApplication.sharedApplication().idleTimerDisabled = false
        self.player?.shutdown()
    }

    override public func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
