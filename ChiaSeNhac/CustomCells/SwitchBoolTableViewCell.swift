//
//  SwitchBoolTableViewCell.swift
//  ChiaSeNhac
//
//  Created by Phan Hữu Thắng on 1/6/16.
//  Copyright © 2016 V2T Multimedia. All rights reserved.
//

import UIKit

protocol SwitchBoolCellDelegate {
    func switchBoolCellChanged(switchBoolCell: SwitchBoolTableViewCell, value: Bool)
}

class SwitchBoolTableViewCell: UITableViewCell {

    @IBOutlet weak var switchControl: UISwitch!
    @IBOutlet weak var titleCell: UILabel!
    
    var delegate: SwitchBoolCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func switchBoolChanged(sender: AnyObject) {
        let switchBool = sender as! UISwitch
        if delegate != nil {
            self.delegate?.switchBoolCellChanged(self, value: switchBool.on)
        }
    }
    

}
