//
//  V2TWithSearchBarViewController.swift
//  ChiaSeNhac
//
//  Created by Vũ Trung Thành on 12/27/15.
//  Copyright © 2015 V2T Multimedia. All rights reserved.
//

import UIKit
import FontAwesome_swift

class V2TWithSearchBarViewController: V2TViewController, UISearchBarDelegate {
    
    let searchBar = UISearchBar()
    var searchResultController: SearchViewController = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle()).instantiateViewControllerWithIdentifier("SearchViewResult") as! SearchViewController

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Set navigation bar tint / background colour
        UINavigationBar.appearance().barTintColor = UIColor(rgba: "#d32f2f")
        
        // Set Navigation bar Title colour
        UINavigationBar.appearance().titleTextAttributes = [NSForegroundColorAttributeName:UIColor.whiteColor()]
        
        // Set navigation bar ItemButton tint colour
        UIBarButtonItem.appearance().tintColor = UIColor.whiteColor()
        
        //Set navigation bar Back button tint colour
        UINavigationBar.appearance().tintColor = UIColor.whiteColor()

        // Do any additional setup after loading the view.
        self.searchBar.delegate = self
        self.searchBar.placeholder = NSLocalizedString("Bạn muốn nghe gì...?", comment: "")
        self.searchBar.tintColor = UIColor.clearColor()
        self.searchBar.barTintColor = UIColor.clearColor()
        self.searchBar.backgroundImage = UIImage()
        self.searchBar.translucent = true
        let textField: UITextField? = self.searchBar.valueForKey("_searchField") as? UITextField
        textField?.backgroundColor = UIColor(rgba: "#b71c1c")
        textField?.textColor = UIColor.whiteColor()
        textField?.tintColor = UIColor.whiteColor()
        let textFieldInsideSearchBarLabel = textField?.valueForKey("placeholderLabel") as? UILabel
        textFieldInsideSearchBarLabel?.textColor = UIColor.whiteColor()
        if CSN.Setting.isShowSearchFunction {
            navigationItem.titleView = self.searchBar
        }
        self.registerObserver(CSN.Notification.getAppConfigDone, object: nil, queue: nil) { (noti) -> Void in
            if CSN.Setting.isShowThirdPartyContent {
                self.navigationItem.titleView = self.searchBar
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    // MARK: - UISearchBarDelegate
    func searchBarCancelButtonClicked(searchBar: UISearchBar) {
        self.searchBar.resignFirstResponder()
        self.searchBar.text = ""
        searchResultController.willMoveToParentViewController(nil)
        searchResultController.view.removeFromSuperview()
        searchResultController.removeFromParentViewController()
        
        navigationItem.leftBarButtonItem = nil
    }
    
    func searchBarTextDidBeginEditing(searchBar: UISearchBar) {
        
        searchBar.text = searchResultController.query
        self.addChildViewController(searchResultController)
        self.view.addSubview(searchResultController.view)
        searchResultController.view.translatesAutoresizingMaskIntoConstraints = false
        self.view.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|[searchView]|", options: NSLayoutFormatOptions(), metrics: nil, views: ["searchView": searchResultController.view]))
        self.view.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:|[searchView]|", options: NSLayoutFormatOptions(), metrics: nil, views: ["searchView": searchResultController.view]))
        searchResultController.didMoveToParentViewController(self)
        
        let backButtonItem = UIBarButtonItem(image: UIImage.fontAwesomeIconWithName(FontAwesome.ArrowLeft, textColor: UIColor(rgba: "#d32f2f"), size: CGSize(width: 26, height: 26)), style: .Plain, target: self, action: "searchBarCancelButtonClicked:")
        navigationItem.leftBarButtonItem = backButtonItem
    }
    
    func searchBarTextDidEndEditing(searchBar: UISearchBar) {
        self.searchBar.resignFirstResponder()
    }
    
    func searchBarSearchButtonClicked(searchBar: UISearchBar) {
        self.searchBar.resignFirstResponder()
        if let query = searchBar.text {
            searchResultController.query = query
            NSNotificationCenter.defaultCenter().postNotificationName(CSN.Notification.searchButtonDidTapped, object: nil)
        }
    }

}
