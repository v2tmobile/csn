//
//  VideoDetailsTableViewController.swift
//  ChiaSeNhac
//
//  Created by Phan Hữu Thắng on 12/16/15.
//  Copyright © 2015 V2T Multimedia. All rights reserved.
//

import UIKit
import SwiftyJSON
import DZNEmptyDataSet
import FontAwesome_swift

protocol RelatedVideosTableViewControllerDelegate {
    func relatedVideoDidSelectedWithVideo(video: Music)
}

class RelatedVideosTableViewController: V2TTableViewController, DZNEmptyDataSetSource, DZNEmptyDataSetDelegate {
    
    private let reuseCellIdentifier: String = "videoDetailsCell"
    
    @IBOutlet weak var adsView: UIView!
    
    var relatedVideosList: [Music] = [] {
        didSet {
            self.tableView.reloadData()
        }
    }

    var artistVideosList: [Music] = [] {
        didSet {
            self.tableView.reloadData()
        }
    }

    var music: Music?
    var delegate: RelatedVideosTableViewControllerDelegate?
    
    var tabBarIndexOfSelectedSegment: Int = 0

    @IBOutlet weak var videoImageView: UIView!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.rowHeight = 70
        self.tableView.delegate = self
        
        self.tableView.emptyDataSetSource = self
        self.tableView.emptyDataSetDelegate = self
        self.tableView.tableFooterView = UIView()
        
        self.loadData()
        
        self.registerObserver(CSN.Notification.startPlayingVideo, object: nil, queue: nil) { (noti) -> Void in
            self.loadData()
        }
    }
    
    func loadData() {
        self.music = CSN.videoPlayer.playerController?.currentPlaying
        self.relatedVideosList = []
        self.artistVideosList = []
        if let music = music {
            
            CSN.API.getDetailsMusic(music: music,
                success: { (result) -> Void in
                    if result["related"] != nil {
                        for relatedVideo: SwiftyJSON.JSON in result["related"]["music_list"].arrayValue {
                            self.relatedVideosList.append(Music.fromDictionary(relatedVideo.dictionaryObject!))
                        }
                    }
                    
                    if result["artist"] != nil {
                        for artistVideo: SwiftyJSON.JSON in result["artist"]["music_list"].arrayValue {
                            self.artistVideosList.append(Music.fromDictionary(artistVideo.dictionaryObject!))
                        }
                    }
                    self.tableView.reloadData()
                },
                failure: { (error) -> Void in
                    //
            })
        }
    }
    
    override func viewDidAppear(animated: Bool) {
        sharedV2TAds.showBannerAdsInView(sharedV2TAds.bannerReuseableCellAdsNetwork, view: self.tableView.tableHeaderView!, viewController: self) { (banner) -> Void in
            (self.tableView as! V2TTableView).headerViewHeight = 50
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tabBarIndexOfSelectedSegment == 0 {
            return relatedVideosList.count
        } else {
            return artistVideosList.count
        }
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell: SongItemTableViewCell = tableView.dequeueReusableCellWithIdentifier(reuseCellIdentifier, forIndexPath: indexPath) as! SongItemTableViewCell

        // Configure the cell...

        if tabBarIndexOfSelectedSegment == 0 {
            cell.music = relatedVideosList[indexPath.row]
        } else {
            cell.music = artistVideosList[indexPath.row]
        }
        cell.songNumberLabel.text = String(indexPath.row + 1)
        return cell
    }

    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */
    
    // MARK: - Tableview delegate
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        let selectedCell = tableView.cellForRowAtIndexPath(indexPath) as! SongItemTableViewCell
        var music = selectedCell.music
        
        CSN.API.getDetailsMusic(music: music!,
            success: {(result) -> Void in
                
                if result["music_info"] != nil {
                    music = Music.fromDictionary(result["music_info"].dictionaryObject!)
                    if music?.musicWidth == "0" {
                        var list: [Music] = []
                        if result["artist"] != nil {
                            for artistVideo: SwiftyJSON.JSON in result["artist"]["music_list"].arrayValue {
                                list.append(Music.fromDictionary(artistVideo.dictionaryObject!))
                            }
                        }
                        CSN.musicPlayer.playItem(music!)
                        CSN.musicPlayer.addItemsToQueue(list)
                        // Show interstitial ads
                        if sharedV2TAds.shouldSHowInterstitalOnPlay {
                            sharedV2TAds.showInterstitialAdsFromRootViewController(self)
                        }
                    } else  {
                        if self.delegate != nil {
                            self.relatedVideosList = []
                            self.artistVideosList = []
                            if result["related"] != nil {
                                for relatedVideo: SwiftyJSON.JSON in result["related"]["music_list"].arrayValue {
                                    self.relatedVideosList.append(Music.fromDictionary(relatedVideo.dictionaryObject!))
                                }
                            }
                            
                            if result["artist"] != nil {
                                for artistVideo: SwiftyJSON.JSON in result["artist"]["music_list"].arrayValue {
                                    self.artistVideosList.append(Music.fromDictionary(artistVideo.dictionaryObject!))
                                }
                            }
                            self.tableView.reloadData()
                            self.delegate?.relatedVideoDidSelectedWithVideo(music!)
                        }
                    }
                }
            })
            {(error) -> Void in
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    // MARK: - DZNEmptyDataSetSource
    
    func imageForEmptyDataSet(scrollView: UIScrollView!) -> UIImage! {
        return UIImage.fontAwesomeIconWithName(FontAwesome.Music, textColor: UIColor(rgba: "#E0E0E0"), size: CGSize(width: 80, height: 80))
    }
   }

