//
//  VideoDetailInfomationsViewController.swift
//  ChiaSeNhac
//
//  Created by Vũ Trung Thành on 1/10/16.
//  Copyright © 2016 V2T Multimedia. All rights reserved.
//

import UIKit

class VideoDetailInfomationsViewController: V2TViewController {
    
    var music: Music?
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var artistLabel: UILabel!
    @IBOutlet weak var lyricLabel: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.registerObserver(CSN.Notification.startPlayingVideo, object: nil, queue: nil) { (noti) -> Void in
            self.titleLabel.text = CSN.videoPlayer.playerController?.currentPlaying?.musicTitle
            self.artistLabel.text = CSN.videoPlayer.playerController?.currentPlaying?.musicArtist
            self.lyricLabel.text = CSN.videoPlayer.playerController?.currentPlaying?.musicLyric
        }
        
        if var music = music {
            
            CSN.API.getDetailsMusic(music: music,
                success: { (result) -> Void in
                    if result["music_info"] != nil {
                        music = Music.fromDictionary(result["music_info"].dictionaryObject!)
                        self.music = music
                        self.titleLabel.text = music.musicTitle
                        self.artistLabel.text = music.musicArtist
                        self.lyricLabel.text = music.musicLyric
                    }
                },
                failure: { (error) -> Void in
                    //
            })
        }
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
