//
//  VideoDetailsViewController.swift
//  ChiaSeNhac
//
//  Created by Phan Hữu Thắng on 12/17/15.
//  Copyright © 2015 V2T Multimedia. All rights reserved.
//

import UIKit
import Haneke
import SwiftyJSON
import UIColor_Hex_Swift
import FontAwesomeIconFactory
import CarbonKit
import FontAwesome_swift

let sharedVideoDetailVC = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle()).instantiateViewControllerWithIdentifier("videoDetails") as? VideoDetailsViewController

class VideoDetailsViewController: V2TViewController, RelatedVideosTableViewControllerDelegate, VideoPlayerControlDelegate, CarbonTabSwipeNavigationDelegate {
    
    private var timerToShowBannerAds = NSTimer()
    private var timerToHideBannerAds = NSTimer()
    
    private let heightMiniVideoPlayer = 120
    
    private let itemsView = [
        NSLocalizedString("Chi tiết", comment: ""),
        NSLocalizedString("Liên quan", comment: ""),
        NSLocalizedString("Cùng ca sỹ", comment: ""),
    ]
    
    private var currentEmbedViewController: UIViewController!
    
    @IBOutlet weak var videoContainerView: UIView!
    
    @IBOutlet weak var relatedContainerView: UIView!
    
    @IBOutlet weak var videoImageView: UIImageView!

    @IBOutlet weak var backButton: NIKFontAwesomeButton!
    
    var qualitiesSettingView: CMPopTipView!
    
    var isLandscapte = false
    
    var widthConstraint: NSLayoutConstraint!
    var heightConstraint: NSLayoutConstraint!
    var centerXConstraint: NSLayoutConstraint!
    var centerYConstraint: NSLayoutConstraint!
    
    var qualities:[[String: AnyObject]] = []

    var music: Music? {
        didSet {
            qualities = []
            if (music?.fileLosslessUrl != nil) && music?.fileLosslessUrl != "" {
                qualities.append(["name" : "1080p", "url": music!.fileLosslessUrl])
            }
            if (music?.fileM4aUrl != nil) && music?.fileM4aUrl != "" {
                qualities.append(["name" : "720p", "url": music!.fileM4aUrl])
            }
            if (music?.file320Url != nil) && music?.file320Url != "" {
                qualities.append(["name" : "480p", "url": music!.file320Url])
            }
            if (music?.fileUrl != nil) && music?.fileUrl != "" {
                qualities.append(["name" : "320p", "url": music!.fileUrl])
            }
            if (music?.file32Url != nil) && music?.file32Url != "" {
                qualities.append(["name" : "240p", "url": music!.file32Url])
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        self.navigationController?.interactivePopGestureRecognizer?.enabled = false
        
        widthConstraint = NSLayoutConstraint(item: self.videoContainerView, attribute: .Width, relatedBy: .Equal, toItem: self.view, attribute: .Height, multiplier: 1, constant: 0)
        heightConstraint = NSLayoutConstraint(item: self.videoContainerView, attribute: .Height, relatedBy: .Equal, toItem: self.view, attribute: .Width, multiplier: 1, constant: 0)
        centerXConstraint = NSLayoutConstraint(item: self.videoContainerView, attribute: .CenterX, relatedBy: .Equal, toItem: self.view, attribute: .CenterX, multiplier: 1, constant: 0)
        centerYConstraint = NSLayoutConstraint(item: self.videoContainerView, attribute: .CenterY, relatedBy: .Equal, toItem: self.view, attribute: .CenterY, multiplier: 1, constant: 0)
        
        let tabSwipeNavigation = CarbonTabSwipeNavigation(items: itemsView, delegate: self)
        tabSwipeNavigation.setIndicatorColor(UIColor(rgba: "#d32f2f"))
        tabSwipeNavigation.setSelectedColor(UIColor(rgba: "#d32f2f"))
        tabSwipeNavigation.setNormalColor(UIColor(rgba: "#424242"))
        switch UIDevice.currentDevice().userInterfaceIdiom {
        case .Phone:
            // It's an iPhone
            tabSwipeNavigation.setTabBarHeight(32)
        case .Pad:
            // It's an iPad
            tabSwipeNavigation.setTabBarHeight(44)
        default:
            // Uh, oh! What could it be?
            tabSwipeNavigation.setTabBarHeight(32)
        }
        tabSwipeNavigation.carbonSegmentedControl?.setWidth(UIScreen.mainScreen().bounds.width/3, forSegmentAtIndex: 0)
        tabSwipeNavigation.carbonSegmentedControl?.setWidth(UIScreen.mainScreen().bounds.width/3, forSegmentAtIndex: 1)
        tabSwipeNavigation.carbonSegmentedControl?.setWidth(UIScreen.mainScreen().bounds.width/3, forSegmentAtIndex: 2)
        tabSwipeNavigation.setIndicatorHeight(2)
        tabSwipeNavigation.insertIntoRootViewController(self, andTargetView: relatedContainerView)
        
        let storyboard = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle())
        let videoDetailsVC = storyboard.instantiateViewControllerWithIdentifier("videoDetailInformations") as!VideoDetailInfomationsViewController
        videoDetailsVC.music = self.music
        currentEmbedViewController = videoDetailsVC
        
        self.registerObserver(UIDeviceOrientationDidChangeNotification, object: nil, queue: nil) { (noti) -> Void in
            self.rotated()
        }
    }
    
    func backButtonTapped(button: UIButton) {
        self.hide()

    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        UIApplication.sharedApplication().setStatusBarHidden(true, withAnimation: UIStatusBarAnimation.Fade)
        
    }

    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        UIApplication.sharedApplication().setStatusBarHidden(false, withAnimation: UIStatusBarAnimation.Fade)
    }
    
    override func viewDidDisappear(animated: Bool) {
        super.viewDidDisappear(animated)
    }

    func qualitiesSettingView(music: Music, moviePlayer: VideoPlayerViewController) -> CMPopTipView{
        let buttonH = 20
        let settingView = UIView(frame: CGRect(x: 0, y: 0, width: 30, height: qualities.count*buttonH))
        if qualities.count != 0 {
        for i in 0...qualities.count - 1 {
            let button = UIButton.init(frame: CGRect(x: 0, y: i*buttonH, width: 30, height: buttonH))
            let title = qualities[i]["name"]! as! String
            button.setTitle(title, forState: UIControlState.Normal)
            button.autoresizingMask = [UIViewAutoresizing.FlexibleWidth, UIViewAutoresizing.FlexibleBottomMargin]
            if moviePlayer.url == qualities[i]["url"]! as? String{
                button.setTitleColor(UIColor.redColor(), forState: UIControlState.Normal)
            } else {
                button.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
            }
            button.titleLabel?.font = UIFont.systemFontOfSize(8)
            button.tag = i
            button.addTarget(self, action: "qualitySettingButtonSelected:", forControlEvents: UIControlEvents.TouchUpInside)
            settingView.addSubview(button)
        }
        }
        let toolTipView = CMPopTipView.init(customView: settingView)
        toolTipView.backgroundColor = UIColor.blackColor()
        toolTipView.preferredPointDirection = PointDirection.Down
        toolTipView.dismissTapAnywhere = true
        return toolTipView
    }

    func qualitySettingButtonSelected(button: UIButton) {
        if qualities[button.tag]["name"] as! String == "240p" {
            CSN.Setting.videoQuality = .k240p
        }
        if qualities[button.tag]["name"] as! String == "320p" {
            CSN.Setting.videoQuality = .k320p
        }
        if qualities[button.tag]["name"] as! String == "480p" {
            CSN.Setting.videoQuality = .k480p
        }
        if qualities[button.tag]["name"] as! String == "720p" {
            CSN.Setting.videoQuality = .k720p
        }
        if qualities[button.tag]["name"] as! String == "1080p" {
            CSN.Setting.videoQuality = .k1080p
        }
        self.playVideo(self.music!)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func shouldAutorotate() -> Bool {
        return true
    }
    
    override func prefersStatusBarHidden() -> Bool {
        return true
    }
    
    // MARK: - CarbonTabSwipeNavigationDelegate
    func carbonTabSwipeNavigation(carbonTabSwipeNavigation: CarbonTabSwipeNavigation, viewControllerAtIndex index: UInt) -> UIViewController {
        switch index {
        case 0:
            let storyboard = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle())
            let videoDetailsVC = storyboard.instantiateViewControllerWithIdentifier("videoDetailInformations") as!VideoDetailInfomationsViewController
            videoDetailsVC.music = self.music
            return videoDetailsVC
        case 1:
            self.performSegueWithIdentifier("embedRelatedVideosList", sender: index)
            break
        case 2:
            self.performSegueWithIdentifier("embedRelatedVideosList", sender: index)
            break
        default:
            self.performSegueWithIdentifier("embedVideoDetails", sender: index)
            break
        }
        self.view.layoutIfNeeded()
        return self.currentEmbedViewController
    }

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == "embedRelatedVideosList" {
            let relatedVideoTableVC = segue.destinationViewController as! RelatedVideosTableViewController
            relatedVideoTableVC.tabBarIndexOfSelectedSegment = Int(sender as! UInt) - 1
            relatedVideoTableVC.delegate = self
            relatedVideoTableVC.music = self.music
            self.currentEmbedViewController = relatedVideoTableVC
            
//            if var music = music {
//                
//                CSN.API.getDetailsMusic(music: music,
//                    success: { (result) -> Void in
//                        
//                        if result["music_info"] != nil {
//                            music = Music.fromDictionary(result["music_info"].dictionaryObject!)
//                            self.videoImageView.hnk_setImageFromURL(NSURL(string: music.videoPreview)!)
//                            self.music = music
//                        }
//                    },
//                    failure: { (error) -> Void in
//                        self.hide()
//                })
//            }
        }
    }
    
    func playVideo(music: Music) {
        
        CSN.videoPlayer.playerController?.willMoveToParentViewController(nil)
        CSN.videoPlayer.playerController?.view.removeFromSuperview()
        CSN.videoPlayer.playerController?.removeFromParentViewController()

        var musicPath: String?
        switch CSN.Setting.videoQuality {
        case .k1080p:
            musicPath = music.fileLosslessUrl
            break
        case .k720p:
            musicPath = music.fileM4aUrl
            break
        case .k480p:
            musicPath = music.file320Url
            break
        case .k320p:
            musicPath = music.fileUrl
            break
        case .k240p:
            musicPath = music.file32Url
            break
        }
        
        if musicPath == nil || musicPath == "" {
            switch CSN.Setting.videoQuality {
            case .k1080p:
                musicPath = music.fileM4aUrl
                break
            case .k720p:
                musicPath = music.file320Url
                break
            case .k480p:
                musicPath = music.fileUrl
                break
            case .k320p:
                musicPath = music.fileUrl
                break
            case .k240p:
                musicPath = music.fileUrl
                break
            }
        }
        
        if musicPath == nil || musicPath == "" {
            switch CSN.Setting.videoQuality {
            case .k1080p:
                musicPath = music.file320Url
                break
            case .k720p:
                musicPath = music.fileUrl
                break
            case .k480p:
                musicPath = music.fileUrl
                break
            case .k320p:
                musicPath = music.fileUrl
                break
            case .k240p:
                musicPath = music.fileUrl
                break
            }
        }
        
        if musicPath == nil || musicPath == "" {
            musicPath = music.fileUrl
        }
        
        if music.path != "" {
            let fileManager = NSFileManager.defaultManager()
            let directoryURL = fileManager.URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask)[0]
            let dataPath = directoryURL.URLByAppendingPathComponent(music.path)
            debugPrint(dataPath)
            musicPath = dataPath.absoluteString
        }
//        print(pathVideo)
        if musicPath == nil {return}
        
        if let videoPlayerController = CSN.videoPlayer.initPlayer(musicPath!, parameters: ["title": music.musicTitle, "artist": music.musicArtist]) {
            videoPlayerController.playerControl.delegate = self
            videoPlayerController.currentPlaying = music
            // send analytic to GA
            if let title = music.musicTitle {
                let tracker = GAI.sharedInstance().defaultTracker
                tracker.set(kGAIScreenName, value: "Video: \(title)")
                
                let builder = GAIDictionaryBuilder.createScreenView()
                tracker.send(builder.build() as [NSObject : AnyObject])
            }
            

            self.addChildViewController(videoPlayerController)
            self.videoContainerView.insertSubview(videoPlayerController.view!, aboveSubview: self.videoImageView)
            videoPlayerController.didMoveToParentViewController(self)
            videoPlayerController.view!.translatesAutoresizingMaskIntoConstraints = false
            self.videoContainerView.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|[videoPlayerView]|", options: NSLayoutFormatOptions(), metrics: nil, views: ["videoPlayerView": videoPlayerController.view!]))
            self.videoContainerView.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:|[videoPlayerView]|", options: NSLayoutFormatOptions(), metrics: nil, views: ["videoPlayerView": videoPlayerController.view!]))
            self.view.layoutIfNeeded()
            qualitiesSettingView = self.qualitiesSettingView(music, moviePlayer: videoPlayerController)
            
            // Ads setting
            if sharedV2TAds.shouldShowBannerInVideo {
                sharedV2TAds.showBannerAdsInView(sharedV2TAds.bannerVideoPlayerAdsNetwork, view: videoPlayerController.playerControl.adsView, viewController: self, bannerViewDidLoad: { (banner) -> Void in
                    videoPlayerController.playerControl.adsView.hidden = true
                    self.timerToShowBannerAds.invalidate()
                    self.timerToShowBannerAds = NSTimer.scheduledTimerWithTimeInterval(sharedV2TAds.delayShowBannerInVideoPlayer, target: self, selector: "showBannerAds", userInfo: nil, repeats: true)
                })
            }
            
            // save to histories
            let musicsList = CSN.uiRealm.objects(Musics).filter("name = 'Histories'")[0].musics
            for item: Music in musicsList {
                if item.musicId == music.musicId {
                    do {
                        try CSN.uiRealm.write({ () -> Void in
                            item.createdAtDate = NSDate()
                            NSNotificationCenter.defaultCenter().postNotificationName(CSN.Notification.saveHistoryDone, object: music)
                        })
                        
                    } catch let error {
                        debugPrint(error)
                        // Handling write error,
                        // e.g. Delete the Realm file, etc.
                    }
                    return
                }
            }
            do {
                try CSN.uiRealm.write({ () -> Void in
                    music.createdAtDate = NSDate()
                    musicsList.append(music)
                    NSNotificationCenter.defaultCenter().postNotificationName(CSN.Notification.saveHistoryDone, object: music)
                })
                
            } catch let error {
                debugPrint(error)
                // Handling write error,
                // e.g. Delete the Realm file, etc.
            }
            //
        }
    }
    
    func showBannerAds() {
        if UIDeviceOrientationIsLandscape(UIDevice.currentDevice().orientation) {
            CSN.videoPlayer.playerController?.playerControl.adsView.hidden = false
            self.timerToHideBannerAds.invalidate()
            self.timerToHideBannerAds = NSTimer.scheduledTimerWithTimeInterval(sharedV2TAds.delayHideBannerInVideoPlayer, target: self, selector: "hideBannerAds", userInfo: nil, repeats: false)
        } else {
            CSN.videoPlayer.playerController?.playerControl.adsView.hidden = true
        }
    }
    
    func hideBannerAds() {
        CSN.videoPlayer.playerController?.playerControl.adsView.hidden = true
        self.timerToShowBannerAds.invalidate()
        self.timerToShowBannerAds = NSTimer.scheduledTimerWithTimeInterval(sharedV2TAds.delayShowBannerInVideoPlayer, target: self, selector: "showBannerAds", userInfo: nil, repeats: true)
    }
    
    func relatedVideoDidSelectedWithVideo(video: Music) {
        self.music = video
        self.videoImageView.hnk_setImageFromURL(NSURL(string: self.music!.videoPreview)!)
        playVideo(video)
    }
    
    func rotated() {
//        self.qualitiesSettingView.dismissAnimated(true)
        self.videoContainerView.transform = CGAffineTransformMakeRotation(0)
        self.view.removeConstraints([widthConstraint, heightConstraint, centerXConstraint, centerYConstraint])
        if(UIDeviceOrientationIsLandscape(UIDevice.currentDevice().orientation))
        {
            NSNotificationCenter.defaultCenter().postNotificationName(CSN.Notification.videoPlayerEnterFullScreen, object: nil)
//            backButton.hidden = true
            self.tabBarController?.tabBar.hidden = true
            if UIDevice.currentDevice().userInterfaceIdiom != .Pad {
                if UIDevice.currentDevice().orientation == UIDeviceOrientation.LandscapeLeft {
                    self.videoContainerView.transform = CGAffineTransformMakeRotation(CGFloat(M_PI_2))
                } else {
                    self.videoContainerView.transform = CGAffineTransformMakeRotation(CGFloat(-M_PI_2))
                }
                if self.videoContainerView.superview == self.view {
                    self.view.addConstraints([widthConstraint, heightConstraint, centerXConstraint, centerYConstraint])
                }
            }
            if sharedV2TAds.shouldShowBannerInVideo {
                CSN.videoPlayer.playerController?.playerControl.adsView.hidden = false
            }
            CSN.videoPlayer.playerController?.playerControl.downloadButton.hidden = true
            
            if (sharedV2TAds.shouldShowVideoAdsOnFullScreen) {
                CSN.videoPlayer.playerController?.player?.pause()
                sharedV2TAds.showVideoAds({ (Void) -> Void in
                    if UIDeviceOrientationIsLandscape(UIDevice.currentDevice().orientation) {
                        CSN.videoPlayer.playerController?.player?.play()
                        if self.videoContainerView.superview == self.view {
                            self.view.addConstraints([self.widthConstraint, self.heightConstraint, self.centerXConstraint, self.centerYConstraint])
                        }
                    }
                })
            }
            
            print("Lansdcape")
            self.isLandscapte = true
        }
        
        if(UIDeviceOrientationIsPortrait(UIDevice.currentDevice().orientation))
        {
            NSNotificationCenter.defaultCenter().postNotificationName(CSN.Notification.videoPlayerExitFullScreen, object: nil)
//            backButton.hidden = false
            self.tabBarController?.tabBar.hidden = false
            CSN.videoPlayer.playerController?.playerControl.adsView.hidden = true
            if (CSN.Setting.isShowDownloadFunction) {
                CSN.videoPlayer.playerController?.playerControl.downloadButton.hidden = false
            }
            print("Portrait")
            self.isLandscapte = false
        }
        UIView.animateWithDuration(0.5, animations: { () -> Void in
            self.view.layoutIfNeeded()
        })
    }
    
//    func v2tMovieViewController(moviePlayerController: V2TMovieViewController!, didTappedSettingButton button: UIButton!) {
//        qualitiesSettingView.presentPointingAtView(button, inView: self.videoContainerView, animated: true)
//    }
    
    
    func showInViewController(viewController: UIViewController) {
        
        sharedVideoDetailVC?.willMoveToParentViewController(nil)
        sharedVideoDetailVC?.view.removeFromSuperview()
        sharedVideoDetailVC?.removeFromParentViewController()
        
        UIApplication.sharedApplication().setStatusBarHidden(true, withAnimation: UIStatusBarAnimation.Fade)
        
        self.movieNotificationObservers()
        
        viewController.addChildViewController(self)
        self.view.translatesAutoresizingMaskIntoConstraints = false
        viewController.view.addSubview(self.view)
        viewController.view.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|[view]|", options: NSLayoutFormatOptions(), metrics: nil, views: ["view": self.view]))
        viewController.view.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:|[view]|", options: NSLayoutFormatOptions(), metrics: nil, views: ["view": self.view]))
        self.didMoveToParentViewController(viewController)
        self.view.alpha = 0
        UIView.animateWithDuration(0.5) { () -> Void in
            self.view.alpha = 1
            if var music = self.music {
                
                if music.downloadStatus == 1 { // IF video downdloaded
                    self.videoImageView.hnk_setImageFromURL(NSURL(string: music.videoPreview)!)
                    self.music = music
                    self.playVideo(music)
                    self.backButton.hidden = true
                } else {
                    CSN.API.getDetailsMusic(music: music,
                        success: { (result) -> Void in
                            
                            if result["music_info"] != nil {
                                music = Music.fromDictionary(result["music_info"].dictionaryObject!)
                                self.videoImageView.hnk_setImageFromURL(NSURL(string: music.videoPreview)!)
                                self.music = music
                                self.playVideo(music)
                            }
                            
                            self.view.layoutSubviews()
                            self.backButton.hidden = true
                        },
                        failure: { (error) -> Void in
                            self.hide()
                    })
                }
            }
        }
    }
    
    func hide() {
        UIApplication.sharedApplication().setStatusBarHidden(false, withAnimation: UIStatusBarAnimation.Fade)
        UIView.animateWithDuration(0.5, animations: { () -> Void in
            self.view.alpha = 0
            }) { (Bool) -> Void in
                CSN.videoPlayer.playerController?.willMoveToParentViewController(nil)
                CSN.videoPlayer.playerController?.view.removeFromSuperview()
                CSN.videoPlayer.playerController?.removeFromParentViewController()
                self.willMoveToParentViewController(nil)
                self.view.removeFromSuperview()
                self.removeFromParentViewController()
                self.removeNotificationObservers()
        }
    }
    
    // MARK: - VideoPlayerDelegate
    // MARK: - VideoPlayerControlDelegate
    func videoPlayerControl(videoPlayerControl: VideoPlayerControl, backButtonDidTapped button: UIButton) {
        if UIDeviceOrientationIsLandscape(UIDevice.currentDevice().orientation) {
            let value = UIInterfaceOrientation.Portrait.rawValue
            UIDevice.currentDevice().setValue(value, forKey: "orientation")
        } else {
//            self.navigationController?.popViewControllerAnimated(true)
            self.hide()
        }
    }
    func videoPlayerControl(videoPlayerControl: VideoPlayerControl, downloadButtonDidTapped button: UIButton) {
        NSNotificationCenter.defaultCenter().postNotificationName(CSN.Notification.showDownloadVideo, object: self.music)
        
    }
    func videoPlayerControl(videoPlayerControl: VideoPlayerControl, likeButtonDidTapped button: UIButton) {
        let musicList = CSN.uiRealm.objects(Musics).filter("name = 'Favorites'")[0].musics
        debugPrint("Music ID: \(music?.musicId)")
        
        for item: Music in musicList {
            debugPrint("item ID: \(item.musicId)")
            
            if item.musicId == music?.musicId {
                return
            }
        }
        do {
            try CSN.uiRealm.write({ () -> Void in
                musicList.append(self.music!)
                NSNotificationCenter.defaultCenter().postNotificationName(CSN.Notification.favoriteDone, object: self.music)
            })
            
        } catch let error {
            debugPrint(error)
        }
    }
    func videoPlayerControl(videoPlayerControl: VideoPlayerControl, settingButtonDidTapped button: UIButton) {
        if self.music?.downloadStatus == 1 {
            return
        }
        if CSN.videoPlayer.playerController != nil && self.music != nil {
            self.qualitiesSettingView(self.music!, moviePlayer: CSN.videoPlayer.playerController!).presentPointingAtView(button, inView: CSN.videoPlayer.playerController!.view, animated: true)
        }
    }
    func videoPlayerControl(videoPlayerControl: VideoPlayerControl, fullscreenButtonDidTapped button: UIButton) {
        
    }
    func videoPlayerControl(videoPlayerControl: VideoPlayerControl, playButtonDidTapped button: UIButton) {
        
    }
    func videoPlayerControl(videoPlayerControl: VideoPlayerControl, pauseButtonDidTapped button: UIButton) {
        if UIDeviceOrientationIsLandscape(UIDevice.currentDevice().orientation) {
            if (sharedV2TAds.shouldShowVideoAdsOnPause) {
                sharedV2TAds.showVideoAds({ (Void) -> Void in
                    if UIDeviceOrientationIsLandscape(UIDevice.currentDevice().orientation) {
                        if self.videoContainerView.superview == self.view {
                            self.view.addConstraints([self.widthConstraint, self.heightConstraint, self.centerXConstraint, self.centerYConstraint])
                        }
                    }
                })
                return
            }
        }
        if sharedV2TAds.shouldShowInterstitialOnPause {
            sharedV2TAds.showInterstitialAdsFromRootViewController(self)
        }
    }
    
    func movieNotificationObservers() {
        self.registerObserver(IJKMPMoviePlayerPlaybackDidFinishNotification, object: CSN.videoPlayer.playerController?.player, queue: nil) { (noti) -> Void in
            if let reason: Int = Int((noti.userInfo?[IJKMPMoviePlayerPlaybackDidFinishReasonUserInfoKey]?.intValue)!) {
                debugPrint(reason)
                
                switch reason {
                case 0://IJKMPMovieFinishReasonPlaybackEnded:
                    // show video ads when video end
                    if UIDeviceOrientationIsLandscape(UIDevice.currentDevice().orientation) {
                        if (sharedV2TAds.shouldShowVideoAdsOnPause) {
                            sharedV2TAds.showVideoAds({ (Void) -> Void in
                                if UIDeviceOrientationIsLandscape(UIDevice.currentDevice().orientation) {
                                    if self.videoContainerView.superview == self.view {
                                        self.view.addConstraints([self.widthConstraint, self.heightConstraint, self.centerXConstraint, self.centerYConstraint])
                                    }
                                }
                            })
                        }
                    }
                    break
                case 1://IJKMPMovieFinishReasonUserExited:
                    break
                case 2://IJKMPMovieFinishReasonPlaybackError:
                    break
                default:
                    break
                }
            }
        }
    }
    
    func removeNotificationObservers() {
        NSNotificationCenter.defaultCenter().removeObserver(self, name: IJKMPMoviePlayerPlaybackDidFinishNotification, object: CSN.videoPlayer.playerController?.player)
    }


}
