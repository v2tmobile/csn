//
//  MusicPlayerViewController.swift
//  ChiaSeNhac
//
//  Created by Vũ Trung Thành on 12/8/15.
//  Copyright © 2015 V2T Multimedia. All rights reserved.
//

import UIKit
import FontAwesome_swift
import FontAwesomeIconFactory
import Haneke

class MusicPlayerViewController: V2TViewController, AudioPlayerDelegate {

    @IBOutlet weak var adsView: UIView!
    @IBOutlet weak var abstractView: UIView!
    @IBOutlet weak var backgroundImageView: UIImageView!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var currentTimeLabel: UILabel!
    @IBOutlet weak var totalTimeLabel: UILabel!
    @IBOutlet weak var musicSlider: UISlider!
    @IBOutlet weak var playButton: NIKFontAwesomeButton!
    @IBOutlet weak var pauseButton: NIKFontAwesomeButton!
    @IBOutlet weak var previousButton: NIKFontAwesomeButton!
    @IBOutlet weak var nextButton: NIKFontAwesomeButton!
    @IBOutlet weak var repeatButton: NIKFontAwesomeButton!
    @IBOutlet weak var equalizerButton: NIKFontAwesomeButton!
    @IBOutlet weak var closeButton: NIKFontAwesomeButton!
    @IBOutlet weak var adsViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var timerButton: UIButton!
    
    var pageViews = [UIViewController]()
    var pages: Int = 3
    var pageViewController: UIPageViewController?
    var currentIndex = 0
    
    var tapCloseButtonActionHandler: (Void -> Void)?
    var tracksList: [Music] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        for i in 0..<pages {
            if let vc = self.storyboard?.instantiateViewControllerWithIdentifier("musicPageView") as? MusicPlayerPageViewController {
                vc.indexPage = i
                pageViews.append(vc)
            }
        }
        pageViewController = self.childViewControllers[0] as? UIPageViewController
        pageViewController!.delegate = self
        pageViewController!.dataSource = self
        pageViewController!.setViewControllers([pageViews[1]], direction: .Forward, animated: true, completion: nil)
        pageControl.numberOfPages = pages
        pageControl.currentPage = 1
        
        // Blur background
        let darkBlur = UIBlurEffect(style: UIBlurEffectStyle.Dark)
        let blurView = UIVisualEffectView(effect: darkBlur)
        backgroundImageView.addSubview(blurView)
        blurView.translatesAutoresizingMaskIntoConstraints = false
        
        backgroundImageView.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|[blurView]|", options: NSLayoutFormatOptions(), metrics: nil, views: ["blurView": blurView]))
        backgroundImageView.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:|[blurView]|", options: NSLayoutFormatOptions(), metrics: nil, views: ["blurView": blurView]))
        
        self.musicSlider.setThumbImage(UIImage.fontAwesomeIconWithName(FontAwesome.Circle, textColor: UIColor(rgba: "#d32f2f"), size: CGSize(width: 16, height: 16)), forState: UIControlState.Normal)
        self.musicSlider.setThumbImage(UIImage.fontAwesomeIconWithName(FontAwesome.Circle, textColor: UIColor(rgba: "#d32f2f"), size: CGSize(width: 16, height: 16)), forState: UIControlState.Selected)
        self.musicSlider.setThumbImage(UIImage.fontAwesomeIconWithName(FontAwesome.Circle, textColor: UIColor(rgba: "#d32f2f"), size: CGSize(width: 16, height: 16)), forState: UIControlState.Highlighted)
        
        
        
        switch CSN.musicPlayer.mode {
        case AudioPlayerModeMask.Repeat:
            repeatButton.iconHex = "f021"
            break
        case AudioPlayerModeMask.RepeatAll:
            repeatButton.iconHex = "f079"
            break
        case AudioPlayerModeMask.Shuffle:
            repeatButton.iconHex = "f074"
            break
        default:
            repeatButton.iconHex = "f178"
            break
        }
        
        sharedV2TAds.showBannerAdsInView(self.adsView, viewController: self, bannerViewDidLoad: {Void in
            UIView.animateWithDuration(0.5, animations: { (banner) in
                self.adsViewHeightConstraint.constant = 50
            })
        })
        
        // Disable time to sleep music function in this version
        timerButton.hidden = true
        
        CSN.musicPlayer.delegate = self
        
        if let thumbURL = CSN.musicPlayer.currentItem?.musicImg {
            self.backgroundImageView.hnk_setImageFromURL(NSURL(string: thumbURL)!)
        }
        
        self.registerObserver(CSN.Notification.startPlayingSong, object: nil, queue: nil) { (noti) -> Void in
            if let thumbURL = CSN.musicPlayer.currentItem?.musicImg {
                self.backgroundImageView.hnk_setImageFromURL(NSURL(string: thumbURL)!)
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func closeButtonTapped(sender: AnyObject) {
        tapCloseButtonActionHandler?()
        self.dismissViewControllerAnimated(true, completion: nil)
    }

    @IBAction func musicSliderChanged(sender: UISlider) {
        CSN.musicPlayer.seekToTime(Double(sender.value))
    }
    
    @IBAction func playButtonTapped(sender: AnyObject) {
        if CSN.musicPlayer.state == AudioPlayerState.Paused {
            CSN.musicPlayer.resume()
        } else {
            if let item = CSN.musicPlayer.currentItem {
                CSN.musicPlayer.playItem(item)
            }
        }
    }
    
    @IBAction func pauseButtonTapped(sender: AnyObject) {
        if CSN.musicPlayer.state == AudioPlayerState.Playing {
            CSN.musicPlayer.pause()
        }
    }
    
    
    @IBAction func previousButtonTapped(sender: AnyObject) {
        CSN.musicPlayer.previous()
    }
    
    @IBAction func nextButtonTapped(sender: AnyObject) {
        CSN.musicPlayer.next()
    }
    
    @IBAction func repeatButtonTapped(sender: AnyObject) {
        let button = sender as! NIKFontAwesomeButton
        switch CSN.musicPlayer.mode {
        case AudioPlayerModeMask.Repeat:
            CSN.musicPlayer.mode = AudioPlayerModeMask.RepeatAll
            button.iconHex = "f079"
            break
        case AudioPlayerModeMask.RepeatAll:
            CSN.musicPlayer.mode = AudioPlayerModeMask.Shuffle
            button.iconHex = "f074"
            break
        case AudioPlayerModeMask.Shuffle:
            CSN.musicPlayer.mode = AudioPlayerModeMask()
            button.iconHex = "f178"
            break
        default:
            CSN.musicPlayer.mode = AudioPlayerModeMask.Repeat
            button.iconHex = "f021"
            break
        }
    }
    
    @IBAction func equalizerButtonTapped(sender: AnyObject) {
    }
    
    
    @IBAction func timerButtonTapped(sender: AnyObject) {
//        let bottomMenu = BottomMenuView(items: [
//            BottomMenuViewItem(title: NSLocalizedString("5 phút", comment: ""), selectedAction: { (Void) -> Void in
//                self.startTimerToTurnOffMusic(afterMinutes: 5)
//            }),
//            BottomMenuViewItem(title: NSLocalizedString("10 phút", comment: ""), selectedAction: { (Void) -> Void in
//                self.startTimerToTurnOffMusic(afterMinutes: 10)
//            }),
//            BottomMenuViewItem(title: NSLocalizedString("15 phút", comment: ""), selectedAction: { (Void) -> Void in
//                self.startTimerToTurnOffMusic(afterMinutes: 15)
//            }),
//            BottomMenuViewItem(title: NSLocalizedString("30 phút", comment: ""), selectedAction: { (Void) -> Void in
//                self.startTimerToTurnOffMusic(afterMinutes: 30)
//            }),
//            BottomMenuViewItem(title: NSLocalizedString("45 phút", comment: ""), selectedAction: { (Void) -> Void in
//                self.startTimerToTurnOffMusic(afterMinutes: 45)
//            }),
//            BottomMenuViewItem(title: NSLocalizedString("1 tiếng", comment: ""), selectedAction: { (Void) -> Void in
//                self.startTimerToTurnOffMusic(afterMinutes: 60)
//            }),
//            BottomMenuViewItem(title: NSLocalizedString("2 tiếng", comment: ""), selectedAction: { (Void) -> Void in
//                self.startTimerToTurnOffMusic(afterMinutes: 120)
//            }),
//            BottomMenuViewItem(title: NSLocalizedString("3 tiếng", comment: ""), selectedAction: { (Void) -> Void in
//                self.startTimerToTurnOffMusic(afterMinutes: 180)
//            }),
//            BottomMenuViewItem(title: NSLocalizedString("5 tiếng", comment: ""), selectedAction: { (Void) -> Void in
//                self.startTimerToTurnOffMusic(afterMinutes: 300)
//            }),
//            BottomMenuViewItem(title: NSLocalizedString("Mình yêu nhau đi - Forever", comment: ""), selectedAction: { (Void) -> Void in
//                self.timerToTurnOff.invalidate()
//            }),
//            ], didSelectedHandler: nil)
//        bottomMenu.showInViewController(self)
    }
    
    /*
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    
    /**
    This method is called when the audio player changes its state.
    A fresh created audioPlayer starts in `.Stopped` mode.
    
    - parameter audioPlayer: The audio player.
    - parameter from:        The state before any changes.
    - parameter to:          The new state.
    */
    
    func audioPlayer(audioPlayer: AudioPlayer, didChangeStateFrom from: AudioPlayerState, toState to: AudioPlayerState) {
        NSNotificationCenter.defaultCenter().postNotificationName(CSN.Notification.musicPlayerStateChanged, object: nil, userInfo: nil)
    }
    
    /**
     This method is called when the audio player is about to start playing
     a new item.
     
     - parameter audioPlayer: The audio player.
     - parameter item:        The item that is about to start being played.
     */
    func audioPlayer(audioPlayer: AudioPlayer, willStartPlayingItem item: Music) {
        
    }
    
    /**
     This method is called a regular time interval while playing. It notifies
     the delegate that the current playing progression changed.
     
     - parameter audioPlayer:    The audio player.
     - parameter time:           The current progression.
     - parameter percentageRead: The percentage of the file that has been read.
     It's a Float value between 0 & 100 so that you can
     easily update an `UISlider` for example.
     */
    func audioPlayer(audioPlayer: AudioPlayer, didUpdateProgressionToTime time: NSTimeInterval, percentageRead: Float) {
        self.totalTimeLabel.text = CSN.formatTimeFromSeconds(Double(CSN.musicPlayer.currentItemDuration!))
        self.currentTimeLabel.text = CSN.formatTimeFromSeconds(Double(CSN.musicPlayer.currentItemProgression!))
        self.musicSlider.value = Float(CSN.musicPlayer.currentItemProgression!)
        self.musicSlider.maximumValue = Float(CSN.musicPlayer.currentItemDuration!)
        self.musicSlider.minimumValue = 0
        
        if CSN.musicPlayer.state == AudioPlayerState.Playing {
            self.pauseButton.hidden = false
            self.playButton.hidden = true
        } else {
            self.pauseButton.hidden = true
            self.playButton.hidden = false
        }
    }
    
    /**
     This method gets called when the current item duration has been found.
     
     - parameter audioPlayer: The audio player.
     - parameter duration:    Current item's duration.
     - parameter item:        Current item.
     */
    func audioPlayer(audioPlayer: AudioPlayer, didFindDuration duration: NSTimeInterval, forItem item: Music) {
        
    }
    
    /**
     This methods gets called before duration gets updated with discovered metadata.
     
     - parameter audioPlayer: The audio player.
     - parameter item:        Found metadata.
     - parameter data:        Current item.
     */
    func audioPlayer(audioPlayer: AudioPlayer, didUpdateEmptyMetadataOnItem item: Music, withData data: Metadata) {
        
    }
    
    /**
     This method gets called while the audio player is loading the file (over
     the network or locally). It lets the delegate know what time range has
     already been loaded.
     
     - parameter audioPlayer: The audio player.
     - parameter range:       The time range that the audio player loaded.
     - parameter item:        Current item.
     */
    func audioPlayer(audioPlayer: AudioPlayer, didLoadRange range: AudioPlayer.TimeRange, forItem item: Music) {
        
    }

}


extension MusicPlayerViewController: UIPageViewControllerDataSource, UIPageViewControllerDelegate {
    
    func pageViewController(pageViewController: UIPageViewController, viewControllerAfterViewController viewController: UIViewController) -> UIViewController? {
        let vc = viewController as! MusicPlayerPageViewController
        let idx = vc.indexPage + 1
        if idx < pageViews.count {
            return pageViews[idx]
        } else {
            return nil
        }
    }
    
    func pageViewController(pageViewController: UIPageViewController, viewControllerBeforeViewController viewController: UIViewController) -> UIViewController? {
        let vc = viewController as! MusicPlayerPageViewController
        let idx = vc.indexPage - 1
        if idx >= 0 {
            return pageViews[idx]
        } else {
            return nil
        }
    }
    
    func pageViewController(pageViewController: UIPageViewController, willTransitionToViewControllers pendingViewControllers: [UIViewController]) {
        let vc = pendingViewControllers[0] as! MusicPlayerPageViewController
        pageControl.currentPage = vc.indexPage
        currentIndex = vc.indexPage
    }
    
}
