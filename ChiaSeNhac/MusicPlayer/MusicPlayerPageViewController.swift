//
//  MusicPlayerPageViewController.swift
//  ChiaSeNhac
//
//  Created by Vũ Trung Thành on 12/16/15.
//  Copyright © 2015 V2T Multimedia. All rights reserved.
//

import UIKit

class MusicPlayerPageViewController: V2TViewController {

    var indexPage: Int = 0 {
        didSet {
            switch indexPage {
            case 0:
                self.performSegueWithIdentifier("embedMusicTrackListView", sender: nil)
                break
            case 1:
                self.performSegueWithIdentifier("embedMusicPlayerMainView", sender: nil)
                break
            case 2:
                self.performSegueWithIdentifier("embedMusicLyricsView", sender: nil)
                break
            default:
                self.performSegueWithIdentifier("embedMusicPlayerMainView", sender: nil)
                break
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    


    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier != nil {
            let desnitationViewController = segue.destinationViewController
            self.addChildViewController(desnitationViewController)
            self.view.addSubview(desnitationViewController.view)
            desnitationViewController.didMoveToParentViewController(self)
            
            desnitationViewController.view.translatesAutoresizingMaskIntoConstraints = false
            
            self.view.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|[desnitationView]|", options: NSLayoutFormatOptions(), metrics: nil, views: ["desnitationView": desnitationViewController.view]))
            self.view.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:|[desnitationView]|", options: NSLayoutFormatOptions(), metrics: nil, views: ["desnitationView": desnitationViewController.view]))
        }
    }


}
