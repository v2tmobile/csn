//
//  MusicPlayerMainViewController.swift
//  ChiaSeNhac
//
//  Created by Vũ Trung Thành on 12/17/15.
//  Copyright © 2015 V2T Multimedia. All rights reserved.
//

import UIKit
import FontAwesomeIconFactory
import Haneke

class MusicPlayerMainViewController: V2TViewController {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var artistLabel: UILabel!
    @IBOutlet weak var likeButton: NIKFontAwesomeButton!
    @IBOutlet weak var downloadButton: NIKFontAwesomeButton!
    @IBOutlet weak var coverImageView: UIImageView!
    
    @IBOutlet weak var coverImageConstraint: NSLayoutConstraint!
    
    var music: Music?
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        if UIScreen.mainScreen().bounds.height == 480 {
            self.coverImageConstraint.constant = 0
        }
        
        self.coverImageView.layer.cornerRadius = UIScreen.mainScreen().bounds.width * 0.33
        self.coverImageView.clipsToBounds = true
        // Do any additional setup after loading the view.
        
        self.updatePlayerInfo()
        
        self.runSpinAnimationOn(self.coverImageView, duration: 1, rotation: M_PI / 2 / 60, loop: MAXFLOAT)
        
        self.music = CSN.musicPlayer.currentItem
        self.updatePlayerInfo()
        
        self.registerObserver(CSN.Notification.startPlayingSong, object: nil, queue: nil) { (noti) -> Void in
            self.music = CSN.musicPlayer.currentItem
            self.updatePlayerInfo()
        }
        
        self.coverImageView.registerObserver(CSN.Notification.musicPlayerStateChanged, object: nil, queue: nil) { (noti) -> Void in
            if (CSN.musicPlayer.state != AudioPlayerState.Playing) {
                self.pauseAnimation(self.coverImageView.layer)
            } else {
                self.resumeAnimation(self.coverImageView.layer)
            }
        }
        
        if (CSN.Setting.isShowDownloadFunction) {
            downloadButton.hidden = false
        } else {
            downloadButton.hidden = true
        }
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        let tracker = GAI.sharedInstance().defaultTracker
        tracker.set(kGAIScreenName, value: "Main music player")
        
        let builder = GAIDictionaryBuilder.createScreenView()
        tracker.send(builder.build() as [NSObject : AnyObject])
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func updatePlayerInfo() {
        if let thumbURL = CSN.musicPlayer.currentItem?.musicImg {
            self.coverImageView.hnk_setImageFromURL(NSURL(string: thumbURL)!)
        }
        self.titleLabel.text = CSN.musicPlayer.currentItem?.musicTitle
        self.artistLabel.text = CSN.musicPlayer.currentItem?.musicArtist
    }
    
    func runSpinAnimationOn(view: UIView, duration: Double, rotation: Double, loop: Float) {
        let animation = CABasicAnimation(keyPath: "transform.rotation.z")
        animation.toValue = NSNumber(double: M_PI * 2.0 * rotation * duration)
        animation.duration = duration
        animation.cumulative = true
        animation.repeatCount = loop
        animation.removedOnCompletion = false
        animation.fillMode = kCAFillModeForwards;
        view.layer.addAnimation(animation, forKey: "rotationAnimation")
    }
    
    func pauseAnimation(layer: CALayer) {
        let pausedTime = layer.convertTime(CACurrentMediaTime(), fromLayer: nil)
        layer.speed = 0
        layer.timeOffset = pausedTime
    }
    
    func resumeAnimation(layer: CALayer) {
        let pausedTime = layer.timeOffset
        if (pausedTime > 0) {
            layer.speed = 1
            layer.timeOffset = 0
            layer.beginTime = layer.convertTime(CACurrentMediaTime(), fromLayer: nil) - pausedTime
        } else {
            self.runSpinAnimationOn(self.coverImageView, duration: 1, rotation: M_PI / 2 / 60, loop: MAXFLOAT)
        }
    }
    
    @IBAction func likeButtonTapped(sender: AnyObject) {
        if let music = self.music {
            CSN.saveFavorite(music)
        } else {
            return
        }
        
    }
    @IBAction func downloadButtonTapped(sender: AnyObject) {
        if self.music == nil {return}
        NSNotificationCenter.defaultCenter().postNotificationName(CSN.Notification.showDownloadMusic, object: self.music)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
