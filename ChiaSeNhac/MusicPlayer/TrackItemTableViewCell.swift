//
//  TrackItemTableViewCell.swift
//  ChiaSeNhac
//
//  Created by Vũ Trung Thành on 12/19/15.
//  Copyright © 2015 V2T Multimedia. All rights reserved.
//

import UIKit
import FontAwesomeIconFactory

class TrackItemTableViewCell: UITableViewCell {

    @IBOutlet weak var playButton: NIKFontAwesomeButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var artistLabel: UILabel!
    @IBOutlet weak var trackNumberLabel: UILabel!
    
    var music: Music? {
        didSet {
            if let music = music {
                titleLabel.text = music.musicTitle
                artistLabel.text = music.musicArtist
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
