//
//  MusicPlayer.swift
//  ChiaSeNhac
//
//  Created by Vũ Trung Thành on 12/20/15.
//  Copyright © 2015 V2T Multimedia. All rights reserved.
//

import UIKit
import SwiftyJSON
import StreamingKit

public protocol MusicPlayerDelegate {
    /// Raised when an item has started playing
    func musicPlayer(musicPlayer: MusicPlayer!, didStartPlayingQueueItemId queueItemId: NSObject!)
    /// Raised when an item has finished buffering (may or may not be the currently playing item)
    /// This event may be raised multiple times for the same item if seek is invoked on the player
    func musicPlayer(musicPlayer: MusicPlayer!, didFinishBufferingSourceWithQueueItemId queueItemId: NSObject!)
    /// Raised when the state of the player has changed
    func musicPlayer(musicPlayer: MusicPlayer!, stateChanged state: STKAudioPlayerState, previousState: STKAudioPlayerState)
    /// Raised when an item has finished playing
    func musicPlayer(musicPlayer: MusicPlayer!, didFinishPlayingQueueItemId queueItemId: NSObject!, withReason stopReason: STKAudioPlayerStopReason, andProgress progress: Double, andDuration duration: Double)
    /// Raised when an unexpected and possibly unrecoverable error has occured (usually best to recreate the STKAudioPlauyer)
    func musicPlayer(musicPlayer: MusicPlayer!, unexpectedError errorCode: STKAudioPlayerErrorCode)
    
    /// Optionally implemented to get logging information from the STKAudioPlayer (used internally for debugging)
    func musicPlayer(musicPlayer: MusicPlayer!, logInfo line: String!)
    /// Raised when items queued items are cleared (usually because of a call to play, setDataSource or stop)
    func musicPlayer(musicPlayer: MusicPlayer!, didCancelQueuedItems queuedItems: [AnyObject]!)
}

public class MusicPlayer: NSObject, STKAudioPlayerDelegate {
    
    public var delegate: MusicPlayerDelegate?
    
    public var audioPlayer = STKAudioPlayer()
    public var tracksList: [Music] = []
    public var currentPlaying: Music?
    public var loopType: LoopType = .RepeatAll
    
    override init() {
        super.init()
        self.registerObserver(CSN.Notification.startPlayingVideo, object: nil, queue: nil) { (noti) -> Void in
            self.audioPlayer.pause()
        }
        self.registerObserver(CSN.Notification.videoPlayerStateChanged, object: nil, queue: nil) { (noti) -> Void in
            if self.audioPlayer.state == STKAudioPlayerState.Playing {
                self.audioPlayer.pause()
            }
        }
        self.audioPlayer.delegate = self
        self.registerRemoteControlEvent()
    }
    
    public func play(music music: Music?, tracksList: [Music]?) {
        if let music = music {
            if music.path != ""{
                let fileManager = NSFileManager.defaultManager()
                let directoryURL = fileManager.URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask)[0]
                let dataPath = directoryURL.URLByAppendingPathComponent(music.path)
                debugPrint(dataPath)
                self.audioPlayer.playURL(dataPath)
            } else {
                
                var musicPath: String?
                switch CSN.Setting.musicQuality {
                case .Lossless:
                    musicPath = music.fileLosslessUrl
                    break
                case .M4A:
                    musicPath = music.fileM4aUrl
                    break
                case .k320:
                    musicPath = music.file320Url
                    break
                case .k128:
                    musicPath = music.fileUrl
                    break
                case .k32:
                    musicPath = music.file32Url
                    break
                }
                
                if musicPath == nil || musicPath == "" {
                    switch CSN.Setting.musicQuality {
                    case .Lossless:
                        musicPath = music.fileM4aUrl
                        break
                    case .M4A:
                        musicPath = music.file320Url
                        break
                    case .k320:
                        musicPath = music.fileUrl
                        break
                    case .k128:
                        musicPath = music.fileUrl
                        break
                    case .k32:
                        musicPath = music.fileUrl
                        break
                    }
                }
                
                if musicPath == nil || musicPath == "" {
                    switch CSN.Setting.musicQuality {
                    case .Lossless:
                        musicPath = music.file320Url
                        break
                    case .M4A:
                        musicPath = music.fileUrl
                        break
                    case .k320:
                        musicPath = music.fileUrl
                        break
                    case .k128:
                        musicPath = music.fileUrl
                        break
                    case .k32:
                        musicPath = music.fileUrl
                        break
                    }
                }
                
                if musicPath == nil || musicPath == "" {
                    musicPath = music.fileUrl
                }
                
                self.audioPlayer.play(musicPath!)
            }
            self.currentPlaying = music
            if let tracksList = tracksList{
                self.tracksList.removeAll()
                self.tracksList = tracksList
            }
        }
        
        if let title = music?.musicTitle {
            let tracker = GAI.sharedInstance().defaultTracker
            tracker.set(kGAIScreenName, value: "Bài hát: \(title)")
            
            let builder = GAIDictionaryBuilder.createScreenView()
            tracker.send(builder.build() as [NSObject : AnyObject])
        }
        
        NSNotificationCenter.defaultCenter().postNotificationName(CSN.Notification.startPlayingSong, object: nil)
        
        // Save History
        
        let musicsList = CSN.uiRealm.objects(Musics).filter("name = 'Histories'")[0].musics
        for item: Music in musicsList {
            if item.musicId == music!.musicId {
                do {
                    try CSN.uiRealm.write({ () -> Void in
                        item.createdAtDate = NSDate()
                    })
                    NSNotificationCenter.defaultCenter().postNotificationName(CSN.Notification.saveHistoryDone, object: music)
                } catch let error {
                    debugPrint(error)
                    // Handling write error,
                    // e.g. Delete the Realm file, etc.
                }
                return
            }
        }
        do {
            try CSN.uiRealm.write({ () -> Void in
                music?.createdAtDate = NSDate()
                musicsList.append(music!)
                NSNotificationCenter.defaultCenter().postNotificationName(CSN.Notification.saveHistoryDone, object: music)
            })
            
        } catch let error {
            debugPrint(error)
            // Handling write error,
            // e.g. Delete the Realm file, etc.
        }
        //

    }
    
    //
    func getCurrentIndexSong() -> Int {
        var currentIndex: Int = -1
        for i in 0..<CSN.musicPlayer.tracksList.count {
            if CSN.musicPlayer.currentPlaying?.musicId == CSN.musicPlayer.tracksList[i].musicId {
                currentIndex = i
            }
        }
        return currentIndex
    }
    
    func playNextSong() {
        var currentIndex = self.getCurrentIndexSong()
        if currentIndex == -1 || currentIndex == CSN.musicPlayer.tracksList.count - 1 {
            if loopType != .RepeatAll && loopType != .Random {
                return
            } else {
                currentIndex = -1
            }
        }
        
        var song: Music!
        
        if loopType == .RepeatOne {
            song = self.tracksList[currentIndex]
        } else if loopType == .Random {
            song = self.tracksList[Int(arc4random_uniform(UInt32(self.tracksList.count)))]
        } else {
            if currentIndex != -1 {
                song = self.tracksList[currentIndex + 1]
            } else {
                return
            }
        }
        
        if song.path != "" {
            CSN.musicPlayer.play(music: song, tracksList: CSN.musicPlayer.tracksList)
            return
        }
        
        CSN.API.listenMusic(music: song, success: { (result) -> Void in
            if result["music_info"] != nil {
                let music = Music.fromDictionary(result["music_info"].dictionaryObject!)
                CSN.musicPlayer.play(music: music, tracksList: CSN.musicPlayer.tracksList)
            }
            }) { (error) -> Void in
                //
        }
    }
    
    func playPreviousSong() {
        var currentIndex = self.getCurrentIndexSong()
        if currentIndex < 1 {
            if loopType != .RepeatAll {
                return
            } else {
                currentIndex = self.tracksList.count
            }
        }
        
        var song: Music!
        
        if loopType == .RepeatOne {
            song = self.tracksList[currentIndex]
        } else if loopType == .Random {
            song = self.tracksList[Int(arc4random_uniform(UInt32(self.tracksList.count + 1)))]
        } else {
            if currentIndex != 0 {
                song = self.tracksList[currentIndex - 1]
            } else {
                return
            }
        }
        
        if song.path != "" {
            CSN.musicPlayer.play(music: song, tracksList: CSN.musicPlayer.tracksList)
            return
        }
        
        CSN.API.listenMusic(music: song, success: { (result) -> Void in
            if result["music_info"] != nil {
                let music = Music.fromDictionary(result["music_info"].dictionaryObject!)
                CSN.musicPlayer.play(music: music, tracksList: CSN.musicPlayer.tracksList)
            }
            }) { (error) -> Void in
                //
        }
    }
    
    /// Raised when an item has started playing
    public func audioPlayer(audioPlayer: STKAudioPlayer, didStartPlayingQueueItemId queueItemId: NSObject) {
        debugPrint("start playing...")
        debugPrint("set music info remote control")
        var songInfo: [String: AnyObject] = Dictionary()
        songInfo[MPMediaItemPropertyTitle] = CSN.musicPlayer.currentPlaying?.musicTitle
        songInfo[MPMediaItemPropertyArtist] = CSN.musicPlayer.currentPlaying?.musicArtist
        songInfo[MPNowPlayingInfoPropertyPlaybackRate] = 1.0
        songInfo[MPNowPlayingInfoPropertyElapsedPlaybackTime] = CSN.musicPlayer.audioPlayer.progress
        songInfo[MPMediaItemPropertyPlaybackDuration] = CSN.musicPlayer.audioPlayer.duration
        if let imageURL = CSN.musicPlayer.currentPlaying?.musicImg {
            let url = NSURL(string: imageURL)
            if let data = NSData(contentsOfURL: url!) {
                let image = UIImage(data: data)
                songInfo[MPMediaItemPropertyArtwork] = MPMediaItemArtwork(image: image!)
            }
        }
        
        MPNowPlayingInfoCenter.defaultCenter().nowPlayingInfo = songInfo
        
        self.delegate?.musicPlayer(self, didStartPlayingQueueItemId: queueItemId)
    }
    /// Raised when an item has finished buffering (may or may not be the currently playing item)
    /// This event may be raised multiple times for the same item if seek is invoked on the player
    public func audioPlayer(audioPlayer: STKAudioPlayer, didFinishBufferingSourceWithQueueItemId queueItemId: NSObject){
        self.delegate?.musicPlayer(self, didFinishBufferingSourceWithQueueItemId: queueItemId)
    }
    /// Raised when the state of the player has changed
    public func audioPlayer(audioPlayer: STKAudioPlayer, stateChanged state: STKAudioPlayerState, previousState: STKAudioPlayerState){
        self.delegate?.musicPlayer(self, stateChanged: state, previousState: previousState)
    }
    /// Raised when an item has finished playing
    public func audioPlayer(audioPlayer: STKAudioPlayer, didFinishPlayingQueueItemId queueItemId: NSObject, withReason stopReason: STKAudioPlayerStopReason, andProgress progress: Double, andDuration duration: Double) {
        debugPrint(stopReason)
        if stopReason == STKAudioPlayerStopReason.Eof ||
            stopReason == STKAudioPlayerStopReason.Error {
                CSN.musicPlayer.playNextSong()
        }
        self.delegate?.musicPlayer(self, didFinishPlayingQueueItemId: queueItemId, withReason: stopReason, andProgress: progress, andDuration: duration)
    }
    /// Raised when an unexpected and possibly unrecoverable error has occured (usually best to recreate the STKAudioPlauyer)
    public func audioPlayer(audioPlayer: STKAudioPlayer, unexpectedError errorCode: STKAudioPlayerErrorCode) {
        self.delegate?.musicPlayer(self, unexpectedError: errorCode)
    }
    
    /// Optionally implemented to get logging information from the STKAudioPlayer (used internally for debugging)
    public func audioPlayer(audioPlayer: STKAudioPlayer, logInfo line: String) {
        self.delegate?.musicPlayer(self, logInfo: line)
    }
    /// Raised when items queued items are cleared (usually because of a call to play, setDataSource or stop)
    public func audioPlayer(audioPlayer: STKAudioPlayer, didCancelQueuedItems queuedItems: [AnyObject]) {
        self.delegate?.musicPlayer(self, didCancelQueuedItems: queuedItems)
    }
    
    public enum LoopType: Int {
        case RepeatOne
        case RepeatAll
        case Random
        case None
    }
    
    func registerRemoteControlEvent() {
        let commandCetner = MPRemoteCommandCenter.sharedCommandCenter()
        commandCetner.playCommand.addTarget(self, action: "didReceivePlayCommand:")
        commandCetner.pauseCommand.addTarget(self, action: "didReceivePauseCommand:")
        commandCetner.stopCommand.addTarget(self, action: "didReceiveStopCommand:")
        commandCetner.nextTrackCommand.addTarget(self, action: "didReceiveNextTrackCommand:")
        commandCetner.previousTrackCommand.addTarget(self, action: "didReceivePreviousTrackCommand:")
    }
    
    func didReceivePlayCommand(command: AnyObject) {
        if self.audioPlayer.state == STKAudioPlayerState.Paused {
            self.audioPlayer.resume()
        }
    }
    
    func didReceivePauseCommand(command: AnyObject) {
        self.audioPlayer.pause()
    }
    
    func didReceiveStopCommand(coomand: AnyObject) {
        self.audioPlayer.stop()
    }
    
    func didReceiveNextTrackCommand(command: AnyObject) {
        self.playNextSong()
    }
    
    func didReceivePreviousTrackCommand(command: AnyObject) {
        self.playPreviousSong()
    }
}
