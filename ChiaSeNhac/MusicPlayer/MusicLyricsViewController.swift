//
//  MusicLyricsViewController.swift
//  ChiaSeNhac
//
//  Created by Vũ Trung Thành on 12/17/15.
//  Copyright © 2015 V2T Multimedia. All rights reserved.
//

import UIKit

class MusicLyricsViewController: V2TViewController {

    @IBOutlet weak var lyricsLabel: UILabel!
    @IBOutlet weak var scrollView: UIScrollView!
    
    var music: Music?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if !CSN.Setting.isShowThirdPartyContent {
            self.lyricsLabel.hidden = true
        } else {
            self.lyricsLabel.hidden = false
        }
        
        self.music = CSN.musicPlayer.currentItem
        self.lyricsLabel.text = self.music?.musicLyric
        self.lyricsLabel.numberOfLines = 0
        self.lyricsLabel.lineBreakMode = NSLineBreakMode.ByWordWrapping
        self.registerObserver(CSN.Notification.startPlayingSong, object: nil, queue: nil) { (noti) -> Void in
            self.music = CSN.musicPlayer.currentItem
            self.lyricsLabel.text = self.music?.musicLyric
        }
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        let tracker = GAI.sharedInstance().defaultTracker
        tracker.set(kGAIScreenName, value: "Lời bài hát")
        
        let builder = GAIDictionaryBuilder.createScreenView()
        tracker.send(builder.build() as [NSObject : AnyObject])
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
