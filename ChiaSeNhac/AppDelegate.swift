//
//  AppDelegate.swift
//  ChiaSeNhac
//
//  Created by Vũ Trung Thành on 12/3/15.
//  Copyright © 2015 V2T Multimedia. All rights reserved.
//

import UIKit
import AVFoundation
import Haneke
import iRate


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, V2TAdsDeletage {

    var window: UIWindow?


    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        
        sharedV2TAds.getAdsInfo(nil)
//        sharedV2TAds.delegate = self
        
        #if DEBUG
        FBAdSettings.addTestDevice("c01bad3f813cc6fdc2e0a310ba909ebf37f0ec4f")
        #endif
        
        // Google analytics
        
        // Configure tracker from GoogleService-Info.plist.
        var configureError:NSError?
        GGLContext.sharedInstance().configureWithError(&configureError)
        assert(configureError == nil, "Error configuring Google services: \(configureError)")
        
        // Optional: configure GAI options.
        let gai = GAI.sharedInstance()
        gai.dispatchInterval = 10
        gai.trackUncaughtExceptions = true  // report uncaught exceptions
        #if DEBUG
        gai.logger.logLevel = GAILogLevel.Verbose  // remove before app release
        #endif
        
        // Start audio session
        do {
            try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback)
        } catch {
            
        }
        // remote control
        UIApplication.sharedApplication().beginReceivingRemoteControlEvents()
        
        // Remove cache if networking is avaiable
        if V2TReachability.isConnectedToNetwork() == true {
            Shared.JSONCache.removeAll()
        } else {
//            debugPrint("Internet connection FAILED")
//            let alert = UIAlertView()
//            alert.title = NSLocalizedString("Lỗi", comment: "")
//            alert.message = NSLocalizedString("Có thế đã xảy ra lỗi về kết nối mạng, vui lòng kiểm tra và thử lại sau.\nXin cảm ơn.", comment: "")
//            alert.addButtonWithTitle("OK")
//            alert.show()
        }
        
        let defaults = NSUserDefaults.standardUserDefaults()
        if defaults.stringForKey("isFirstInstall") == nil {
            // Created DowloadList and Favorites list if not exits
            let downloadList = Musics()
            downloadList.name = "Downloads"
            let favoriteList = Musics()
            favoriteList.name = "Favorites"
            let historiesList = Musics()
            historiesList.name = "Histories"
        
            do {
                try CSN.uiRealm.write({ () -> Void in
                    CSN.uiRealm.add(downloadList)
                    CSN.uiRealm.add(favoriteList)
                    CSN.uiRealm.add(historiesList)
                    let defaults = NSUserDefaults.standardUserDefaults()
                    defaults.setObject(1, forKey: "isFirstInstall")
                })
                
            } catch let error {
                debugPrint(error)
                // Handling write error,
                // e.g. Delete the Realm file, etc.
            }
            
            // Set default setting
            
            CSN.Setting.isPauseVideoInBackground = false
            CSN.Setting.shakeToNextSong = true
            CSN.Setting.musicQuality = .k320
            CSN.Setting.videoQuality = .k480p
        }
        
        // iRate
        iRate.sharedInstance().appStoreID = 1071671579
//        iRate.sharedInstance().previewMode = true
        
        return true
    }

    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }

    func v2tAdsDidGetAdsInfo(v2tAds: V2TAds) {
        sharedV2TAds.showSplashScreenAds()
    }
}

