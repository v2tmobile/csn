//
//  OfflineTableViewController.swift
//  ChiaSeNhac
//
//  Created by Phan Hữu Thắng on 1/3/16.
//  Copyright © 2016 V2T Multimedia. All rights reserved.
//

import UIKit
import FontAwesome_swift
import UIColor_Hex_Swift
import MessageUI

class OfflineTableViewController: V2TTableViewController, MFMailComposeViewControllerDelegate {
    private let reuseCellIdentifier: String = "OfflineCell"
    var identifier: String!
    var listSelect: [[[String: String]]] = [
        
        [["title": "Nhạc yêu thích", "segue": "showFavoritedList"],
        ["title": "Nhạc trong máy", "segue": "showDownloadedList"],
        ["title": "Playlist", "segue": "showPlaylist"],
        ["title": "Lịch sử", "segue": "showHistories"]],
        
        [["title": "Cài đặt", "segue": "showSettings"],
        ["title": "Hướng dẫn", "segue": "showTutorial"],
        ["title": "Góp ý & Liên hệ", "segue": "showAbouts"],
        ["title": "Đánh giá ứng dụng", "segue": "showRate"],
        ["title": "Ứng dụng khác", "segue": "showMoreApps"]]
    ]
    
    var icon: [[UIImage]] = [[
        
        UIImage.fontAwesomeIconWithName(FontAwesome.Heart, textColor: UIColor(rgba: "#d32f2f"), size: CGSize(width: 24, height: 24)),
        UIImage.fontAwesomeIconWithName(FontAwesome.Download, textColor: UIColor.grayColor(), size: CGSize(width: 24, height: 24)),
        UIImage.fontAwesomeIconWithName(FontAwesome.List, textColor: UIColor.grayColor(), size: CGSize(width: 24, height: 24)),
        UIImage.fontAwesomeIconWithName(FontAwesome.History, textColor: UIColor.grayColor(), size: CGSize(width: 24, height: 24))],
        
        [UIImage.fontAwesomeIconWithName(FontAwesome.Gear, textColor: UIColor.grayColor(), size: CGSize(width: 24, height: 24)),
        UIImage.fontAwesomeIconWithName(FontAwesome.Book, textColor: UIColor.grayColor(), size: CGSize(width: 24, height: 24)),
        UIImage.fontAwesomeIconWithName(FontAwesome.Info, textColor: UIColor.grayColor(), size: CGSize(width: 24, height: 24)),
        UIImage.fontAwesomeIconWithName(FontAwesome.Star, textColor: UIColor(rgba: "#FFEB3B"), size: CGSize(width: 24, height: 24)),
        UIImage.fontAwesomeIconWithName(FontAwesome.CartPlus, textColor: UIColor(rgba: "#4CAF50"), size: CGSize(width: 24, height: 24))]
        ]
                
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        self.navigationController?.navigationBar.barTintColor = UIColor(rgba: "#d32f2f")
        self.navigationController?.navigationBar.translucent = false
        
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        let tracker = GAI.sharedInstance().defaultTracker
        tracker.set(kGAIScreenName, value: "Thư viện")
        
        let builder = GAIDictionaryBuilder.createScreenView()
        tracker.send(builder.build() as [NSObject : AnyObject])
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return self.listSelect.count
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return self.listSelect[section].count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier(reuseCellIdentifier, forIndexPath: indexPath) as UITableViewCell
        cell.textLabel?.text = self.listSelect[indexPath.section][indexPath.row]["title"]
        cell.imageView?.image = self.icon[indexPath.section][indexPath.row]
        return cell
    }

    override func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        var headerName : String
        switch section {
        case 0:
            headerName = ""
        case 1:
            headerName = "Thêm"
        default:
            headerName = ""
        }
        return headerName
    }

    override func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0 {
            return 0
        }
        return 32
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        self.tableView.deselectRowAtIndexPath(indexPath, animated: true)
        
        if self.listSelect[indexPath.section][indexPath.row]["title"] == "Ứng dụng khác" {
            if let json = CSN.Setting.globalJSON {
                if json["more_apps"]["type"].intValue != 1 {
                    let url = NSURL(string: json["more_apps"]["link"].stringValue)!
                    UIApplication.sharedApplication().openURL(url)
                    return
                }
            }
        }
        
        if self.listSelect[indexPath.section][indexPath.row]["title"] == "Hướng dẫn" {
            if let json = CSN.Setting.globalJSON {
                let storyboard = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle())
                let webVC = storyboard.instantiateViewControllerWithIdentifier("WebViewController") as! V2TWebViewController
                webVC.linkString = json["tutorial_link"].stringValue
                webVC.title = NSLocalizedString("Hướng dẫn", comment: "")
                let navVC = V2TNavigationController(rootViewController: webVC)
                self.navigationController?.presentViewController(navVC, animated: true, completion: { () -> Void in
                    //
                })
                return
            }
        }
        
        if self.listSelect[indexPath.section][indexPath.row]["title"] == "Góp ý & Liên hệ" {
            let mailComposeViewController = configuredMailComposeViewController()
            if MFMailComposeViewController.canSendMail() {
                self.presentViewController(mailComposeViewController, animated: true, completion: nil)
            } else {
                self.showSendMailErrorAlert()
            }
            return
        }
        
        if self.listSelect[indexPath.section][indexPath.row]["title"] == "Đánh giá ứng dụng" {
            if let json = CSN.Setting.globalJSON {
                let url = NSURL(string: json["itunes_link"].stringValue)!
                UIApplication.sharedApplication().openURL(url)
                return
            }
        }
        
        self.performSegueWithIdentifier(self.listSelect[indexPath.section][indexPath.row]["segue"]!, sender: nil)
    }
    
    
    func configuredMailComposeViewController() -> MFMailComposeViewController {
        let mailComposerVC = MFMailComposeViewController()
        mailComposerVC.mailComposeDelegate = self // Extremely important to set the --mailComposeDelegate-- property, NOT the --delegate-- property
        
        mailComposerVC.setToRecipients(["support@v2t.mobi"])
        mailComposerVC.setSubject("[Chiasenhac-iOS] - Feedback")
        mailComposerVC.setMessageBody("Ứng dụng chia sẻ nhạc rất hay... ", isHTML: false)
        
        return mailComposerVC
    }
    
    func showSendMailErrorAlert() {
        let sendMailErrorAlert = UIAlertView(title: "Could Not Send Email", message: "Your device could not send e-mail.  Please check e-mail configuration and try again.", delegate: self, cancelButtonTitle: "OK")
        sendMailErrorAlert.show()
    }
    
    // MARK: MFMailComposeViewControllerDelegate Method
    func mailComposeController(controller: MFMailComposeViewController, didFinishWithResult result: MFMailComposeResult, error: NSError?) {
        controller.dismissViewControllerAnimated(true, completion: nil)
    }

    
}