//
//  SettingOfflineTableViewController.swift
//  ChiaSeNhac
//
//  Created by Phan Hữu Thắng on 1/5/16.
//  Copyright © 2016 V2T Multimedia. All rights reserved.
//

import UIKit
import FontAwesome_swift

class SettingOfflineTableViewController: V2TTableViewController, SwitchBoolCellDelegate {
    private let reuseCellIdentifierSetting: String = "settingCell"
    private let reuseCellIdentifierVideoPlay: String = "switchControlCell"

    
    var listSelect: [String] = ["Chất lượng nhạc","Chất lượng video","Cho phép video chạy nền", "Lắc để chuyển bài"]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.navigationItem.title = "Cài đặt"
        self.tableView.bounces = false

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    func backButtonTapped(button: UIButton) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        //        self.navigationController?.interactivePopGestureRecognizer?.enabled = false
        // Left navigation button
        let backButton = UIButton(type: .Custom)
        backButton.setImage(UIImage.fontAwesomeIconWithName(FontAwesome.AngleLeft, textColor: UIColor.whiteColor(), size: CGSize(width: 26, height: 26)), forState: .Normal)
        backButton.frame = CGRect(x: 0, y: 0, width: 32, height: 32)
        backButton.layer.cornerRadius = 16
        backButton.layer.borderColor = UIColor.whiteColor().CGColor
        backButton.layer.borderWidth = 1
        backButton.addTarget(self, action: "backButtonTapped:", forControlEvents: .TouchUpInside)
        let backBarButtonItem = UIBarButtonItem(customView: backButton)
        self.navigationItem.leftBarButtonItem = backBarButtonItem
        
        let tracker = GAI.sharedInstance().defaultTracker
        tracker.set(kGAIScreenName, value: "Cài đặt")
        
        let builder = GAIDictionaryBuilder.createScreenView()
        tracker.send(builder.build() as [NSObject : AnyObject])
    }

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listSelect.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        switch indexPath.row {
        case 2, 3:
            let cell = tableView.dequeueReusableCellWithIdentifier(reuseCellIdentifierVideoPlay, forIndexPath: indexPath) as! SwitchBoolTableViewCell
            cell.titleCell.text = self.listSelect[indexPath.row]
            cell.delegate = self
            if indexPath.row == 2 {
                cell.switchControl.on = !CSN.Setting.isPauseVideoInBackground
            }
            if indexPath.row == 3 {
                cell.switchControl.on = CSN.Setting.shakeToNextSong
            }
            return cell

        default:
            let cell = tableView.dequeueReusableCellWithIdentifier(reuseCellIdentifierSetting, forIndexPath: indexPath) as UITableViewCell
            cell.textLabel?.text = self.listSelect[indexPath.row]
            if self.listSelect[indexPath.row] == "Chất lượng nhạc" {
                cell.detailTextLabel?.text = CSN.Setting.musicQuality.rawValue
            }
            if self.listSelect[indexPath.row] == "Chất lượng video" {
                cell.detailTextLabel?.text = CSN.Setting.videoQuality.rawValue
            }
            return cell
        }
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        self.tableView.deselectRowAtIndexPath(indexPath, animated: true)
        let cell = self.tableView.cellForRowAtIndexPath(indexPath)
        switch indexPath.row {
            case 0:
                showMusicQualityOptions(atCell: cell)
                break
            case 1:
                showVideoQualityOptions(atCell: cell)
                break
        default: break
        }
    }
    
    func showMusicQualityOptions(atCell cell: UITableViewCell?) {
        let items = [
            BottomMenuViewItem(title: "Lossless", selectedAction: { (Void) -> Void in
                CSN.Setting.musicQuality = .Lossless
            }),
            BottomMenuViewItem(title: "M4A", selectedAction: { (Void) -> Void in
                CSN.Setting.musicQuality = .M4A
            }),
            BottomMenuViewItem(title: "320K", selectedAction: { (Void) -> Void in
                CSN.Setting.musicQuality = .k320
            }),
            BottomMenuViewItem(title: "128K", selectedAction: { (Void) -> Void in
                CSN.Setting.musicQuality = .k128
            }),
            BottomMenuViewItem(title: "32K", selectedAction: { (Void) -> Void in
                CSN.Setting.musicQuality = .k32
            })
        ]
        let bottomMenu = BottomMenuView(items: items) { (index) -> Void in
            debugPrint(items[index])
            if let cell = cell {
                cell.detailTextLabel?.text = items[index].title
            }
        }
//        bottomMenu.itemAgliment = .Center
        bottomMenu.showInViewController(self.tabBarController!)
    }

    func showVideoQualityOptions(atCell cell: UITableViewCell?) {
        let items = [
            BottomMenuViewItem(title: "1080p", selectedAction: { (Void) -> Void in
                CSN.Setting.videoQuality = .k1080p
            }),
            BottomMenuViewItem(title: "720p", selectedAction: { (Void) -> Void in
                CSN.Setting.videoQuality = .k720p
            }),
            BottomMenuViewItem(title: "480p", selectedAction: { (Void) -> Void in
                CSN.Setting.videoQuality = .k480p
            }),
            BottomMenuViewItem(title: "320p", selectedAction: { (Void) -> Void in
                CSN.Setting.videoQuality = .k320p
            }),
            BottomMenuViewItem(title: "240p", selectedAction: { (Void) -> Void in
                CSN.Setting.videoQuality = .k240p
            })
        ]
        let bottomMenu = BottomMenuView(items: items) { (index) -> Void in
            if let cell = cell {
                cell.detailTextLabel?.text = items[index].title
            }
        }
//        bottomMenu.itemAgliment = .Center
        bottomMenu.showInViewController(self.tabBarController!)
    }
    
    func switchBoolCellChanged(switchBoolCell: SwitchBoolTableViewCell, value: Bool) {
        let indexPath = self.tableView.indexPathForCell(switchBoolCell)
        if indexPath?.row == 2 {
            CSN.Setting.isPauseVideoInBackground = !value
        }
        if indexPath?.row == 3 {
            CSN.Setting.shakeToNextSong = value
        }
    }
    
}
