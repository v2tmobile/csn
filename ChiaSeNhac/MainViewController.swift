//
//  MainViewController.swift
//  ChiaSeNhac
//
//  Created by Vũ Trung Thành on 12/5/15.
//  Copyright © 2015 V2T Multimedia. All rights reserved.
//

import UIKit
import FontAwesome_swift
import ARNTransitionAnimator

class MainViewController: V2TTabBarViewController, UITabBarControllerDelegate {
    
    @IBOutlet var miniPlayerView: UIView!
    @IBOutlet weak var discImageView: UIImageView!
    
    var animator : ARNTransitionAnimator!
    var musicPlayerVC : MusicPlayerViewController!
    var selectedVC: UIViewController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = UIColor.whiteColor()
        
        self.selectedIndex = 1
        
        self.delegate = self
        
        self.selectedVC = tabBarController?.selectedViewController
        
        let storyboard = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle())
        self.musicPlayerVC = storyboard.instantiateViewControllerWithIdentifier("MusicPlayer") as? MusicPlayerViewController
        self.musicPlayerVC.modalPresentationStyle = .Custom
        self.musicPlayerVC.tapCloseButtonActionHandler = { [weak self] in
            self!.animator.interactiveType = .None
        }
        self.musicPlayerVC.dismissViewControllerAnimated(true, completion: nil)
        
        miniPlayerView.registerObserver(CSN.Notification.videoPlayerEnterFullScreen, object: nil, queue: nil) { (noti) -> Void in
            self.miniPlayerView.hidden = true
        }
        
        miniPlayerView.registerObserver(CSN.Notification.videoPlayerExitFullScreen, object: nil, queue: nil) { (noti) -> Void in
            self.miniPlayerView.hidden = false
        }
        
        addMiniPlayer(atIndex: 1)
        
        setupAnimator()
        
        self.runSpinAnimationOn(self.discImageView, duration: 1, rotation: M_PI / 2 / 60, loop: MAXFLOAT)
        
        self.registerObserver(CSN.Notification.startPlayingSong, object: nil, queue: nil) { (noti) -> Void in
            self.discImageView.image = UIImage(named: "LaunchImage")
            if let musicImg = CSN.musicPlayer.currentItem?.musicImg {
                self.discImageView.hnk_setImageFromURL(NSURL(string: musicImg)!)
            }
        }
        
        self.registerObserver(ReachabilityChangedNotification, object: nil, queue: nil) { (noti) -> Void in
            let reachability: Reachability = noti.object as! Reachability
            if reachability.isReachable() {
                let v2tCore = V2TCore(appName: "chiasenhac")
                v2tCore.getConfigs { (json) -> Void in
                    CSN.Setting.globalJSON = json
                    NSNotificationCenter.defaultCenter().postNotificationName(CSN.Notification.getAppConfigDone, object: nil)
                }
            }
        }
    }
    
    func runSpinAnimationOn(view: UIView, duration: Double, rotation: Double, loop: Float) {
        let animation = CABasicAnimation(keyPath: "transform.rotation.z")
        animation.toValue = NSNumber(double: M_PI * 2.0 * rotation * duration)
        animation.duration = duration
        animation.cumulative = true
        animation.repeatCount = loop
        animation.removedOnCompletion = false
        animation.fillMode = kCAFillModeForwards;
        view.layer.addAnimation(animation, forKey: "rotationAnimation")
    }
    
    func pauseAnimation(layer: CALayer) {
        let pausedTime = layer.convertTime(CACurrentMediaTime(), fromLayer: nil)
        layer.speed = 0
        layer.timeOffset = pausedTime
    }
    
    func resumeAnimation(layer: CALayer) {
        let pausedTime = layer.timeOffset
        if (pausedTime > 0) {
            layer.speed = 1
            layer.timeOffset = 0
            layer.beginTime = layer.convertTime(CACurrentMediaTime(), fromLayer: nil) - pausedTime
        } else {
            self.runSpinAnimationOn(self.discImageView, duration: 1, rotation: M_PI / 2 / 60, loop: MAXFLOAT)
        }
    }
    
    func addMiniPlayer(atIndex index: Int) {
        let vc = UIViewController()
        vc.tabBarItem = UITabBarItem(title: "", image: nil, tag: 0)
        vc.tabBarItem.enabled = false
        self.viewControllers?.insert(vc, atIndex: index)
        
        self.view.addSubview(self.miniPlayerView)
        self.miniPlayerView.translatesAutoresizingMaskIntoConstraints = false
        self.view.addConstraint(NSLayoutConstraint(item: self.miniPlayerView, attribute: .CenterX, relatedBy: .Equal, toItem: self.view, attribute: .CenterX, multiplier: 1.0, constant: 0))
        self.view.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:[miniPlayerView(88)]", options: NSLayoutFormatOptions(), metrics: nil, views: ["miniPlayerView": self.miniPlayerView]))
        self.view.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:[miniPlayerView(70)]|", options: NSLayoutFormatOptions(), metrics: nil, views: ["miniPlayerView": self.miniPlayerView]))
    }
    
    func setupAnimator() {
        self.animator = ARNTransitionAnimator(operationType: .Present, fromVC: self, toVC: musicPlayerVC)
        self.animator.usingSpringWithDamping = 0.4
        self.animator.gestureTargetView = self.miniPlayerView
        self.animator.interactiveType = .Present
        
        // Present
        
        self.animator.presentationBeforeHandler = { [weak self] (containerView: UIView, transitionContext:
            UIViewControllerContextTransitioning) in
            self?.animator.direction = .Top
            
            self?.musicPlayerVC.view.frame.origin.y = self!.miniPlayerView.frame.origin.y + self!.miniPlayerView.frame.size.height
            containerView.addSubview(self!.view)
            self?.view.addSubview(self!.musicPlayerVC.view)
            
            self?.view.layoutIfNeeded()
            self?.musicPlayerVC.view.layoutIfNeeded()
            
            // miniPlayerView
            let startOriginY = self!.miniPlayerView.frame.origin.y
            let endOriginY = -self!.miniPlayerView.frame.size.height
            let diff = -endOriginY + startOriginY
            
            self?.animator.presentationCancelAnimationHandler = { (containerView: UIView) in
                self?.miniPlayerView.frame.origin.y = startOriginY
                self?.musicPlayerVC.view.frame.origin.y = self!.miniPlayerView.frame.origin.y + self!.miniPlayerView.frame.size.height
                
                self?.musicPlayerVC.view.alpha = 1.0
                for subview in self!.miniPlayerView.subviews {
                    subview.alpha = 1.0
                }
            }
            
            self?.animator.presentationAnimationHandler = { [weak self] (containerView: UIView, percentComplete: CGFloat) in
                let _percentComplete = percentComplete >= 0 ? percentComplete : 0
                self?.miniPlayerView.frame.origin.y = startOriginY - (diff * _percentComplete)
                if self?.miniPlayerView.frame.origin.y < endOriginY {
                    self?.miniPlayerView.frame.origin.y = endOriginY
                }
                self?.musicPlayerVC.view.frame.origin.y = self!.miniPlayerView.frame.origin.y + self!.miniPlayerView.frame.size.height
                
                self?.musicPlayerVC.view.alpha = (1.0 * _percentComplete) + 0.5
                for subview in self!.miniPlayerView.subviews {
                    subview.alpha = 1.0 - (1.0 * percentComplete)
                }
            }
            
            self?.animator.presentationCompletionHandler = {(containerView: UIView, completeTransition: Bool) in
                if completeTransition {
                    self?.view.removeFromSuperview()
                    self?.musicPlayerVC.view.removeFromSuperview()
                    if let musicView = self?.musicPlayerVC.view {
                        containerView.addSubview(musicView)
                    }
                    self?.animator.interactiveType = .Dismiss
                    self?.animator.gestureTargetView = self?.musicPlayerVC.containerView
                    self?.animator.direction = .Bottom
                } else {
                    UIApplication.sharedApplication().keyWindow!.addSubview(self!.view)
                }
            }
        }
        
        // Dismiss
        
        self.animator.dismissalBeforeHandler = { [weak self] (containerView: UIView, transitionContext: UIViewControllerContextTransitioning) in
            containerView.addSubview(self!.view)
            self?.view.addSubview(self!.musicPlayerVC.view)
            
            self?.view.layoutSubviews()
            
            let startOriginY = 0 - CGRectGetHeight(self!.miniPlayerView.bounds)
            let endOriginY = CGRectGetHeight(self!.view.bounds) - self!.miniPlayerView.frame.size.height
            let diff = -startOriginY + endOriginY
            
            self?.animator.dismissalCancelAnimationHandler = { (containerView: UIView) in
                self?.miniPlayerView.frame.origin.y = startOriginY
                self?.musicPlayerVC.view.frame.origin.y = self!.miniPlayerView.frame.origin.y + self!.miniPlayerView.frame.size.height
                
                self?.musicPlayerVC.view.alpha = 1
                for subview in self!.miniPlayerView.subviews {
                    subview.alpha = 0.0
                }
            }
            
            self?.animator.dismissalAnimationHandler = {(containerView: UIView, percentComplete: CGFloat) in
                let _percentComplete = percentComplete >= -0.05 ? percentComplete : -0.05
                self?.miniPlayerView.frame.origin.y = startOriginY + (diff * _percentComplete)
                self?.musicPlayerVC.view.frame.origin.y = self!.miniPlayerView.frame.origin.y + self!.miniPlayerView.frame.size.height
                
                self!.musicPlayerVC.view.alpha = 1.0 - (1.0 * _percentComplete)
                for subview in self!.miniPlayerView.subviews {
                    subview.alpha = 1.0 * _percentComplete
                }
            }
            
            self?.animator.dismissalCompletionHandler = { (containerView: UIView, completeTransition: Bool) in
                if completeTransition {
                    self?.musicPlayerVC.view.removeFromSuperview()
                    self?.animator.gestureTargetView = self!.miniPlayerView
                    self?.animator.interactiveType = .Present
                    UIApplication.sharedApplication().keyWindow!.addSubview(self!.view)
                    self?.setNeedsStatusBarAppearanceUpdate()
                }
            }
        }
        
        self.musicPlayerVC.transitioningDelegate = self.animator
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
//    func discImageViewTapped(discImageView: UIImageView) {
//        let storyboard = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle())
//        let musicVC = storyboard.instantiateViewControllerWithIdentifier("MusicPlayer") as? MusicPlayerViewController
//        if musicVC != nil {
//            self.presentViewController(musicVC!, animated: true, completion: nil)
//        }
//    }
    
    func tabBarController(tabBarController: UITabBarController, shouldSelectViewController viewController: UIViewController) -> Bool {
        debugPrint("should select: " + String(tabBarController.selectedIndex))
        if selectedVC == viewController {
            return false
        }
        return true
    }
    
    func tabBarController(tabBarController: UITabBarController, didSelectViewController viewController: UIViewController) {
        debugPrint("didselect: " + String(tabBarController.selectedIndex))
        selectedVC = viewController
    }
    
    
    // Shake to play the next song
    override func motionEnded(motion: UIEventSubtype,
        withEvent event: UIEvent?) {
            
            if motion == .MotionShake{
                if CSN.Setting.shakeToNextSong {
                    CSN.musicPlayer.next()
                }
            }
            
    }
    
    /*
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    // Get the new view controller using segue.destinationViewController.
    // Pass the selected object to the new view controller.
    }
    */
    
}
