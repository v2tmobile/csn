//
//	Music.swift
//
//	Create by Thành Vũ Trung on 12/12/2015
//	Copyright © 2015 V2T Multimedia. All rights reserved.

import RealmSwift


public class Music : Object {
    
    dynamic var catId : String!
    dynamic var catLevel : String!
    dynamic var catSublevel : String!
    dynamic var coverId : String!
    dynamic var file320Url : String!
    dynamic var file32Url : String!
    dynamic var fileLosslessUrl : String!
    dynamic var fileM4aUrl : String!
    dynamic var fileUrl : String!
    dynamic var fullUrl : String!
    dynamic var music320Filesize : String!
    dynamic var music32Filesize : String!
    dynamic var musicAlbum : String!
    dynamic var musicAlbumId : String!
    dynamic var musicArtist : String!
    dynamic var musicBitrate : String!
    dynamic var musicComposer : String!
    dynamic var musicDownloads : String!
    dynamic var musicFilesize : String!
    dynamic var musicGenre : String!
    dynamic var musicHeight : String!
    dynamic var musicId : String!
    dynamic var musicImg : String!
    dynamic var musicImgHeight : String!
    dynamic var musicImgWidth : String!
    dynamic var musicLength : String!
    dynamic var musicListen : String!
    dynamic var musicLosslessFilesize : String!
    dynamic var musicLyric : String!
    dynamic var musicM4aFilesize : String!
    dynamic var musicProduction : String!
    dynamic var musicTime : String!
    dynamic var musicTitle : String!
    dynamic var musicTitleUrl : String!
    dynamic var musicUsername : String!
    dynamic var musicWidth : String!
    dynamic var musicYear : String!
    dynamic var videoPreview : String!
    dynamic var videoThumbnail : String!
    dynamic var artistFaceUrl : String!
    dynamic var thumbnailUrl : String!
    // Offline properties
    dynamic var watched: Int = 0 // O: isn't downloaded, 1: downloaded
    dynamic var favorited: Int = 0 // O: isn't downloaded, 1: downloaded
    dynamic var downloadStatus: Int = 0 // O: isn't downloaded, 1: downloaded
    dynamic var createdAtDate = NSDate()
    dynamic var path: String = ""
    
    
    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    class func fromDictionary(dictionary: NSDictionary) -> Music	{
        let this = Music()
        if let catIdValue = dictionary["cat_id"] as? String{
            this.catId = catIdValue
        }
        if let catLevelValue = dictionary["cat_level"] as? String{
            this.catLevel = catLevelValue
        }
        if let catSublevelValue = dictionary["cat_sublevel"] as? String{
            this.catSublevel = catSublevelValue
        }
        if let coverIdValue = dictionary["cover_id"] as? String{
            this.coverId = coverIdValue
        }
        if let file320UrlValue = dictionary["file_320_url"] as? String{
            this.file320Url = file320UrlValue
        }
        if let file32UrlValue = dictionary["file_32_url"] as? String{
            this.file32Url = file32UrlValue
        }
        if let fileLosslessUrlValue = dictionary["file_lossless_url"] as? String{
            this.fileLosslessUrl = fileLosslessUrlValue
        }
        if let fileM4aUrlValue = dictionary["file_m4a_url"] as? String{
            this.fileM4aUrl = fileM4aUrlValue
        }
        if let fileUrlValue = dictionary["file_url"] as? String{
            this.fileUrl = fileUrlValue
        }
        if let fullUrlValue = dictionary["full_url"] as? String{
            this.fullUrl = fullUrlValue
        }
        if let music320FilesizeValue = dictionary["music_320_filesize"] as? String{
            this.music320Filesize = music320FilesizeValue
        }
        if let music32FilesizeValue = dictionary["music_32_filesize"] as? String{
            this.music32Filesize = music32FilesizeValue
        }
        if let musicAlbumValue = dictionary["music_album"] as? String{
            this.musicAlbum = musicAlbumValue
        }
        if let musicAlbumIdValue = dictionary["music_album_id"] as? String{
            this.musicAlbumId = musicAlbumIdValue
        }
        if let musicArtistValue = dictionary["music_artist"] as? String{
            this.musicArtist = musicArtistValue
        }
        if let musicBitrateValue = dictionary["music_bitrate"] as? String{
            this.musicBitrate = musicBitrateValue
        }
        if let musicComposerValue = dictionary["music_composer"] as? String{
            this.musicComposer = musicComposerValue
        }
        if let musicDownloadsValue = dictionary["music_downloads"] as? String{
            this.musicDownloads = musicDownloadsValue
        }
        if let musicFilesizeValue = dictionary["music_filesize"] as? String{
            this.musicFilesize = musicFilesizeValue
        }
        if let musicGenreValue = dictionary["music_genre"] as? String{
            this.musicGenre = musicGenreValue
        }
        if let musicHeightValue = dictionary["music_height"] as? String{
            this.musicHeight = musicHeightValue
        }
        if let musicIdValue = dictionary["music_id"] as? String{
            this.musicId = musicIdValue
        }
        if let musicImgValue = dictionary["music_img"] as? String{
            this.musicImg = musicImgValue
        }
        if let musicImgHeightValue = dictionary["music_img_height"] as? String{
            this.musicImgHeight = musicImgHeightValue
        }
        if let musicImgWidthValue = dictionary["music_img_width"] as? String{
            this.musicImgWidth = musicImgWidthValue
        }
        if let musicLengthValue = dictionary["music_length"] as? String{
            this.musicLength = musicLengthValue
        }
        if let musicListenValue = dictionary["music_listen"] as? String{
            this.musicListen = musicListenValue
        }
        if let musicLosslessFilesizeValue = dictionary["music_lossless_filesize"] as? String{
            this.musicLosslessFilesize = musicLosslessFilesizeValue
        }
        if let musicLyricValue = dictionary["music_lyric"] as? String{
            this.musicLyric = musicLyricValue
        }
        if let musicM4aFilesizeValue = dictionary["music_m4a_filesize"] as? String{
            this.musicM4aFilesize = musicM4aFilesizeValue
        }
        if let musicProductionValue = dictionary["music_production"] as? String{
            this.musicProduction = musicProductionValue
        }
        if let musicTimeValue = dictionary["music_time"] as? String{
            this.musicTime = musicTimeValue
        }
        if let musicTitleValue = dictionary["music_title"] as? String{
            this.musicTitle = musicTitleValue
        }
        if let musicTitleUrlValue = dictionary["music_title_url"] as? String{
            this.musicTitleUrl = musicTitleUrlValue
        }
        if let musicUsernameValue = dictionary["music_username"] as? String{
            this.musicUsername = musicUsernameValue
        }
        if let musicWidthValue = dictionary["music_width"] as? String{
            this.musicWidth = musicWidthValue
        }
        if let musicYearValue = dictionary["music_year"] as? String{
            this.musicYear = musicYearValue
        }
        if let videoPreviewValue = dictionary["video_preview"] as? String{
            this.videoPreview = videoPreviewValue
        }
        if let videoThumbnailValue = dictionary["video_thumbnail"] as? String{
            this.videoThumbnail = videoThumbnailValue
        } else {
            if let videoThumbnailValue = dictionary["thumbnail"] as? String{
                this.videoThumbnail = videoThumbnailValue
            }
        }
        if let artistFaceUrlValue = dictionary["artist_face_url"] as? String{
            this.artistFaceUrl = artistFaceUrlValue
        }
        if let thumbnailUrlValue = dictionary["thumbnail_url"] as? String{
            this.thumbnailUrl = thumbnailUrlValue
        }
        return this
    }
    
    /**
     * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> NSDictionary
    {
        let dictionary = NSMutableDictionary()
        if catId != nil{
            dictionary["cat_id"] = catId
        }
        if catLevel != nil{
            dictionary["cat_level"] = catLevel
        }
        if catSublevel != nil{
            dictionary["cat_sublevel"] = catSublevel
        }
        if coverId != nil{
            dictionary["cover_id"] = coverId
        }
        if file320Url != nil{
            dictionary["file_320_url"] = file320Url
        }
        if file32Url != nil{
            dictionary["file_32_url"] = file32Url
        }
        if fileLosslessUrl != nil{
            dictionary["file_lossless_url"] = fileLosslessUrl
        }
        if fileM4aUrl != nil{
            dictionary["file_m4a_url"] = fileM4aUrl
        }
        if fileUrl != nil{
            dictionary["file_url"] = fileUrl
        }
        if fullUrl != nil{
            dictionary["full_url"] = fullUrl
        }
        if music320Filesize != nil{
            dictionary["music_320_filesize"] = music320Filesize
        }
        if music32Filesize != nil{
            dictionary["music_32_filesize"] = music32Filesize
        }
        if musicAlbum != nil{
            dictionary["music_album"] = musicAlbum
        }
        if musicAlbumId != nil{
            dictionary["music_album_id"] = musicAlbumId
        }
        if musicArtist != nil{
            dictionary["music_artist"] = musicArtist
        }
        if musicBitrate != nil{
            dictionary["music_bitrate"] = musicBitrate
        }
        if musicComposer != nil{
            dictionary["music_composer"] = musicComposer
        }
        if musicDownloads != nil{
            dictionary["music_downloads"] = musicDownloads
        }
        if musicFilesize != nil{
            dictionary["music_filesize"] = musicFilesize
        }
        if musicGenre != nil{
            dictionary["music_genre"] = musicGenre
        }
        if musicHeight != nil{
            dictionary["music_height"] = musicHeight
        }
        if musicId != nil{
            dictionary["music_id"] = musicId
        }
        if musicImg != nil{
            dictionary["music_img"] = musicImg
        }
        if musicImgHeight != nil{
            dictionary["music_img_height"] = musicImgHeight
        }
        if musicImgWidth != nil{
            dictionary["music_img_width"] = musicImgWidth
        }
        if musicLength != nil{
            dictionary["music_length"] = musicLength
        }
        if musicListen != nil{
            dictionary["music_listen"] = musicListen
        }
        if musicLosslessFilesize != nil{
            dictionary["music_lossless_filesize"] = musicLosslessFilesize
        }
        if musicLyric != nil{
            dictionary["music_lyric"] = musicLyric
        }
        if musicM4aFilesize != nil{
            dictionary["music_m4a_filesize"] = musicM4aFilesize
        }
        if musicProduction != nil{
            dictionary["music_production"] = musicProduction
        }
        if musicTime != nil{
            dictionary["music_time"] = musicTime
        }
        if musicTitle != nil{
            dictionary["music_title"] = musicTitle
        }
        if musicTitleUrl != nil{
            dictionary["music_title_url"] = musicTitleUrl
        }
        if musicUsername != nil{
            dictionary["music_username"] = musicUsername
        }
        if musicWidth != nil{
            dictionary["music_width"] = musicWidth
        }
        if musicYear != nil{
            dictionary["music_year"] = musicYear
        }
        if videoPreview != nil{
            dictionary["video_preview"] = videoPreview
        }
        if videoThumbnail != nil{
            dictionary["video_thumbnail"] = videoThumbnail
        }
        if artistFaceUrl != nil{
            dictionary["artist_face_url"] = artistFaceUrl
        }
        if thumbnailUrl != nil{
            dictionary["thumbnail_url"] = thumbnailUrl
        }
        return dictionary
    }
    
}

extension Music {
    public var soundURLs: [AudioQuality: NSURL] {
        var URLs = [AudioQuality: NSURL]()
        if let losslessURL = NSURL(string: self.fileLosslessUrl) {
            URLs[.Lossless] = losslessURL
        }
        if let highURL = NSURL(string: self.fileM4aUrl) {
            URLs[.High] = highURL
        }
        if let mediumURL = NSURL(string: self.file320Url) {
            URLs[.Medium] = mediumURL
        }
        if let lowURL = NSURL(string: self.file32Url) {
            URLs[.Low] = lowURL
        }
        return URLs
    }
    
    // MARK: Quality selection
    /// Returns the lossless quality URL found or nil if no URLs are available
    public var losslessQualityURL: AudioItemURL {
        return (AudioItemURL(quality: .Lossless, URL: soundURLs[.Lossless]) ??
            AudioItemURL(quality: .High, URL: soundURLs[.High]) ??
            AudioItemURL(quality: .Medium, URL: soundURLs[.Medium]) ??
            AudioItemURL(quality: .Low, URL: soundURLs[.Low]))!
    }
    
    /// Returns the highest quality URL found or nil if no URLs are available
    public var highQualityURL: AudioItemURL {
        return (AudioItemURL(quality: .High, URL: soundURLs[.High]) ??
            AudioItemURL(quality: .Medium, URL: soundURLs[.Medium]) ??
            AudioItemURL(quality: .Low, URL: soundURLs[.Low])) ??
            AudioItemURL(quality: .Lossless, URL: soundURLs[.Lossless])!
    }
    
    /// Returns the medium quality URL found or nil if no URLs are available
    public var mediumQualityURL: AudioItemURL {
        return (AudioItemURL(quality: .Medium, URL: soundURLs[.Medium]) ??
            AudioItemURL(quality: .Low, URL: soundURLs[.Low]) ??
            AudioItemURL(quality: .High, URL: soundURLs[.High])) ??
            AudioItemURL(quality: .Lossless, URL: soundURLs[.Lossless])!
    }
    
    /// Returns the lowest quality URL found or nil if no URLs are available
    public var lowestQualityURL: AudioItemURL {
        return (AudioItemURL(quality: .Low, URL: soundURLs[.Low]) ??
            AudioItemURL(quality: .Medium, URL: soundURLs[.Medium]) ??
            AudioItemURL(quality: .High, URL: soundURLs[.High])) ??
            AudioItemURL(quality: .Lossless, URL: soundURLs[.Lossless])!
    }
    
    // MARK: KVO
    
//    internal static var ap_KVOItems: [String] {
//        return ["artist", "title", "album", "artworkImage"]
//    }
//    
//    // MARK: Metadata
//    
//    public func parseMetadata(items: [AVMetadataItem]) {
//        items.forEach {
//            if let commonKey = $0.commonKey {
//                switch commonKey {
//                case AVMetadataCommonKeyTitle where musicTitle == nil:
//                    musicTitle = $0.value as? String
//                case AVMetadataCommonKeyArtist where musicArtist == nil:
//                    musicArtist = $0.value as? String
//                case AVMetadataCommonKeyAlbumName where musicAlbum == nil:
//                    musicAlbum = $0.value as? String
//                default:
//                    #if os(iOS)
////                        if commonKey == AVMetadataCommonKeyArtwork && artworkImage == nil {
////                            artworkImage = ($0.value as? NSData).map { UIImage(data: $0) } ?? nil
////                        }
//                    #endif
//                }
//            }
//        }
//    }
}

// MARK: - AudioItemURL

/**
`AudioItemURL` contains information about an Item URL such as its
quality.
*/
public struct AudioItemURL {
    public let quality: AudioQuality
    public let URL: NSURL
    
    public init?(quality: AudioQuality, URL: NSURL?) {
        if let URL = URL {
            self.quality = quality
            self.URL = URL
        }
        else {
            return nil
        }
    }
}


// MARK: - AudioQuality

/**
`AudioQuality` differentiates qualities for audio.

- `High`:   The highest quality.
- `Medium`: The quality between highest and lowest.
- `Low`:    The lowest quality.
*/
public enum AudioQuality {
    case Lossless
    case High
    case Medium
    case Low
}