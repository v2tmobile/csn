//
//	Album.swift
//
//	Create by Thành Vũ Trung on 4/12/2015
//	Copyright © 2015 V2T Multimedia. All rights reserved.


import RealmSwift

public class Album : Object{

	dynamic var catId : String!
	dynamic var catLevel : String!
	dynamic var catSublevel : String!
	dynamic var coverImg : String!
	dynamic var musicAlbum : String!
	dynamic var musicAlbumId : String!
	dynamic var musicArtist : String!
	dynamic var musicBitrate : String!
	dynamic var musicId : String!
	dynamic var musicLength : String!
	dynamic var musicTitleUrl : String!
	dynamic var musicYear : String!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	class func fromDictionary(dictionary: NSDictionary) -> Album	{
		let this = Album()
		if let catIdValue = dictionary["cat_id"] as? String{
			this.catId = catIdValue
		}
		if let catLevelValue = dictionary["cat_level"] as? String{
			this.catLevel = catLevelValue
		}
		if let catSublevelValue = dictionary["cat_sublevel"] as? String{
			this.catSublevel = catSublevelValue
		}
		if let coverImgValue = dictionary["cover_img"] as? String{
			this.coverImg = coverImgValue
		}
		if let musicAlbumValue = dictionary["music_album"] as? String{
			this.musicAlbum = musicAlbumValue
		}
		if let musicAlbumIdValue = dictionary["music_album_id"] as? String{
			this.musicAlbumId = musicAlbumIdValue
		}
		if let musicArtistValue = dictionary["music_artist"] as? String{
			this.musicArtist = musicArtistValue
		}
		if let musicBitrateValue = dictionary["music_bitrate"] as? String{
			this.musicBitrate = musicBitrateValue
		}
		if let musicIdValue = dictionary["music_id"] as? String{
			this.musicId = musicIdValue
		}
		if let musicLengthValue = dictionary["music_length"] as? String{
			this.musicLength = musicLengthValue
		}
		if let musicTitleUrlValue = dictionary["music_title_url"] as? String{
			this.musicTitleUrl = musicTitleUrlValue
		}
		if let musicYearValue = dictionary["music_year"] as? String{
			this.musicYear = musicYearValue
		}
		return this
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if catId != nil{
			dictionary["cat_id"] = catId
		}
		if catLevel != nil{
			dictionary["cat_level"] = catLevel
		}
		if catSublevel != nil{
			dictionary["cat_sublevel"] = catSublevel
		}
		if coverImg != nil{
			dictionary["cover_img"] = coverImg
		}
		if musicAlbum != nil{
			dictionary["music_album"] = musicAlbum
		}
		if musicAlbumId != nil{
			dictionary["music_album_id"] = musicAlbumId
		}
		if musicArtist != nil{
			dictionary["music_artist"] = musicArtist
		}
		if musicBitrate != nil{
			dictionary["music_bitrate"] = musicBitrate
		}
		if musicId != nil{
			dictionary["music_id"] = musicId
		}
		if musicLength != nil{
			dictionary["music_length"] = musicLength
		}
		if musicTitleUrl != nil{
			dictionary["music_title_url"] = musicTitleUrl
		}
		if musicYear != nil{
			dictionary["music_year"] = musicYear
		}
		return dictionary
	}

}