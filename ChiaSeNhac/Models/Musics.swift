//
//  DownloadList.swift
//  ChiaSeNhac
//
//  Created by Vũ Trung Thành on 1/10/16.
//  Copyright © 2016 V2T Multimedia. All rights reserved.
//

import Foundation
import RealmSwift

class Musics: Object {
    
    dynamic var name = ""
    dynamic var createAt = NSDate()
    let musics = List<Music>()
    
// Specify properties to ignore (Realm won't persist these)
    
//  override static func ignoredProperties() -> [String] {
//    return []
//  }
}
