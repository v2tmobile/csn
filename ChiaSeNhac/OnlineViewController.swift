//
//  ViewController.swift
//  ChiaSeNhac
//
//  Created by Vũ Trung Thành on 12/3/15.
//  Copyright © 2015 V2T Multimedia. All rights reserved.
//

import UIKit
import SwiftyJSON
import UIColor_Hex_Swift
import FontAwesome_swift
import CarbonKit
import MBProgressHUD

class OnlineViewController: V2TWithSearchBarViewController, CarbonTabSwipeNavigationDelegate {
    
    @IBOutlet weak var contentView: UIView!
    
    var currentTabIndex = 0
    var key: String!
    var topMusicIndex: Int!
    
    private enum MusicCategory: String {
        
        case ThuyNga = "thuy-nga"
        case ThuyNgaNhacTre = "tn-nhac-tre"
        case ThuyNgaQueHuong = "tn-que-huong"
        case ThuyNgaTruTinh = "tn-tru-tinh"
        
        case Instrumental = "beat-playback"
        case VietNamInstrumental = "v-instrumental"
        case USUKInstrumental = "u-instrumental"
        case ChineseInstrumental = "c-instrumental"
        case KoreaInstrumental = "k-instrumental"
        case OtherInstrumental = "o-instrumental"

        case Video = "video"
        case VietNamVideo = "v-video"
        case USUKVideo = "u-video"
        case ChineseVideo = "c-video"
        case KoreaVideo = "k-video"
        case OtherVideo = "o-video"

        case VietNam = "vietnam"
        case VietNamPopRock = "v-pop"
        case VietnamRapHiphop = "v-rap-hiphop"
        case VietNamDanceRemix = "v-dance-remix"
        
        case USUK = "us-uk"
        case USUKPopRock = "u-pop"
        case USUKRapHiphop = "u-rap-hiphop"
        case USUKDanceRemix = "u-dance-remix"

        case Chinese = "chinese"
        case ChinesePopRock = "c-pop"
        case ChineseRapHiphop = "c-rap-hiphop"
        case ChineseDanceRemix = "c-dance-remix"
        
        case Korea = "korea"
        case KoreaPopRock = "k-pop"
        case KoreaRapHiphop = "k-rap-hiphop"
        case KoreaDanceRemix = "k-dance-remix"
        
        case Other = "other"
        case OtherPopRock = "o-pop"
        case OtherRapHiphop = "o-rap-hiphop"
        case OtherDanceRemix = "o-dance-remix"
    }

    var itemsView = [
        NSLocalizedString("BXH Album", comment: ""),
        NSLocalizedString("BXH Bài Hát", comment: ""),
        NSLocalizedString("BXH Video", comment: ""),
        NSLocalizedString("Thuý Nga", comment: ""),
    ]
    
    let thirdParty = [
        NSLocalizedString("Nhạc không lời", comment: ""),
        NSLocalizedString("Video Clip", comment: ""),
        NSLocalizedString("Việt Nam", comment: ""),
        NSLocalizedString("Âu Mỹ", comment: ""),
        NSLocalizedString("Nhạc Hoa", comment: ""),
        NSLocalizedString("Nhạc Hàn", comment: ""),
        NSLocalizedString("Nước khác", comment: ""),
    ]
    
//    @IBOutlet weak var containerView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = NSLocalizedString("Nhạc online", comment: "")
        
        if CSN.Setting.isShowThirdPartyContent {
            itemsView.appendContentsOf(thirdParty)
        }
        
        let tabSwipeNavigation = CarbonTabSwipeNavigation(items: itemsView, delegate: self)
        tabSwipeNavigation.setIndicatorColor(UIColor(rgba: "#d32f2f"))
        tabSwipeNavigation.setSelectedColor(UIColor(rgba: "#d32f2f"))
        tabSwipeNavigation.setNormalColor(UIColor(rgba: "#424242"))
        switch UIDevice.currentDevice().userInterfaceIdiom {
        case .Phone:
            // It's an iPhone
            tabSwipeNavigation.setTabBarHeight(32)
        case .Pad:
            // It's an iPad
            tabSwipeNavigation.setTabBarHeight(44)
        default:
            // Uh, oh! What could it be?
            tabSwipeNavigation.setTabBarHeight(32)
        }
//        tabSwipeNavigation.setTabExtraWidth(20)
        tabSwipeNavigation.setIndicatorHeight(2)
        tabSwipeNavigation.insertIntoRootViewController(self, andTargetView: contentView)
        
        MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        CSN.API.getAllMusic(success: { (result) -> Void in
            MBProgressHUD.hideHUDForView(self.view, animated: true)
            }) { (error) -> Void in
                MBProgressHUD.hideHUDForView(self.view, animated: true)
        }
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        let tracker = GAI.sharedInstance().defaultTracker
        tracker.set(kGAIScreenName, value: "Nhạc online")
        
        let builder = GAIDictionaryBuilder.createScreenView()
        tracker.send(builder.build() as [NSObject : AnyObject])
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - CarbonKitTapNavigation Delegate
    
    func carbonTabSwipeNavigation(carbonTabSwipeNavigation: CarbonTabSwipeNavigation, viewControllerAtIndex index: UInt) -> UIViewController {
        self.currentTabIndex = Int(index)
        switch index {
        case 0:
            return self.topAlbumViewController()
        case 1:
            return self.topMusicViewController()
        case 2:
            return self.topVideoHotViewController()
        case 3:
            return self.topThuyNgaViewController()
        case 4:
            return self.topInstrumentalMusicViewController()
        case 5:
            return self.topVideoViewController()
        case 6:
            return self.topVietNamViewController()
        case 7:
            return self.topUSUKViewController()
        case 8:
            return self.topChineseViewController()
        case 9:
            return self.topKoreaViewController()
        case 10:
            return self.topOtherViewController()
        default:
            break
        }
        
        // return viewController at index
        return self.topMusicViewController()
    }

    func topAlbumViewController() -> UIViewController {
        // GA
        let tracker = GAI.sharedInstance().defaultTracker
        tracker.set(kGAIScreenName, value: "Top Album")
        
        let builder = GAIDictionaryBuilder.createScreenView()
        tracker.send(builder.build() as [NSObject : AnyObject])
        
        //
        let storyboard = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle())
        let albumListVC = storyboard.instantiateViewControllerWithIdentifier("AlbumList") as! AlbumsCollectionViewController
        
        return albumListVC as UIViewController
    }
    
    func topMusicViewController() -> UIViewController {
        // GA
        let tracker = GAI.sharedInstance().defaultTracker
        tracker.set(kGAIScreenName, value: "Top bài hát")
        
        let builder = GAIDictionaryBuilder.createScreenView()
        tracker.send(builder.build() as [NSObject : AnyObject])
        
        //
        let storyboard = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle())
        let songListVC = storyboard.instantiateViewControllerWithIdentifier("SongList") as! SongsListTableViewController
        
        songListVC.viewDidLoadHandler = {
            self.topMusicIndex = 1
            songListVC.headerName = NSLocalizedString("Nhạc Việt Nam", comment: "")
            self.loadTopMusic(self.topMusicIndex, songListVC: songListVC)
            songListVC.key = MusicCategory.VietNam.rawValue
        }
        
        songListVC.didSelectCategoryHandler = {
            self.showTopMusicCategory(songListVC)
        }

        songListVC.showDetailCategoryHandler = {
            let categoryListVC = self.storyboard!.instantiateViewControllerWithIdentifier("categoryList") as! CategoryDetailsViewController
            categoryListVC.catUrlString = songListVC.key
            categoryListVC.tabBarTitle = NSLocalizedString("BXH-\(songListVC.headerName)", comment: "")
            self.navigationController!.pushViewController(categoryListVC, animated: true)
        }
        
        songListVC.reloadHandler = {
            self.loadTopMusic(self.topMusicIndex, songListVC: songListVC)
            songListVC.swipeRefreshControl.endRefreshing()
            sharedV2TAds.showInterstitialAdsFromRootViewController(songListVC)
        }
        
        return songListVC as UIViewController
    }
    
    func showTopMusicCategory(songListViewController: SongsListTableViewController) {
        var items = [
            BottomMenuViewItem(title: NSLocalizedString("Nhạc Việt Nam", comment: ""), selectedAction: { (Void) -> Void in
                songListViewController.headerName = NSLocalizedString("Nhạc Việt Nam", comment: "")
                songListViewController.key = MusicCategory.VietNam.rawValue
            }),
        ]
        
        let thirdPartyItems = [
            BottomMenuViewItem(title: NSLocalizedString("Nhạc US-UK", comment: ""), selectedAction: { (Void) -> Void in
                songListViewController.headerName = NSLocalizedString("Nhạc US-UK", comment: "")
                songListViewController.key = MusicCategory.USUK.rawValue
            }),
            BottomMenuViewItem(title: NSLocalizedString("Nhạc Hoa", comment: ""), selectedAction: { (Void) -> Void in
                songListViewController.headerName = NSLocalizedString("Nhạc Hoa", comment: "")
                songListViewController.key = MusicCategory.Chinese.rawValue
            }),
            BottomMenuViewItem(title: NSLocalizedString("Nhạc Hàn", comment: ""), selectedAction: { (Void) -> Void in
                songListViewController.headerName = NSLocalizedString("Nhạc Hàn", comment: "")
                songListViewController.key = MusicCategory.Korea.rawValue
            }),
            BottomMenuViewItem(title: NSLocalizedString("Nhạc nước khác", comment: ""), selectedAction: { (Void) -> Void in
                songListViewController.headerName =  NSLocalizedString("Nhạc nước khác", comment: "")
                songListViewController.key = MusicCategory.Other.rawValue
            }),
            BottomMenuViewItem(title: NSLocalizedString("Beat, Playback", comment: ""), selectedAction: { (Void) -> Void in
                songListViewController.headerName = NSLocalizedString("Beat, Playback", comment: "")
                songListViewController.key = MusicCategory.Instrumental.rawValue
            }),
        ]
        
        if CSN.Setting.isShowThirdPartyContent {
            items.appendContentsOf(thirdPartyItems)
        }
        
        let bottomMenu = BottomMenuView(items: items, didSelectedHandler: {
                index in
                self.loadTopMusic(index, songListVC: songListViewController)
        })
        bottomMenu.showInViewController(navigationController!)
        
    }
    
    func loadTopMusic(index: Int, songListVC: SongsListTableViewController) {
        songListVC.swipeRefreshControl.startRefreshing()
        CSN.API.getAllMusic(loadCache: false, success: { (result) ->  Void in
            if result["category"] != nil {
                songListVC.songsList = []
                if let musicArray = result["category"][index+1]["music"].array {
                    for item: JSON in musicArray {
                        songListVC.songsList.append(Music.fromDictionary(item.dictionaryObject!))
                    }
                }
            }
            songListVC.tableView.reloadData()
            self.topMusicIndex = index
            songListVC.swipeRefreshControl.endRefreshing()
            }) { (error) -> Void in
        }
    }
    
    func topVideoHotViewController() -> UIViewController {
        // GA
        let tracker = GAI.sharedInstance().defaultTracker
        tracker.set(kGAIScreenName, value: "Top video hot")
        
        let builder = GAIDictionaryBuilder.createScreenView()
        tracker.send(builder.build() as [NSObject : AnyObject])
        
        //
        let storyboard = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle())
        let videosListVC = storyboard.instantiateViewControllerWithIdentifier("VideoList") as! VideosListTableViewController
        
        videosListVC.showCategoryButton = false

        videosListVC.viewDidLoadHandler = {
            self.loadTopVideoHot(videosListVC)
        }
        
        videosListVC.reloadHandler = {
            self.loadTopVideoHot(videosListVC)
            videosListVC.swipeRefreshControl.endRefreshing()
            sharedV2TAds.showInterstitialAdsFromRootViewController(videosListVC)
        }

        videosListVC.showDetailCategoryHandler = {
            let categoryListVC = self.storyboard!.instantiateViewControllerWithIdentifier("categoryList") as! CategoryDetailsViewController
            categoryListVC.tabBarTitle = NSLocalizedString("BXH Video", comment: "")
            categoryListVC.catUrlString = videosListVC.key
            categoryListVC.categoryType = .Video
            self.navigationController!.pushViewController(categoryListVC, animated: true)
        }
        
        return videosListVC as UIViewController
    }
    
    func loadTopVideoHot(videosListVC: VideosListTableViewController) {
        videosListVC.swipeRefreshControl.startRefreshing()
        CSN.API.getAllMusic(loadCache: false, success: { (result) -> () in
            if result["category"] != nil {
                videosListVC.videosData = []
                if let musicArray = result["category"][0]["music"].array {
                    for item: JSON in musicArray {
                        videosListVC.videosList.append(Music.fromDictionary(item.dictionaryObject!))
                    }
                }
                videosListVC.tableView?.reloadData()
            }
            videosListVC.swipeRefreshControl.endRefreshing()
            videosListVC.key = MusicCategory.Video.rawValue
            }){ (error) -> Void in
        }

    }
    
    func topThuyNgaViewController() -> UIViewController {
        // GA
        let tracker = GAI.sharedInstance().defaultTracker
        tracker.set(kGAIScreenName, value: "Top nhạc Thuý Nga")
        
        let builder = GAIDictionaryBuilder.createScreenView()
        tracker.send(builder.build() as [NSObject : AnyObject])
        
        //
        let storyboard = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle())
        let songListVC = storyboard.instantiateViewControllerWithIdentifier("SongList") as! SongsListTableViewController
        
        songListVC.viewDidLoadHandler = {
            songListVC.key = MusicCategory.ThuyNga.rawValue
            self.loadTopThuyNga(songListVC)
            songListVC.headerName = NSLocalizedString("Tổng hợp", comment: "")
        }
        
        songListVC.reloadHandler = {
            self.loadTopThuyNga(songListVC)
            songListVC.swipeRefreshControl.endRefreshing()
            sharedV2TAds.showInterstitialAdsFromRootViewController(songListVC)
        }

        songListVC.didSelectCategoryHandler = {
            self.showTopThuyNgaCategory(songListVC)
        }

        songListVC.showDetailCategoryHandler = {
            let categoryListVC = self.storyboard!.instantiateViewControllerWithIdentifier("categoryList") as! CategoryDetailsViewController
            categoryListVC.tabBarTitle = NSLocalizedString("Thuý Nga-\(songListVC.headerName)", comment: "")
            categoryListVC.catUrlString = songListVC.key
            self.navigationController!.pushViewController(categoryListVC, animated: true)
        }
        
        return songListVC as UIViewController
    }

    func showTopThuyNgaCategory(songListViewController: SongsListTableViewController) {
        let bottomMenu = BottomMenuView(items: [
            BottomMenuViewItem(title: NSLocalizedString("Tổng hợp", comment: ""), selectedAction: { (Void) -> Void in
                songListViewController.key = MusicCategory.ThuyNga.rawValue
                songListViewController.headerName = NSLocalizedString("Tổng hợp", comment: "")
            }),
            BottomMenuViewItem(title: NSLocalizedString("Nhạc trẻ", comment: ""), selectedAction: { (Void) -> Void in
                songListViewController.key = MusicCategory.ThuyNgaNhacTre.rawValue
                songListViewController.headerName = NSLocalizedString("nhạc trẻ", comment: "")
            }),
            BottomMenuViewItem(title: NSLocalizedString("Nhạc trữ tình", comment: ""), selectedAction: { (Void) -> Void in
                songListViewController.key = MusicCategory.ThuyNgaTruTinh.rawValue
                songListViewController.headerName =  NSLocalizedString("nhạc trữ tình", comment: "")
            }),
            BottomMenuViewItem(title: NSLocalizedString("Nhạc quê hương", comment: ""), selectedAction: { (Void) -> Void in
                songListViewController.key = MusicCategory.ThuyNgaQueHuong.rawValue
                songListViewController.headerName = NSLocalizedString("nhạc quê hương", comment: "")
            })
            ], didSelectedHandler: { (Void) -> Void in
                self.loadTopThuyNga(songListViewController)
        })
        bottomMenu.showInViewController(navigationController!)
        
    }
    
    func loadTopThuyNga(songListVC: SongsListTableViewController) {
        songListVC.swipeRefreshControl.startRefreshing()
        CSN.API.getCategory(name: songListVC.key, page: 1, loadCache: false, success: { (result) -> Void in
            songListVC.songsList = []
            if let result: JSON = result["hot"] {
                if let musicArray = result["music"].array {
                    for item: JSON in musicArray {
                        songListVC.songsList.append(Music.fromDictionary(item.dictionaryObject!))
                    }
                }
                songListVC.tableView.reloadData()
            }
            songListVC.swipeRefreshControl.endRefreshing()
            }, failure: { (error) -> Void in
                
        })
    }
    
    func topInstrumentalMusicViewController() -> UIViewController {
        //GA
        let tracker = GAI.sharedInstance().defaultTracker
        tracker.set(kGAIScreenName, value: "Top nhạc không lời")
        
        let builder = GAIDictionaryBuilder.createScreenView()
        tracker.send(builder.build() as [NSObject : AnyObject])
        
        let storyboard = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle())
        let songListVC = storyboard.instantiateViewControllerWithIdentifier("SongList") as! SongsListTableViewController
        
        songListVC.viewDidLoadHandler = {
            songListVC.key = MusicCategory.Instrumental.rawValue
            self.loadTopInstrumentalCategory(songListVC)
            songListVC.headerName = NSLocalizedString("Tổng hợp", comment: "")
        }
        
        songListVC.reloadHandler = {
            self.loadTopInstrumentalCategory(songListVC)
            songListVC.swipeRefreshControl.endRefreshing()
            sharedV2TAds.showInterstitialAdsFromRootViewController(songListVC)
        }
        
        songListVC.didSelectCategoryHandler = {
            self.showTopInstrumentalCategory(songListVC)
        }

        songListVC.showDetailCategoryHandler = {
            let categoryListVC = self.storyboard!.instantiateViewControllerWithIdentifier("categoryList") as! CategoryDetailsViewController
            categoryListVC.tabBarTitle = NSLocalizedString("Nhạc không lời-\(songListVC.headerName)", comment: "")
            categoryListVC.catUrlString = songListVC.key
            self.navigationController!.pushViewController(categoryListVC, animated: true)
        }
        
        return songListVC as UIViewController
    }
    
    func showTopInstrumentalCategory(songListViewController: SongsListTableViewController) {
        let bottomMenu = BottomMenuView(items: [
            BottomMenuViewItem(title: NSLocalizedString("Tổng hợp", comment: ""), selectedAction: { (Void) -> Void in
                songListViewController.key = MusicCategory.Instrumental.rawValue
                songListViewController.headerName = NSLocalizedString("Tổng hợp", comment: "")
            }),
            BottomMenuViewItem(title: NSLocalizedString("Việt Nam", comment: ""), selectedAction: { (Void) -> Void in
                songListViewController.key = MusicCategory.VietNamInstrumental.rawValue
                songListViewController.headerName = NSLocalizedString("Việt Nam", comment: "")
            }),
            BottomMenuViewItem(title: NSLocalizedString("Âu Mỹ", comment: ""), selectedAction: { (Void) -> Void in
                songListViewController.key = MusicCategory.USUKInstrumental.rawValue
                songListViewController.headerName = NSLocalizedString("Âu Mỹ", comment: "")
            }),
            BottomMenuViewItem(title: NSLocalizedString("Hoa", comment: ""), selectedAction: { (Void) -> Void in
                songListViewController.key = MusicCategory.ChineseInstrumental.rawValue
                songListViewController.headerName = NSLocalizedString("Hoa", comment: "")
            }),
            BottomMenuViewItem(title: NSLocalizedString("Hàn", comment: ""), selectedAction: { (Void) -> Void in
                songListViewController.key = MusicCategory.KoreaInstrumental.rawValue
                songListViewController.headerName = NSLocalizedString("Hàn", comment: "")
            }),
            BottomMenuViewItem(title: NSLocalizedString("Nước khác", comment: ""), selectedAction: { (Void) -> Void in
                songListViewController.key = MusicCategory.OtherInstrumental.rawValue
                songListViewController.headerName = NSLocalizedString("nước khác", comment: "")
            })
            ], didSelectedHandler: { (Void) -> Void in
                self.loadTopInstrumentalCategory(songListViewController)
        })
        bottomMenu.showInViewController(navigationController!)
        
    }
    
    func loadTopInstrumentalCategory(songListVC: SongsListTableViewController) {
        songListVC.swipeRefreshControl.startRefreshing()
        CSN.API.getCategory(name: songListVC.key, page: 1, loadCache: false, success: { (result) -> Void in
            songListVC.songsList = []
            if let result: JSON = result["hot"] {
                if let musicArray = result["music"].array {
                    for item: JSON in musicArray {
                        songListVC.songsList.append(Music.fromDictionary(item.dictionaryObject!))
                    }
                }
                songListVC.tableView.reloadData()
            }
            songListVC.swipeRefreshControl.endRefreshing()
            }, failure: { (error) -> Void in
        })
    }
    
    func topVideoViewController() -> UIViewController {
        // GA
        let tracker = GAI.sharedInstance().defaultTracker
        tracker.set(kGAIScreenName, value: "Top video")
        
        let builder = GAIDictionaryBuilder.createScreenView()
        tracker.send(builder.build() as [NSObject : AnyObject])
        
        //
        let storyboard = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle())
        let videosListVC = storyboard.instantiateViewControllerWithIdentifier("VideoList") as! VideosListTableViewController
        
        videosListVC.viewDidLoadHandler = {
            videosListVC.key = MusicCategory.Video.rawValue
            videosListVC.headerName = NSLocalizedString("Tổng hợp", comment: "")
            self.loadTopVideo(videosListVC)
        }
        
        videosListVC.reloadHandler = {
            self.loadTopVideo(videosListVC)
            videosListVC.swipeRefreshControl.endRefreshing()
            sharedV2TAds.showInterstitialAdsFromRootViewController(videosListVC)
        }
        
        videosListVC.didSelectCategoryHandler = {
            self.showTopVideoCategory(videosListVC)
        }

        videosListVC.showDetailCategoryHandler = {
            let categoryListVC = self.storyboard!.instantiateViewControllerWithIdentifier("categoryList") as! CategoryDetailsViewController
            categoryListVC.tabBarTitle = NSLocalizedString("Video clip-\(videosListVC.headerName)", comment: "")
            categoryListVC.categoryType = .Video
            categoryListVC.catUrlString = videosListVC.key
            
            self.navigationController!.pushViewController(categoryListVC, animated: true)
        }
        
        return videosListVC as UIViewController

    }
    
    func showTopVideoCategory(videoListViewController: VideosListTableViewController) {
        let bottomMenu = BottomMenuView(items: [
            BottomMenuViewItem(title: NSLocalizedString("Tổng hợp", comment: ""), selectedAction: { (Void) -> Void in
                videoListViewController.key = MusicCategory.Video.rawValue
                videoListViewController.headerName = NSLocalizedString("Tổng hợp", comment: "")
            }),
            BottomMenuViewItem(title: NSLocalizedString("Việt Nam", comment: ""), selectedAction: { (Void) -> Void in
                videoListViewController.key = MusicCategory.VietNamVideo.rawValue
                videoListViewController.headerName = NSLocalizedString("Việt Nam", comment: "")
            }),
            BottomMenuViewItem(title: NSLocalizedString("Âu Mỹ", comment: ""), selectedAction: { (Void) -> Void in
                videoListViewController.key = MusicCategory.USUKVideo.rawValue
                videoListViewController.headerName = NSLocalizedString("Âu Mỹ", comment: "")
            }),
            BottomMenuViewItem(title: NSLocalizedString("Hoa", comment: ""), selectedAction: { (Void) -> Void in
                videoListViewController.key = MusicCategory.ChineseVideo.rawValue
                videoListViewController.headerName = NSLocalizedString("Hoa", comment: "")
            }),
            BottomMenuViewItem(title: NSLocalizedString("Hàn", comment: ""), selectedAction: { (Void) -> Void in
                videoListViewController.key = MusicCategory.KoreaVideo.rawValue
                videoListViewController.headerName = NSLocalizedString("Hàn", comment: "")
            }),
            BottomMenuViewItem(title: NSLocalizedString("Nước khác", comment: ""), selectedAction: { (Void) -> Void in
                videoListViewController.key = MusicCategory.OtherVideo.rawValue
                videoListViewController.headerName = NSLocalizedString("Nước khác", comment: "")
            })
            ], didSelectedHandler: { (Void) -> Void in
                self.loadTopVideo(videoListViewController)
        })
        bottomMenu.showInViewController(navigationController!)
        
    }

    func loadTopVideo(videosListVC: VideosListTableViewController) {
        videosListVC.swipeRefreshControl.startRefreshing()
        CSN.API.getCategory(name: videosListVC.key, page: 1, loadCache: false, success: { (result) -> Void in
            if let result: JSON = result["hot"] {
                videosListVC.videosList = []
                if let musicArray = result["music"].array {
                    for item: JSON in musicArray {
                        videosListVC.videosList.append(Music.fromDictionary(item.dictionaryObject!))
                    }
                }
                videosListVC.tableView.reloadData()
            }
            videosListVC.swipeRefreshControl.endRefreshing()
            }, failure: { (error) -> Void in
        })
    }

    func topVietNamViewController() -> UIViewController {
        // GA
        let tracker = GAI.sharedInstance().defaultTracker
        tracker.set(kGAIScreenName, value: "Top nhạc Việt Nam")
        
        let builder = GAIDictionaryBuilder.createScreenView()
        tracker.send(builder.build() as [NSObject : AnyObject])
        
        //
        let storyboard = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle())
        let songListVC = storyboard.instantiateViewControllerWithIdentifier("SongList") as! SongsListTableViewController
        
        songListVC.viewDidLoadHandler = {
            songListVC.key = MusicCategory.VietNam.rawValue
            self.loadTopVietNam(songListVC)
            songListVC.headerName = NSLocalizedString("Tổng hợp", comment: "")
        }
        
        songListVC.reloadHandler = {
            self.loadTopVietNam(songListVC)
            songListVC.swipeRefreshControl.endRefreshing()
            sharedV2TAds.showInterstitialAdsFromRootViewController(songListVC)
        }
        
        songListVC.didSelectCategoryHandler = {
            self.showVietNamMusicCategory(songListVC)
        }
        
        songListVC.showDetailCategoryHandler = {
            let categoryListVC = self.storyboard!.instantiateViewControllerWithIdentifier("categoryList") as! CategoryDetailsViewController
            categoryListVC.tabBarTitle = NSLocalizedString("Việt Nam-\(songListVC.headerName)", comment: "")
            categoryListVC.catUrlString = songListVC.key
            self.navigationController!.pushViewController(categoryListVC, animated: true)
        }
        
        return songListVC as UIViewController
    }
    
    func showVietNamMusicCategory(songListViewController: SongsListTableViewController) {
        let bottomMenu = BottomMenuView(items: [
            BottomMenuViewItem(title: NSLocalizedString("Tổng hợp", comment: ""), selectedAction: { (Void) -> Void in
                songListViewController.key = MusicCategory.VietNam.rawValue
                songListViewController.headerName = NSLocalizedString("Tổng hợp", comment: "")
            }),
            BottomMenuViewItem(title: NSLocalizedString("Pop, Rock", comment: ""), selectedAction: { (Void) -> Void in
                songListViewController.key = MusicCategory.VietNamPopRock.rawValue
                songListViewController.headerName = NSLocalizedString("Pop, Rock", comment: "")
            }),
            BottomMenuViewItem(title: NSLocalizedString("Rap, Hiphop", comment: ""), selectedAction: { (Void) -> Void in
                songListViewController.key = MusicCategory.VietnamRapHiphop.rawValue
                songListViewController.headerName = NSLocalizedString("Rap, Hiphop", comment: "")
            }),
            BottomMenuViewItem(title: NSLocalizedString("Dance, Remix", comment: ""), selectedAction: { (Void) -> Void in
                songListViewController.key = MusicCategory.VietNamDanceRemix.rawValue
                songListViewController.headerName = NSLocalizedString("Dance, Remix", comment: "")
            })
            ], didSelectedHandler: { (Void) -> Void in
                self.loadTopVietNam(songListViewController)
        })
        bottomMenu.showInViewController(navigationController!)
        
    }
    
    func loadTopVietNam(songListVC: SongsListTableViewController) {
        songListVC.swipeRefreshControl.startRefreshing()
        CSN.API.getCategory(name: songListVC.key, page: 1, loadCache: false, success: { (result) -> Void in
            songListVC.songsList = []
            if let result: JSON = result["hot"] {
                if let musicArray = result["music"].array {
                    for item: JSON in musicArray {
                        songListVC.songsList.append(Music.fromDictionary(item.dictionaryObject!))
                    }
                }
                songListVC.tableView.reloadData()
            }
            songListVC.swipeRefreshControl.endRefreshing()
            }, failure: { (error) -> Void in
        })

    }

    func topUSUKViewController() -> UIViewController {
        // GA
        let tracker = GAI.sharedInstance().defaultTracker
        tracker.set(kGAIScreenName, value: "Top nhạc Âu Mĩ")
        
        let builder = GAIDictionaryBuilder.createScreenView()
        tracker.send(builder.build() as [NSObject : AnyObject])
        
        //
        let storyboard = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle())
        let songListVC = storyboard.instantiateViewControllerWithIdentifier("SongList") as! SongsListTableViewController
        
        songListVC.viewDidLoadHandler = {
            songListVC.key = MusicCategory.USUK.rawValue
            self.loadTopUSUK(songListVC)
            songListVC.headerName = NSLocalizedString("Tổng hợp", comment: "")
        }
        
        songListVC.reloadHandler = {
            self.loadTopUSUK(songListVC)
            songListVC.swipeRefreshControl.endRefreshing()
            sharedV2TAds.showInterstitialAdsFromRootViewController(songListVC)
        }
        
        songListVC.didSelectCategoryHandler = {
            self.showUSUKMusicCategory(songListVC)
        }
        
        songListVC.showDetailCategoryHandler = {
            let categoryListVC = self.storyboard!.instantiateViewControllerWithIdentifier("categoryList") as! CategoryDetailsViewController
            categoryListVC.tabBarTitle = NSLocalizedString("Âu Mỹ-\(songListVC.headerName)", comment: "")
            categoryListVC.catUrlString = songListVC.key
            self.navigationController!.pushViewController(categoryListVC, animated: true)
        }
        
        return songListVC as UIViewController
    }
    
    func showUSUKMusicCategory(songListViewController: SongsListTableViewController) {
        let bottomMenu = BottomMenuView(items: [
            BottomMenuViewItem(title: NSLocalizedString("Tổng hợp", comment: ""), selectedAction: { (Void) -> Void in
                songListViewController.key = MusicCategory.USUK.rawValue
                songListViewController.headerName = NSLocalizedString("Tổng hợp", comment: "")
            }),
            BottomMenuViewItem(title: NSLocalizedString("Pop, Rock", comment: ""), selectedAction: { (Void) -> Void in
                songListViewController.key = MusicCategory.USUKPopRock.rawValue
                songListViewController.headerName = NSLocalizedString("Pop, Rock", comment: "")
            }),
            BottomMenuViewItem(title: NSLocalizedString("Rap, Hiphop", comment: ""), selectedAction: { (Void) -> Void in
                songListViewController.key = MusicCategory.USUKRapHiphop.rawValue
                songListViewController.headerName = NSLocalizedString("Rap, Hiphop", comment: "")
            }),
            BottomMenuViewItem(title: NSLocalizedString("Dance, Remix", comment: ""), selectedAction: { (Void) -> Void in
                songListViewController.key = MusicCategory.USUKDanceRemix.rawValue
                songListViewController.headerName = NSLocalizedString("Dance, Remix", comment: "")
            })
            ], didSelectedHandler: { (Void) -> Void in
                self.loadTopUSUK(songListViewController)
        })
        bottomMenu.showInViewController(navigationController!)
        
    }
    
    func loadTopUSUK(songListVC: SongsListTableViewController) {
        songListVC.swipeRefreshControl.startRefreshing()
        CSN.API.getCategory(name: songListVC.key, page: 1, loadCache: false, success: { (result) -> Void in
            songListVC.songsList = []
            if let result: JSON = result["hot"] {
                if let musicArray = result["music"].array {
                    for item: JSON in musicArray {
                        songListVC.songsList.append(Music.fromDictionary(item.dictionaryObject!))
                    }
                }
                songListVC.tableView.reloadData()
            }
            songListVC.swipeRefreshControl.endRefreshing()
            }, failure: { (error) -> Void in
        })
        
    }

    func topChineseViewController() -> UIViewController {
        // GA
        let tracker = GAI.sharedInstance().defaultTracker
        tracker.set(kGAIScreenName, value: "Top nhạc Hoa")
        
        let builder = GAIDictionaryBuilder.createScreenView()
        tracker.send(builder.build() as [NSObject : AnyObject])
        
        //
        let storyboard = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle())
        let songListVC = storyboard.instantiateViewControllerWithIdentifier("SongList") as! SongsListTableViewController
        
        songListVC.viewDidLoadHandler = {
            songListVC.key = MusicCategory.Chinese.rawValue
            self.loadTopChinese(songListVC)
            songListVC.headerName = NSLocalizedString("Tổng hợp", comment: "")
        }
        
        songListVC.reloadHandler = {
            self.loadTopChinese(songListVC)
            songListVC.swipeRefreshControl.endRefreshing()
            sharedV2TAds.showInterstitialAdsFromRootViewController(songListVC)
        }
        
        songListVC.didSelectCategoryHandler = {
            self.showChineseMusicCategory(songListVC)
        }
        
        songListVC.showDetailCategoryHandler = {
            let categoryListVC = self.storyboard!.instantiateViewControllerWithIdentifier("categoryList") as! CategoryDetailsViewController
            categoryListVC.tabBarTitle = NSLocalizedString("Nhạc Hoa-\(songListVC.headerName)", comment: "")
            categoryListVC.catUrlString = songListVC.key
            self.navigationController!.pushViewController(categoryListVC, animated: true)
        }
        
        return songListVC as UIViewController
    }
    
    func showChineseMusicCategory(songListViewController: SongsListTableViewController) {
        let bottomMenu = BottomMenuView(items: [
            BottomMenuViewItem(title: NSLocalizedString("Tổng hợp", comment: ""), selectedAction: { (Void) -> Void in
                songListViewController.key = MusicCategory.Chinese.rawValue
                songListViewController.headerName = NSLocalizedString("Tổng hợp", comment: "")
            }),
            BottomMenuViewItem(title: NSLocalizedString("Pop, Rock", comment: ""), selectedAction: { (Void) -> Void in
                songListViewController.key = MusicCategory.ChinesePopRock.rawValue
                songListViewController.headerName = NSLocalizedString("Pop, Rock", comment: "")
            }),
            BottomMenuViewItem(title: NSLocalizedString("Rap, Hiphop", comment: ""), selectedAction: { (Void) -> Void in
                songListViewController.key = MusicCategory.ChineseRapHiphop.rawValue
                songListViewController.headerName = NSLocalizedString("Rap, Hiphop", comment: "")
            }),
            BottomMenuViewItem(title: NSLocalizedString("Dance, Remix", comment: ""), selectedAction: { (Void) -> Void in
                songListViewController.key = MusicCategory.ChineseDanceRemix.rawValue
                songListViewController.headerName = NSLocalizedString("Dance, Remix", comment: "")
            })
            ], didSelectedHandler: { (Void) -> Void in
                self.loadTopChinese(songListViewController)
        })
        bottomMenu.showInViewController(navigationController!)
        
    }

    func loadTopChinese(songListVC: SongsListTableViewController) {
        songListVC.swipeRefreshControl.startRefreshing()
        CSN.API.getCategory(name: songListVC.key, page: 1, loadCache: false, success: { (result) -> Void in
            songListVC.songsList = []
            if let result: JSON = result["hot"] {
                if let musicArray = result["music"].array {
                    for item: JSON in musicArray {
                        songListVC.songsList.append(Music.fromDictionary(item.dictionaryObject!))
                    }
                }
                songListVC.tableView.reloadData()
            }
            songListVC.swipeRefreshControl.endRefreshing()
            }, failure: { (error) -> Void in
        })
    }
    
    func topKoreaViewController() -> UIViewController {
        // GA
        let tracker = GAI.sharedInstance().defaultTracker
        tracker.set(kGAIScreenName, value: "Top nhạc Hàn")
        
        let builder = GAIDictionaryBuilder.createScreenView()
        tracker.send(builder.build() as [NSObject : AnyObject])
        
        //
        let storyboard = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle())
        let songListVC = storyboard.instantiateViewControllerWithIdentifier("SongList") as! SongsListTableViewController
        
        songListVC.viewDidLoadHandler = {
            songListVC.key = MusicCategory.Korea.rawValue
            self.loadTopKorea(songListVC)
            songListVC.headerName = NSLocalizedString("Tổng hợp", comment: "")
        }
        
        songListVC.reloadHandler = {
            self.loadTopKorea(songListVC)
            songListVC.swipeRefreshControl.endRefreshing()
            sharedV2TAds.showInterstitialAdsFromRootViewController(songListVC)
        }
        
        songListVC.didSelectCategoryHandler = {
            self.showKoreaMusicCategory(songListVC)
        }
        
        songListVC.showDetailCategoryHandler = {
            let categoryListVC = self.storyboard!.instantiateViewControllerWithIdentifier("categoryList") as! CategoryDetailsViewController
            categoryListVC.tabBarTitle = NSLocalizedString("Nhạc Hàn-\(songListVC.headerName)", comment: "")
            categoryListVC.catUrlString = songListVC.key
            self.navigationController!.pushViewController(categoryListVC, animated: true)
        }
        
        return songListVC as UIViewController
    }
    
    func showKoreaMusicCategory(songListViewController: SongsListTableViewController) {
        let bottomMenu = BottomMenuView(items: [
            BottomMenuViewItem(title: NSLocalizedString("Tổng hợp", comment: ""), selectedAction: { (Void) -> Void in
                songListViewController.key = MusicCategory.Korea.rawValue
                songListViewController.headerName = NSLocalizedString("Tổng hợp", comment: "")
            }),
            BottomMenuViewItem(title: NSLocalizedString("Pop, Rock", comment: ""), selectedAction: { (Void) -> Void in
                songListViewController.key = MusicCategory.KoreaPopRock.rawValue
                songListViewController.headerName = NSLocalizedString("Pop, Rock", comment: "")
            }),
            BottomMenuViewItem(title: NSLocalizedString("Rap, Hiphop", comment: ""), selectedAction: { (Void) -> Void in
                songListViewController.key = MusicCategory.KoreaRapHiphop.rawValue
                songListViewController.headerName = NSLocalizedString("Rap, Hiphop", comment: "")
            }),
            BottomMenuViewItem(title: NSLocalizedString("Dance, Remix", comment: ""), selectedAction: { (Void) -> Void in
                songListViewController.key = MusicCategory.KoreaDanceRemix.rawValue
                songListViewController.headerName = NSLocalizedString("Dance, Remix", comment: "")
            })
            ], didSelectedHandler: { (Void) -> Void in
                self.loadTopKorea(songListViewController)
        })
        bottomMenu.showInViewController(navigationController!)
        
    }
    
    func loadTopKorea(songListVC: SongsListTableViewController) {
        songListVC.swipeRefreshControl.startRefreshing()
        CSN.API.getCategory(name: songListVC.key, page: 1, loadCache: false, success: { (result) -> Void in
            songListVC.songsList = []
            if let result: JSON = result["hot"] {
                if let musicArray = result["music"].array {
                    for item: JSON in musicArray {
                        songListVC.songsList.append(Music.fromDictionary(item.dictionaryObject!))
                    }
                }
                songListVC.tableView.reloadData()
            }
            songListVC.swipeRefreshControl.endRefreshing()
            }, failure: { (error) -> Void in
        })
    }

    func topOtherViewController() -> UIViewController {
        // GA
        let tracker = GAI.sharedInstance().defaultTracker
        tracker.set(kGAIScreenName, value: "Top nhạc khác")
        
        let builder = GAIDictionaryBuilder.createScreenView()
        tracker.send(builder.build() as [NSObject : AnyObject])
        
        //
        let storyboard = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle())
        let songListVC = storyboard.instantiateViewControllerWithIdentifier("SongList") as! SongsListTableViewController
        
        songListVC.viewDidLoadHandler = {
            songListVC.key = MusicCategory.Other.rawValue
            self.loadTopOther(songListVC)
            songListVC.headerName = NSLocalizedString("Tổng hợp", comment: "")
        }
        
        songListVC.reloadHandler = {
            self.loadTopOther(songListVC)
            songListVC.swipeRefreshControl.endRefreshing()
            sharedV2TAds.showInterstitialAdsFromRootViewController(songListVC)
        }
        
        songListVC.didSelectCategoryHandler = {
            self.showOtherMusicCategory(songListVC)
        }
        
        songListVC.showDetailCategoryHandler = {
            let categoryListVC = self.storyboard!.instantiateViewControllerWithIdentifier("categoryList") as! CategoryDetailsViewController
            categoryListVC.tabBarTitle = NSLocalizedString("Nhạc nước khác-\(songListVC.headerName)", comment: "")
            categoryListVC.catUrlString = songListVC.key
            self.navigationController!.pushViewController(categoryListVC, animated: true)
        }
        
        return songListVC as UIViewController
    }
    
    func showOtherMusicCategory(songListViewController: SongsListTableViewController) {
        let bottomMenu = BottomMenuView(items: [BottomMenuViewItem(title: NSLocalizedString("Tổng hợp", comment: ""), selectedAction: { (Void) -> Void in
            songListViewController.key = MusicCategory.Other.rawValue
            songListViewController.headerName = NSLocalizedString("Tổng hợp", comment: "")
            }),
            BottomMenuViewItem(title: NSLocalizedString("Tổng hợp", comment: ""), selectedAction: { (Void) -> Void in
                songListViewController.key = MusicCategory.Other.rawValue
                songListViewController.headerName = NSLocalizedString("Tổng hợp", comment: "")
            }),
            BottomMenuViewItem(title: NSLocalizedString("Pop, Rock", comment: ""), selectedAction: { (Void) -> Void in
                songListViewController.key = MusicCategory.OtherPopRock.rawValue
                songListViewController.headerName = NSLocalizedString("Pop, Rock", comment: "")
            }),
            BottomMenuViewItem(title: NSLocalizedString("Rap, Hiphop", comment: ""), selectedAction: { (Void) -> Void in
                songListViewController.key = MusicCategory.OtherRapHiphop.rawValue
                songListViewController.headerName =  NSLocalizedString("Rap, Hiphop", comment: "")
            }),
            BottomMenuViewItem(title: NSLocalizedString("Dance, Remix", comment: ""), selectedAction: { (Void) -> Void in
                songListViewController.key = MusicCategory.OtherDanceRemix.rawValue
                songListViewController.headerName = NSLocalizedString("Dance, Remix", comment: "")
            })
            ], didSelectedHandler: { (Void) -> Void in
                self.loadTopOther(songListViewController)
        })
        bottomMenu.showInViewController(navigationController!)
        
    }
    
    func loadTopOther(songListVC: SongsListTableViewController) {
        songListVC.swipeRefreshControl.startRefreshing()
        CSN.API.getCategory(name: songListVC.key, page: 1, loadCache: false, success: { (result) -> Void in
            songListVC.songsList = []
            if let result: JSON = result["hot"] {
                if let musicArray = result["music"].array {
                    for item: JSON in musicArray {
                        songListVC.songsList.append(Music.fromDictionary(item.dictionaryObject!))
                    }
                }
                songListVC.tableView.reloadData()
            }
            songListVC.swipeRefreshControl.endRefreshing()
            }, failure: { (error) -> Void in
        })
    }

}

