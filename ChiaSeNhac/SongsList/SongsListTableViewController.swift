//
//  TopSongsListTableViewController.swift
//  ChiaSeNhac
//
//  Created by Vũ Trung Thành on 12/4/15.
//  Copyright © 2015 V2T Multimedia. All rights reserved.
//

import UIKit
import SwiftyJSON
import CarbonKit
import FontAwesome_swift
import DZNEmptyDataSet

class SongsListTableViewController: V2TTableViewController, DZNEmptyDataSetSource, DZNEmptyDataSetDelegate {
    
    private let reuseCellIdentifier: String = "SongCell"
    @IBOutlet weak var adsView: UIView!
    
    var viewDidLoadHandler: (Void -> Void)?
    var didSelectCategoryHandler: (Void -> Void)?
    var reloadHandler: (Void -> Void)?
    var loadMoreHandler: (Void -> Void)?
    var showDetailCategoryHandler: (Void -> Void)?
    var extraButtonAtCellTappedHandler: ((SongItemTableViewCell -> Void))?
    
    var currentPage = 1
    var headerName: String!
    var songsList: [Music] = []
    var key: String!
    
    var showCategoryButton = true
    var showDetailButton = true
    var isOnline = true
    
    var swipeRefreshControl: CarbonSwipeRefresh!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.emptyDataSetSource = self
        self.tableView.emptyDataSetDelegate = self
        self.tableView.tableFooterView = UIView()
        
        // reload:
        
        swipeRefreshControl = CarbonSwipeRefresh.init(scrollView: self.tableView)
        swipeRefreshControl.colors = [UIColor(rgba: "#d32f2f")]
        swipeRefreshControl.addTarget(self, action: "refreshList:", forControlEvents: UIControlEvents.ValueChanged)
        self.view.addSubview(swipeRefreshControl)

        self.viewDidLoadHandler?()
        
    }
    
    func backButtonTapped(button: UIButton) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: true)
//        self.navigationController?.interactivePopGestureRecognizer?.enabled = false
        // Left navigation button
        let backButton = UIButton(type: .Custom)
        backButton.setImage(UIImage.fontAwesomeIconWithName(FontAwesome.AngleLeft, textColor: UIColor.whiteColor(), size: CGSize(width: 26, height: 26)), forState: .Normal)
        backButton.frame = CGRect(x: 0, y: 0, width: 32, height: 32)
        backButton.layer.cornerRadius = 16
        backButton.layer.borderColor = UIColor.whiteColor().CGColor
        backButton.layer.borderWidth = 1
        backButton.addTarget(self, action: "backButtonTapped:", forControlEvents: .TouchUpInside)
        let backBarButtonItem = UIBarButtonItem(customView: backButton)
        self.navigationItem.leftBarButtonItem = backBarButtonItem
        
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        if sharedV2TAds.shouldShowHomeBanner {
            let adsView = UIView()
            sharedV2TAds.showBannerAdsInView(sharedV2TAds.bannerReuseableCellAdsNetwork, view: adsView, viewController: self) { (banner) -> Void in
                self.tableView.tableHeaderView = adsView
                (self.tableView as! V2TTableView).headerViewHeight = 50
                self.tableView.reloadData()
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source
    

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.songsList.count
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell: SongItemTableViewCell = tableView.dequeueReusableCellWithIdentifier(reuseCellIdentifier, forIndexPath: indexPath) as! SongItemTableViewCell
        cell.extraButtonTappedHandler = extraButtonAtCellTappedHandler
        cell.music = self.songsList[indexPath.row]
        cell.songNumberLabel.text = String(indexPath.row + 1)
        cell.isOnline = self.isOnline
        
        return cell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        var music = songsList[indexPath.row]
        if  music.path != "" {
            CSN.musicPlayer.playItems(self.songsList, startAtIndex: indexPath.row)
        } else {
            CSN.API.getDetailsMusic(music: music, success: { (result) -> Void in
                if result["music_info"] != nil {
                    music = Music.fromDictionary(result["music_info"].dictionaryObject!)
                    self.songsList[indexPath.row] = music
                    CSN.musicPlayer.playItems(self.songsList, startAtIndex: indexPath.row)
                }
                })
                { (error) -> Void in
                    //
            }
        }
        
        // Show interstitial ads
        if sharedV2TAds.shouldSHowInterstitalOnPlay {
            sharedV2TAds.showInterstitialAdsFromRootViewController(self)
        }
    }
    
    override func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let selectCategoryView = UIView()
        selectCategoryView.backgroundColor = UIColor(rgba: "#E6E6E6")
        
        let selectCategoryLabel: UILabel = UILabel()
        selectCategoryLabel.textColor = UIColor.blackColor()
        selectCategoryLabel.text = headerName
        selectCategoryLabel.sizeToFit()
        
        let showDetailCategoryButton: UIButton = UIButton()
        showDetailCategoryButton.addTarget(self, action: "showDetailCategory:", forControlEvents: UIControlEvents.TouchUpInside)
        
        let iconSize = CGSize(width: 24, height: 24)
        showDetailCategoryButton.setImage(UIImage.fontAwesomeIconWithName(FontAwesome.ChevronRight, textColor: UIColor(rgba: "#C62828"), size: iconSize), forState: UIControlState.Normal)
        
        let selectCategoryButton: UIButton = UIButton()
        selectCategoryButton.setTitle(NSLocalizedString("Danh mục", comment: ""), forState: .Normal)
        selectCategoryButton.setTitleColor(UIColor(rgba: "#C62828"), forState: .Normal)
        selectCategoryButton.titleLabel?.font = UIFont.systemFontOfSize(15)
        selectCategoryButton.backgroundColor = UIColor.whiteColor()
        selectCategoryButton.layer.cornerRadius = 8
        selectCategoryButton.addTarget(self, action: "selectCategory:", forControlEvents: UIControlEvents.TouchUpInside)
        
        let views = ["selectCategoryLabel": selectCategoryLabel,"showDetailCategoryButton": showDetailCategoryButton,"selectCategoryButton": selectCategoryButton,"view": selectCategoryView]
        
        selectCategoryView.addSubview(selectCategoryLabel)
        selectCategoryView.addSubview(selectCategoryButton)
        selectCategoryView.addSubview(showDetailCategoryButton)
        
        selectCategoryLabel.translatesAutoresizingMaskIntoConstraints = false
        selectCategoryButton.translatesAutoresizingMaskIntoConstraints = false
        showDetailCategoryButton.translatesAutoresizingMaskIntoConstraints = false
        
        let horizontallayoutContraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|-[selectCategoryLabel][showDetailCategoryButton(44)]->=8@750-[selectCategoryButton(80)]-|", options: .AlignAllCenterY, metrics: nil, views: views)
        selectCategoryView.addConstraints(horizontallayoutContraints)
        
        selectCategoryView.addConstraint(NSLayoutConstraint(item: selectCategoryButton, attribute: .CenterY, relatedBy: .Equal, toItem: selectCategoryView, attribute: .CenterY, multiplier: 1, constant: 0))
        
         selectCategoryView.addConstraint(NSLayoutConstraint(item: selectCategoryLabel, attribute: .CenterY, relatedBy: .Equal, toItem: selectCategoryView, attribute: .CenterY, multiplier: 1, constant: 0))
        
        selectCategoryView.addConstraint(NSLayoutConstraint(item: showDetailCategoryButton, attribute: .CenterY, relatedBy: .Equal, toItem: selectCategoryView, attribute: .CenterY, multiplier: 1, constant: 0))
        
        return selectCategoryView
    }
    
    override func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if showCategoryButton == true {
            return 44
        } else {
            return 0
        }
    }
    
    override func tableView(tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        if !showDetailButton {
            return UIView()
        }
        let showDetailCategoryView = UIView()
        showDetailCategoryView.backgroundColor = UIColor.whiteColor()
        
        let showDetailCategoryButton: UIButton = UIButton()
        showDetailCategoryButton.setTitle(NSLocalizedString("Xem đầy đủ", comment: ""), forState: .Normal)
        showDetailCategoryButton.setTitleColor(UIColor(rgba: "#C62828"), forState: .Normal)
        showDetailCategoryButton.titleLabel?.font = UIFont.systemFontOfSize(15)
        showDetailCategoryButton.layer.cornerRadius = 4
        showDetailCategoryButton.addTarget(self, action: "showDetailCategory:", forControlEvents: UIControlEvents.TouchUpInside)
        
        showDetailCategoryView.addSubview(showDetailCategoryButton)
        showDetailCategoryButton.translatesAutoresizingMaskIntoConstraints = false
        
        showDetailCategoryView.addConstraint(NSLayoutConstraint(item: showDetailCategoryButton, attribute: .CenterX, relatedBy: .Equal, toItem: showDetailCategoryView, attribute: .CenterX, multiplier: 1, constant: 0))
        
        showDetailCategoryView.addConstraint(NSLayoutConstraint(item: showDetailCategoryButton, attribute: .CenterY, relatedBy: .Equal, toItem: showDetailCategoryView, attribute: .CenterY, multiplier: 1, constant: 0))
        
        return showDetailCategoryView

    }
    
    override func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if showDetailButton == true {
            return 44
        } else {
            return 0
        }
    }

    // MARK: loadMore
    
    override func scrollViewDidEndDragging(scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        let currentOffset = scrollView.contentOffset.y
        let maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height
        if (maximumOffset - currentOffset <= 100) {
            loadMoreHandler?()
        }
    }
    // MARK: - DZNEmptyDataSetSource
    
    func buttonTitleForEmptyDataSet(scrollView: UIScrollView!, forState state: UIControlState) -> NSAttributedString! {
        let string = NSLocalizedString("Làm mới dữ liệu.", comment: "")
        let attributes = [NSFontAttributeName: UIFont.boldSystemFontOfSize(16),
            NSForegroundColorAttributeName: UIColor.grayColor()]
        return NSAttributedString(string: string, attributes: attributes)
    }
    
    func imageForEmptyDataSet(scrollView: UIScrollView!) -> UIImage! {
        return UIImage.fontAwesomeIconWithName(FontAwesome.Music, textColor: UIColor(rgba: "#E0E0E0"), size: CGSize(width: 80, height: 80))
    }

    func emptyDataSetDidTapView(scrollView: UIScrollView!) {
        viewDidLoadHandler?()
    }
    
    func emptyDataSetDidTapImage(scrollView: UIScrollView!) {
        viewDidLoadHandler?()
    }

    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    // MARK: - Tableview delegate
    
    // MARK: - SwipeRefreshControl delegate
    
    // MARK: showCategory
    
    func selectCategory(sender: UIButton!) {
        self.didSelectCategoryHandler?()
    }
    
    func showDetailCategory(sender: UIButton!) {
        self.showDetailCategoryHandler?()
    }
    
    func refreshList(sender: UIButton!) {
        self.reloadHandler?()
    }
    
}
