//
//  VideoItemTableViewCell.swift
//  ChiaSeNhac
//
//  Created by Vũ Trung Thành on 12/4/15.
//  Copyright © 2015 V2T Multimedia. All rights reserved.
//

import UIKit
import Haneke
import FontAwesomeIconFactory

class VideoItemTableViewCell: UITableViewCell {
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var artistLabel: UILabel!
    @IBOutlet weak var listenLabel: UILabel!
    @IBOutlet weak var bitrateLabel: UILabel!
    @IBOutlet weak var durationLabel: UILabel!
    @IBOutlet weak var downloadLabel: UILabel!
    @IBOutlet weak var videoImageView: UIImageView!
    @IBOutlet weak var downloadIcon: NIKFontAwesomeButton!
    
    var isOnline: Bool!
    var extraButtonTappedHandler: (VideoItemTableViewCell -> Void)?

    var music: Music?
     {
        didSet {
            if let music = music {
                nameLabel.text = music.musicTitle
                artistLabel.text = music.musicArtist
                listenLabel.text = music.musicListen
                downloadLabel.text = music.musicDownloads
//                if !CSN.Setting.isShowDownloadFunction {
//                    downloadIcon.hidden = true
//                    downloadLabel.hidden = true
//                } else {
//                    downloadIcon.hidden = false
//                    downloadLabel.hidden = false
//                }
                if let musicLength = music.musicLength {
                    durationLabel.text = "\(Int(musicLength)!/60): \(Int(musicLength)!%60)"
                }
                
                if let thumbUrl = music.thumbnailUrl {
                    videoImageView.hnk_setImageFromURL(NSURL(string: thumbUrl)!)
                }
                
                if let videoThumbUrl = music.videoThumbnail {
                    videoImageView.hnk_setImageFromURL(NSURL(string: videoThumbUrl)!)
                }
                
                switch music.musicWidth {
                case "1920":
                    bitrateLabel.text = "1080p"
                    break
                case "1280":
                    bitrateLabel.text = "720p"
                    break
                case "720":
                    bitrateLabel.text = "480p"
                    break
                case "480":
                    bitrateLabel.text = "320p"
                    break
                default:
                    bitrateLabel.text = ""
                    break
                }
                }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func extraButtonTapped(sender: AnyObject) {
        if !isOnline {
            self.extraButtonTappedHandler?(self)
        }
        else {
            NSNotificationCenter.defaultCenter().postNotificationName(CSN.Notification.showExtraOptionVideoItem, object: self.music)
        }
        
    }
    
}
