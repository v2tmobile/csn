//
//  CreateNewPlaylistViewController.swift
//  ChiaSeNhac
//
//  Created by Phan Hữu Thắng on 3/4/16.
//  Copyright © 2016 V2T Multimedia. All rights reserved.
//

import UIKit

class CreateNewPlaylistViewController: V2TViewController {
    
    @IBOutlet weak var playlistNameTextField: UITextField!

    var cancelButtonTappedHandler: (Void -> Void)?
    var addButtonTappedHandler: (Void -> Void)?
    var createdPlaylistHandler: (Musics -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        playlistNameTextField.becomeFirstResponder()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func cancelButtonTapped(sender: UIButton) {
        self.view.endEditing(true)
        cancelButtonTappedHandler?()
    }
    
    @IBAction func addButtonTapped(sender: UIButton) {
        self.view.endEditing(true)
        if let playlistName = playlistNameTextField.text?.stringByTrimmingCharactersInSet(
            NSCharacterSet.whitespaceAndNewlineCharacterSet()
            ) {
                if playlistName != "" {
                    createNewPlaylist(playlistName)
                } else {
                    playlistNameTextField.highlighted = true
                    playlistNameTextField.becomeFirstResponder()
                }
        } else {
            playlistNameTextField.highlighted = true
            playlistNameTextField.becomeFirstResponder()
        }
        addButtonTappedHandler?()
    }
    
    private func createNewPlaylist(playlistName: String) {
        let playlist = Musics()
        
        playlist.name = playlistName
        
        try! CSN.uiRealm.write({
            CSN.uiRealm.add(playlist)
            self.createdPlaylistHandler?(playlist)
        })
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
