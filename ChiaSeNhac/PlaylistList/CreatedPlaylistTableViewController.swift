//
//  CreatedPlaylistTableViewController.swift
//  ChiaSeNhac
//
//  Created by Phan Hữu Thắng on 3/5/16.
//  Copyright © 2016 V2T Multimedia. All rights reserved.
//

import UIKit
import RealmSwift
import UIColor_Hex_Swift
import DZNEmptyDataSet
import FontAwesome_swift
import CarbonKit

class CreatedPlaylistTableViewController: V2TTableViewController, DZNEmptyDataSetSource, DZNEmptyDataSetDelegate {
    
    private let reuseableCellIdentifiler = "cell"
    
    var playlists: Results<Musics>?
    var swipeRefreshControl: CarbonSwipeRefresh!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.emptyDataSetSource = self
        self.tableView.emptyDataSetDelegate = self
        self.tableView.tableFooterView = UIView()
        
        title = NSLocalizedString("Playlist", comment: "")
        swipeRefreshControl = CarbonSwipeRefresh.init(scrollView: self.tableView)
        swipeRefreshControl.colors = [UIColor(rgba: "#FF2D55")]
        swipeRefreshControl.addTarget(self, action: "refreshList:", forControlEvents: UIControlEvents.ValueChanged)
        self.view.addSubview(swipeRefreshControl)
        
        self.playlists = CSN.uiRealm.objects(Musics).filter("name != 'Favorites' && name != 'Histories' && name != 'Downloads'")
        self.tableView.reloadData()
        self.swipeRefreshControl.endRefreshing()
        
        let createNewPlaylistButtonItem = UIBarButtonItem(image: UIImage.fontAwesomeIconWithName(FontAwesome.Plus, textColor: UIColor(rgba: "#FF2D55"), size: CGSize(width: 24, height: 24)), style: .Plain, target: self, action: "showCreateNewPlaylistVC")
        navigationItem.rightBarButtonItem = createNewPlaylistButtonItem

    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        self.navigationController?.interactivePopGestureRecognizer?.enabled = false
        // Left navigation button
        let backButton = UIButton(type: .Custom)
        backButton.setImage(UIImage.fontAwesomeIconWithName(FontAwesome.AngleLeft, textColor: UIColor.whiteColor(), size: CGSize(width: 26, height: 26)), forState: .Normal)
        backButton.frame = CGRect(x: 0, y: 0, width: 32, height: 32)
        backButton.layer.cornerRadius = 16
        backButton.layer.borderColor = UIColor.whiteColor().CGColor
        backButton.layer.borderWidth = 1
        backButton.addTarget(self, action: "backButtonTapped:", forControlEvents: .TouchUpInside)
        let backBarButtonItem = UIBarButtonItem(customView: backButton)
        self.navigationItem.leftBarButtonItem = backBarButtonItem
        
        let tracker = GAI.sharedInstance().defaultTracker
        tracker.set(kGAIScreenName, value: "Playlist")
        
        let builder = GAIDictionaryBuilder.createScreenView()
        tracker.send(builder.build() as [NSObject : AnyObject])
    }
    
    func backButtonTapped(button: UIButton) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let playlists = self.playlists {
            return playlists.count
        } else {
            return 0
        }
    }

    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier(reuseableCellIdentifiler, forIndexPath: indexPath)
        cell.textLabel?.text = playlists![indexPath.row].name
        cell.detailTextLabel?.text = "\(playlists![indexPath.row].musics.count) bài hát"
        let backgroundView = UIView()
        backgroundView.backgroundColor = UIColor(rgba: "#424242")
        cell.selectedBackgroundView = backgroundView
        return cell
    }

    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        
        let storyboard = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle())
        let songListVC = storyboard.instantiateViewControllerWithIdentifier("SongList") as! SongsListTableViewController
        
        songListVC.isOnline = false
        songListVC.showCategoryButton = false
        songListVC.showDetailButton = false
        
        let playlistName = playlists![indexPath.row].name
        
        songListVC.viewDidLoadHandler = {
            songListVC.title = playlistName
            self.loadData(playlistName, songListVC: songListVC)
            
//            // Play all
//            let createNewPlaylistButtonItem = UIBarButtonItem(image: UIImage.fontAwesomeIconWithName(FontAwesome.Plus, textColor: UIColor(rgba: "#FF2D55"), size: CGSize(width: 24, height: 24)), style: .Plain, target: self, action: "playAllMusicInPlaylist:")
//            songListVC.navigationItem.rightBarButtonItem = createNewPlaylistButtonItem

        }
        
        self.registerObserver(CSN.Notification.deleteDone, object: nil, queue: nil) { (music) -> Void in
            self.loadData(playlistName, songListVC: songListVC)
        }

        songListVC.extraButtonAtCellTappedHandler = { cell in
            var items: [BottomMenuViewItem] = []
            let iconSize = CGSize(width: 26, height: 26)
            items.append(BottomMenuViewItem(icon: UIImage.fontAwesomeIconWithName(FontAwesome.Play, textColor: UIColor.grayColor(), size: iconSize), title: NSLocalizedString("Chơi ngay", comment: ""), selectedAction: { (Void) -> Void in
                CSN.playMusic(cell.music!)
            }))
            items.append(BottomMenuViewItem(icon: UIImage.fontAwesomeIconWithName(FontAwesome.ListOL, textColor: UIColor.grayColor(), size: iconSize), title: NSLocalizedString("Thêm vào danh sách nghe", comment: ""), selectedAction: { (Void) -> Void in
                CSN.musicPlayer.addItemToQueue(cell.music!)
                NSNotificationCenter.defaultCenter().postNotificationName(CSN.Notification.updateTrackList, object: cell.music)
            }))
            items.append(BottomMenuViewItem(icon: UIImage.fontAwesomeIconWithName(FontAwesome.Heart, textColor: UIColor.grayColor(), size: iconSize), title: NSLocalizedString("Thêm vào yêu thích", comment: ""), selectedAction: { (Void) -> Void in
                CSN.saveFavorite(cell.music!)
            }))
            items.append(BottomMenuViewItem(icon: UIImage.fontAwesomeIconWithName(FontAwesome.Trash, textColor: UIColor.grayColor(), size: iconSize), title: NSLocalizedString("Xoá", comment: ""), selectedAction: { (Void) -> Void in
                self.deleteData(playlistName, music: cell.music!)
            }))
            items.append(BottomMenuViewItem(icon: UIImage.fontAwesomeIconWithName(FontAwesome.AngleDown, textColor: UIColor.grayColor(), size: iconSize), title: NSLocalizedString("Thôi", comment: ""), selectedAction: { (Void) -> Void in
                //
            }))
            
            let bottomMenu = BottomMenuView(items: items, didSelectedHandler: nil)
            let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
            bottomMenu.showInViewController((appDelegate.window?.rootViewController)!)
        }

        
        self.navigationController?.pushViewController(songListVC, animated: true)
    }
    
//    func playAllMusicInPlaylist(music: Music) {
//        CSN.playMusic(music)
//    }
    
    func loadData(playlistName: String, songListVC: SongsListTableViewController) {
        let musicList = CSN.uiRealm.objects(Musics).filter("name = '\(playlistName)'")[0].musics.filter("musicWidth = '0'").sorted("createdAtDate", ascending: false)
        songListVC.songsList = Array(musicList)
        songListVC.tableView.reloadData()
    }

    func deleteData(playlistName: String, music: Music) {
        let musicsList = CSN.uiRealm.objects(Musics).filter("name = '\(playlistName)'")[0].musics
            dispatch_async(dispatch_get_main_queue()) {
                autoreleasepool {
                    try! CSN.uiRealm.write({ () -> Void in
                        if let index = musicsList.indexOf(music) {
                            musicsList.removeAtIndex(index)
                            NSNotificationCenter.defaultCenter().postNotificationName(CSN.Notification.deleteDone, object: nil)
                        }
                    })
                }
            }
    }
    
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            try! CSN.uiRealm.write({ () -> Void in
                CSN.uiRealm.delete(self.playlists![indexPath.row])
                self.tableView.reloadData()
            })
        }
    }

    /*
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    // Get the new view controller using segue.destinationViewController.
    // Pass the selected object to the new view controller.
    }
    */
    
    // MARK: SwipeRefreshControl delegate
    
    // MARK: - DZNEmptyDataSetSource
    
    func buttonTitleForEmptyDataSet(scrollView: UIScrollView!, forState state: UIControlState) -> NSAttributedString! {
        let string = NSLocalizedString("Tạo playlist mới.", comment: "")
        let attributes = [NSFontAttributeName: UIFont.boldSystemFontOfSize(16),
            NSForegroundColorAttributeName: UIColor.grayColor()]
        return NSAttributedString(string: string, attributes: attributes)
    }
    
    func imageForEmptyDataSet(scrollView: UIScrollView!) -> UIImage! {
        return UIImage.fontAwesomeIconWithName(FontAwesome.List, textColor: UIColor(rgba: "#E0E0E0"), size: CGSize(width: 80, height: 80))
    }
    
    func emptyDataSetDidTapView(scrollView: UIScrollView!) {
        self.showCreateNewPlaylistVC()
    }
    
    func emptyDataSetDidTapButton(scrollView: UIScrollView!) {
        self.showCreateNewPlaylistVC()
    }
    
    func showCreateNewPlaylistVC() {
        CSN.Setting.isCreatePlaylist = true
        let popup = PopUpViewController(size: CGSize(width: 280, height: 120))
        //set Size popUp in iphone 4S:
        if UIScreen.mainScreen().bounds.height == 480 {
            popup.heightMultiplierViewContainer = 0.6
        }
        //
        let storyboard = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle())
        let createNewPlaylistVC = storyboard.instantiateViewControllerWithIdentifier("CreateNewPlaylist") as! CreateNewPlaylistViewController
        popup.addChildViewController(createNewPlaylistVC)
        popup.viewContainer.addSubview(createNewPlaylistVC.view)
        createNewPlaylistVC.view.translatesAutoresizingMaskIntoConstraints = false
        if UIScreen.mainScreen().bounds.height == 480 {
            popup.viewContainer.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:|[createNewPlaylistView]|", options: NSLayoutFormatOptions(), metrics: nil, views: ["createNewPlaylistView": createNewPlaylistVC.view]))
        }
        popup.viewContainer.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:|[createNewPlaylistView]|", options: NSLayoutFormatOptions(), metrics: nil, views: ["createNewPlaylistView": createNewPlaylistVC.view]))
        popup.viewContainer.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|[createNewPlaylistView]|", options: NSLayoutFormatOptions(), metrics: nil, views: ["createNewPlaylistView": createNewPlaylistVC.view]))
        createNewPlaylistVC.cancelButtonTappedHandler = {
            popup.hide()
        }
        createNewPlaylistVC.addButtonTappedHandler = {
            popup.hide()
            self.playlists = CSN.uiRealm.objects(Musics).filter("name != 'Favorites' && name != 'Histories' && name != 'Downloads'")
            self.tableView.reloadData()
        }
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        popup.showInViewController((appDelegate.window?.rootViewController)!)
    }
}
