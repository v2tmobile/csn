//
//  CategoryDetailViewController.swift
//  ChiaSeNhac
//
//  Created by Vũ Trung Thành on 1/2/16.
//  Copyright © 2016 V2T Multimedia. All rights reserved.
//

import UIKit
import CarbonKit
import FontAwesome_swift
import SwiftyJSON
import DZNEmptyDataSet

class CategoryDetailsViewController: V2TViewController, CarbonTabSwipeNavigationDelegate {

    @IBOutlet weak var contentView: UIView!
    
    var catUrlString: String!
    var tabBarTitle: String!
    var categoryType: CategoryType = .Song
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = self.tabBarTitle
        var itemsView = [
            NSLocalizedString("Hot", comment: ""),
            NSLocalizedString("Mới", comment: ""),
        ]
        if self.categoryType == .Song {
            itemsView.append(NSLocalizedString("Album", comment: ""))
        }
        if self.categoryType == .Video {
            
        }
        let tabSwipeNavigation = CarbonTabSwipeNavigation(items: itemsView, delegate: self)
        tabSwipeNavigation.setIndicatorColor(UIColor(rgba: "#d32f2f"))
        tabSwipeNavigation.setSelectedColor(UIColor(rgba: "#d32f2f"))
        tabSwipeNavigation.setNormalColor(UIColor(rgba: "#424242"))
        switch UIDevice.currentDevice().userInterfaceIdiom {
            case .Phone:
                // It's an iPhone
                tabSwipeNavigation.setTabBarHeight(32)
            case .Pad:
                // It's an iPad
                tabSwipeNavigation.setTabBarHeight(44)
            default:
                // Uh, oh! What could it be?
                tabSwipeNavigation.setTabBarHeight(32)
        }

        if self.categoryType == .Song {
            tabSwipeNavigation.carbonSegmentedControl?.setWidth(UIScreen.mainScreen().bounds.width/3, forSegmentAtIndex: 0)
            tabSwipeNavigation.carbonSegmentedControl?.setWidth(UIScreen.mainScreen().bounds.width/3, forSegmentAtIndex: 1)
            tabSwipeNavigation.carbonSegmentedControl?.setWidth(UIScreen.mainScreen().bounds.width/3, forSegmentAtIndex: 2)
        }
        if self.categoryType == .Video {
            tabSwipeNavigation.carbonSegmentedControl?.setWidth(UIScreen.mainScreen().bounds.width/2, forSegmentAtIndex: 0)
            tabSwipeNavigation.carbonSegmentedControl?.setWidth(UIScreen.mainScreen().bounds.width/2, forSegmentAtIndex: 1)
        }
        tabSwipeNavigation.setIndicatorHeight(2)
        tabSwipeNavigation.insertIntoRootViewController(self, andTargetView: contentView)
    }
    
    func backButtonTapped(button: UIButton) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        self.navigationController?.interactivePopGestureRecognizer?.enabled = false
        // Left navigation button
        let backButton = UIButton(type: .Custom)
        backButton.setImage(UIImage.fontAwesomeIconWithName(FontAwesome.AngleLeft, textColor: UIColor.whiteColor(), size: CGSize(width: 26, height: 26)), forState: .Normal)
        backButton.frame = CGRect(x: 0, y: 0, width: 32, height: 32)
        backButton.layer.cornerRadius = 16
        backButton.layer.borderColor = UIColor.whiteColor().CGColor
        backButton.layer.borderWidth = 1
        backButton.addTarget(self, action: "backButtonTapped:", forControlEvents: .TouchUpInside)
        let backBarButtonItem = UIBarButtonItem(customView: backButton)
        self.navigationItem.leftBarButtonItem = backBarButtonItem
        
        let tracker = GAI.sharedInstance().defaultTracker
        tracker.set(kGAIScreenName, value: "Xem đầy đủ")
        
        let builder = GAIDictionaryBuilder.createScreenView()
        tracker.send(builder.build() as [NSObject : AnyObject])
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - CarbonTabSwipeNavigationDelegate
    func carbonTabSwipeNavigation(carbonTabSwipeNavigation: CarbonTabSwipeNavigation, viewControllerAtIndex index: UInt) -> UIViewController {
        switch index {
        case 0:
            if self.categoryType == .Song {
                return self.hotSongListViewController()
            }
            if self.categoryType == .Video {
                return self.hotVideoListViewController()
            }
            break
        case 1:
            if self.categoryType == .Song {
                return self.newSongListViewController()
            }
            if self.categoryType == .Video {
                return self.newVideoListViewController()
            }
            break
        case 2:
            return self.albumListViewController()
        default:
            return self.hotSongListViewController()
        }
        
        // return viewController at index
        return self.hotSongListViewController()
    }
    
    func hotSongListViewController() -> UIViewController {
        
        let storyboard = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle())
        let songListVC = storyboard.instantiateViewControllerWithIdentifier("SongList") as! SongsListTableViewController
        
        songListVC.showCategoryButton = false
        songListVC.showDetailButton = false

        songListVC.viewDidLoadHandler = {
            self.loadHotSongList(songListVC)
        }
        
        songListVC.reloadHandler = {
            self.loadHotSongList(songListVC)
            songListVC.swipeRefreshControl.endRefreshing()
            sharedV2TAds.showInterstitialAdsFromRootViewController(songListVC)
        }
        
        return songListVC as UIViewController
    }

    func loadHotSongList(songListVC: SongsListTableViewController) {
        songListVC.swipeRefreshControl.startRefreshing()
        CSN.API.getCategory(name: self.catUrlString, page: 1, loadCache: false, success: { (result) -> Void in
            if let result: JSON = result["hot"] {
                if let musicArray = result["music"].array {
                    for item: JSON in musicArray {
                        songListVC.songsList.append(Music.fromDictionary(item.dictionaryObject!))
                    }
                }
                songListVC.tableView?.reloadData()
            }
            songListVC.swipeRefreshControl.endRefreshing()
            }, failure: { (error) -> Void in
        })
    }
    
    func newSongListViewController() -> UIViewController {
        
        let storyboard = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle())
        let songListVC = storyboard.instantiateViewControllerWithIdentifier("SongList") as! SongsListTableViewController
        
        songListVC.showCategoryButton = false
        songListVC.showDetailButton = false
        
        songListVC.viewDidLoadHandler = {
            self.loadNewSongList(songListVC)
        }
        
        songListVC.reloadHandler = {
            self.loadNewSongList(songListVC)
            songListVC.swipeRefreshControl.endRefreshing()
            sharedV2TAds.showInterstitialAdsFromRootViewController(songListVC)
        }

        songListVC.loadMoreHandler = {
            songListVC.currentPage++
            songListVC.swipeRefreshControl.startRefreshing()
            CSN.API.getCategory(name: self.catUrlString, page: songListVC.currentPage, loadCache: true, success: { (result) -> Void in
                if let result: JSON = result["new"] {
                    if let musicArray = result["music"].array {
                        for item: JSON in musicArray {
                            songListVC.songsList.append(Music.fromDictionary(item.dictionaryObject!))
                        }
                    }
                    songListVC.tableView?.reloadData()
                }
                songListVC.swipeRefreshControl.endRefreshing()
                }, failure: { (error) -> Void in
            })
        }

        return songListVC as UIViewController
    }
    
    func loadNewSongList(songListVC: SongsListTableViewController) {
        songListVC.swipeRefreshControl.startRefreshing()
        CSN.API.getCategory(name: self.catUrlString, page: 1, loadCache: false, success: { (result) -> Void in
            if let result: JSON = result["new"] {
                if let musicArray = result["music"].array {
                    for item: JSON in musicArray {
                        songListVC.songsList.append(Music.fromDictionary(item.dictionaryObject!))
                    }
                }
                songListVC.tableView?.reloadData()
            }
            songListVC.swipeRefreshControl.endRefreshing()
            }, failure: { (error) -> Void in
        })
    }

    func hotVideoListViewController() -> UIViewController {
        
        let storyboard = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle())
        let videoListVC = storyboard.instantiateViewControllerWithIdentifier("VideoList") as! VideosListTableViewController
        
        videoListVC.showCategoryButton = false
        videoListVC.showDetailButton = false

        videoListVC.viewDidLoadHandler = {
            self.loadHotVideoList(videoListVC)
        }
        
        videoListVC.reloadHandler = {
            self.loadHotVideoList(videoListVC)
            videoListVC.swipeRefreshControl.endRefreshing()
            sharedV2TAds.showInterstitialAdsFromRootViewController(videoListVC)
        }
        
        return videoListVC as UIViewController
    }

    
    func loadHotVideoList(videoListVC: VideosListTableViewController) {
        videoListVC.swipeRefreshControl.startRefreshing()
        CSN.API.getCategory(name: self.catUrlString, page: 1, loadCache: false, success: { (result) -> Void in
            if let result: JSON = result["hot"] {
                if let musicArray = result["music"].array {
                    for item: JSON in musicArray {
                        videoListVC.videosList.append(Music.fromDictionary(item.dictionaryObject!))
                    }
                }
                videoListVC.tableView?.reloadData()
            }
            videoListVC.swipeRefreshControl.endRefreshing()
            }, failure: { (error) -> Void in
        })
    }
    
    func newVideoListViewController() -> UIViewController {
        
        let storyboard = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle())
        let videoListVC = storyboard.instantiateViewControllerWithIdentifier("VideoList") as! VideosListTableViewController
        
        videoListVC.showCategoryButton = false
        videoListVC.showDetailButton = false
        
        videoListVC.viewDidLoadHandler = {
            self.loadNewVideoList(videoListVC)
        }
        
        videoListVC.reloadHandler = {
            self.loadNewVideoList(videoListVC)
            videoListVC.swipeRefreshControl.endRefreshing()
            sharedV2TAds.showInterstitialAdsFromRootViewController(videoListVC)
        }
        
        videoListVC.loadMoreHandler = {
            videoListVC.currentPage++
            videoListVC.swipeRefreshControl.startRefreshing()
            CSN.API.getCategory(name: self.catUrlString, page: videoListVC.currentPage, loadCache: true, success: { (result) -> Void in
                if let result: JSON = result["new"] {
                    if let musicArray = result["music"].array {
                        for item: JSON in musicArray {
                            videoListVC.videosList.append(Music.fromDictionary(item.dictionaryObject!))
                        }
                    }
                    videoListVC.tableView?.reloadData()
                }
                videoListVC.swipeRefreshControl.endRefreshing()
                }, failure: { (error) -> Void in
            })

        }
        
        return videoListVC as UIViewController
    }
    
    func loadNewVideoList(videoListVC: VideosListTableViewController) {
        videoListVC.swipeRefreshControl.startRefreshing()
        CSN.API.getCategory(name: self.catUrlString, page: videoListVC.currentPage, loadCache: false, success: { (result) -> Void in
            if let result: JSON = result["new"] {
                if let musicArray = result["music"].array {
                    for item: JSON in musicArray {
                        videoListVC.videosList.append(Music.fromDictionary(item.dictionaryObject!))
                    }
                }
                videoListVC.tableView?.reloadData()
            }
            videoListVC.swipeRefreshControl.endRefreshing()
            }, failure: { (error) -> Void in
        })
    }
    
    func albumListViewController() -> UIViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle())
        let albumListVC = storyboard.instantiateViewControllerWithIdentifier("AlbumList") as! AlbumsCollectionViewController
        albumListVC.catUrlString = catUrlString
        albumListVC.listType = "category"
        return albumListVC as UIViewController
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    
}
