//
//  LoadingViewController.swift
//  TubeTrends
//
//  Created by Vũ Trung Thành on 2/21/16.
//  Copyright © 2016 V2T Multimedia. All rights reserved.
//

import UIKit

class LoadingViewController: V2TViewController {

    @IBOutlet weak var loadingIndicator: UIActivityIndicatorView!
    override func viewDidLoad() {
        super.viewDidLoad()
        loadingIndicator.startAnimating()
        // V2T Core
        let v2tCore = V2TCore(appName: "chiasenhac")
        v2tCore.registerDevice()
        v2tCore.getConfigs { (json) -> Void in
            CSN.Setting.globalJSON = json
            NSNotificationCenter.defaultCenter().postNotificationName(CSN.Notification.getAppConfigDone, object: nil)
            let storyboard = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle())
            let rootVC = storyboard.instantiateViewControllerWithIdentifier("MainViewController") as! MainViewController
            
            let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
            appDelegate.window?.rootViewController = rootVC
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
