//
//  FavoriteListViewController.swift
//  ChiaSeNhac
//
//  Created by Phan Hữu Thắng on 1/12/16.
//  Copyright © 2016 V2T Multimedia. All rights reserved.
//

import UIKit
import CarbonKit
import FontAwesome_swift
import RealmSwift

class FavoriteListViewController: V2TViewController {
    @IBOutlet weak var contentView: UIView!
    var itemsView = [
        NSLocalizedString("Bài hát", comment: ""),
        NSLocalizedString("Video", comment: ""),
    ]
    
    var currentEmbedViewController: UIViewController!

    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = NSLocalizedString("Nhạc yêu thích", comment: "")
        let tabSwipeNavigation = CarbonTabSwipeNavigation(items: itemsView, delegate: self)
        tabSwipeNavigation.setIndicatorColor(UIColor(rgba: "#d32f2f"))
        tabSwipeNavigation.setSelectedColor(UIColor(rgba: "#d32f2f"))
        tabSwipeNavigation.setNormalColor(UIColor(rgba: "#424242"))
        switch UIDevice.currentDevice().userInterfaceIdiom {
        case .Phone:
            // It's an iPhone
            tabSwipeNavigation.setTabBarHeight(32)
        case .Pad:
            // It's an iPad
            tabSwipeNavigation.setTabBarHeight(44)
        default:
            // Uh, oh! What could it be?
            tabSwipeNavigation.setTabBarHeight(32)
        }
        tabSwipeNavigation.carbonSegmentedControl?.setWidth(UIScreen.mainScreen().bounds.width/2, forSegmentAtIndex: 0)
        tabSwipeNavigation.carbonSegmentedControl?.setWidth(UIScreen.mainScreen().bounds.width/2, forSegmentAtIndex: 1)
        tabSwipeNavigation.setIndicatorHeight(2)
        tabSwipeNavigation.insertIntoRootViewController(self, andTargetView: contentView)

    }

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        self.navigationController?.interactivePopGestureRecognizer?.enabled = false
        // Left navigation button
        let backButton = UIButton(type: .Custom)
        backButton.setImage(UIImage.fontAwesomeIconWithName(FontAwesome.AngleLeft, textColor: UIColor.whiteColor(), size: CGSize(width: 26, height: 26)), forState: .Normal)
        backButton.frame = CGRect(x: 0, y: 0, width: 32, height: 32)
        backButton.layer.cornerRadius = 16
        backButton.layer.borderColor = UIColor.whiteColor().CGColor
        backButton.layer.borderWidth = 1
        backButton.addTarget(self, action: "backButtonTapped:", forControlEvents: .TouchUpInside)
        let backBarButtonItem = UIBarButtonItem(customView: backButton)
        self.navigationItem.leftBarButtonItem = backBarButtonItem
        //
        let tracker = GAI.sharedInstance().defaultTracker
        tracker.set(kGAIScreenName, value: "Nhạc yêu thích")
        
        let builder = GAIDictionaryBuilder.createScreenView()
        tracker.send(builder.build() as [NSObject : AnyObject])
    }
    
    func backButtonTapped(button: UIButton) {
        self.navigationController?.popViewControllerAnimated(true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // MARK: - CarbonTabSwipeNavigationDelegate
    func carbonTabSwipeNavigation(carbonTabSwipeNavigation: CarbonTabSwipeNavigation, viewControllerAtIndex index: UInt) -> UIViewController {
        switch index {
        case 0:
            return self.showSongList()
        case 1:
            return self.showVideoList()
        default:
            return self.showSongList()
        }
        
    }
    
    func showSongList() -> UIViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle())
        let songListVC = storyboard.instantiateViewControllerWithIdentifier("SongList") as! SongsListTableViewController
        
        songListVC.isOnline = false
        songListVC.showCategoryButton = false
        songListVC.showDetailButton = false
        
        songListVC.reloadHandler = {
            songListVC.swipeRefreshControl.endRefreshing()
            sharedV2TAds.showInterstitialAdsFromRootViewController(songListVC)
        }
        
        songListVC.viewDidLoadHandler = {
            self.loadSongData(songListVC)
        }
        
        self.registerObserver(CSN.Notification.favoriteDone, object: nil, queue: nil) { (music) -> Void in
            self.loadSongData(songListVC)

        }
        
        self.registerObserver(CSN.Notification.deleteDone, object: nil, queue: nil) { (music) -> Void in
            self.loadSongData(songListVC)
            
        }

        songListVC.extraButtonAtCellTappedHandler = { cell in
            var items: [BottomMenuViewItem] = []
            let iconSize = CGSize(width: 26, height: 26)
            items.append(BottomMenuViewItem(icon: UIImage.fontAwesomeIconWithName(FontAwesome.Play, textColor: UIColor.grayColor(), size: iconSize), title: NSLocalizedString("Chơi ngay", comment: ""), selectedAction: { (Void) -> Void in
                CSN.playMusic(cell.music!)
            }))
            items.append(BottomMenuViewItem(icon: UIImage.fontAwesomeIconWithName(FontAwesome.ListOL, textColor: UIColor.grayColor(), size: iconSize), title: NSLocalizedString("Thêm vào danh sách nghe", comment: ""), selectedAction: { (Void) -> Void in
                CSN.musicPlayer.addItemToQueue(cell.music!)
                NSNotificationCenter.defaultCenter().postNotificationName(CSN.Notification.updateTrackList, object: cell.music)
            }))
            items.append(BottomMenuViewItem(icon: UIImage.fontAwesomeIconWithName(FontAwesome.CloudDownload, textColor: UIColor.grayColor(), size: iconSize), title: NSLocalizedString("Tải về", comment: ""), selectedAction: { (Void) -> Void in
                if let musicWidth = cell.music!.musicWidth {
                    if Int(musicWidth) != 0 {
                        CSN.showDownloadVideoQualityOptions(cell.music!, viewController: self.tabBarController!)
                    }
                    else {
                        CSN.showDownloadMusicQualityOptions(cell.music!, viewController: self.tabBarController!)
                    }
                }
                
            }))
            items.append(BottomMenuViewItem(icon: UIImage.fontAwesomeIconWithName(FontAwesome.Trash, textColor: UIColor.grayColor(), size: iconSize), title: NSLocalizedString("Xoá", comment: ""), selectedAction: { (Void) -> Void in
                self.deleteData(cell.music!)
            }))
            items.append(BottomMenuViewItem(icon: UIImage.fontAwesomeIconWithName(FontAwesome.AngleDown, textColor: UIColor.grayColor(), size: iconSize), title: NSLocalizedString("Thôi", comment: ""), selectedAction: { (Void) -> Void in
                //
            }))

            let bottomMenu = BottomMenuView(items: items, didSelectedHandler: nil)
            let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
            bottomMenu.showInViewController((appDelegate.window?.rootViewController)!)
        }
        
        return songListVC as UIViewController

    }
    
    func loadSongData(songListVC: SongsListTableViewController) {
        let musicList = CSN.uiRealm.objects(Musics).filter("name = 'Favorites'")[0].musics.filter("musicWidth = '0'").sorted("createdAtDate", ascending: false)
        songListVC.songsList = Array(musicList)
        songListVC.tableView.reloadData()
    }

    func showVideoList() -> UIViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle())
        let videoListVC = storyboard.instantiateViewControllerWithIdentifier("VideoList") as! VideosListTableViewController
        
        videoListVC.isOnline = false
        videoListVC.showCategoryButton = false
        videoListVC.showDetailButton = false
        
        videoListVC.viewDidLoadHandler = {
            self.loadVideoData(videoListVC)
        }
        
        videoListVC.reloadHandler = {
            videoListVC.swipeRefreshControl.endRefreshing()
            sharedV2TAds.showInterstitialAdsFromRootViewController(videoListVC)
        }
        
        self.registerObserver(CSN.Notification.favoriteDone, object: nil, queue: nil) { (music) -> Void in
            self.loadVideoData(videoListVC)
        }
        
        self.registerObserver(CSN.Notification.deleteDone, object: nil, queue: nil) { (music) -> Void in
            self.loadVideoData(videoListVC)
        }
        
        videoListVC.extraButtonAtCellTappedHandler = { cell in
            var items: [BottomMenuViewItem] = []
            let iconSize = CGSize(width: 26, height: 26)
            items.append(BottomMenuViewItem(icon: UIImage.fontAwesomeIconWithName(FontAwesome.Play, textColor: UIColor.grayColor(), size: iconSize), title: NSLocalizedString("Chơi ngay", comment: ""), selectedAction: { (Void) -> Void in
                let storyboard = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle())
                let videoDetailsVC: VideoDetailsViewController! = storyboard.instantiateViewControllerWithIdentifier("videoDetails") as? VideoDetailsViewController
                videoDetailsVC?.music = cell.music
                videoDetailsVC.showInViewController(self.navigationController!)
            }))
            items.append(BottomMenuViewItem(icon: UIImage.fontAwesomeIconWithName(FontAwesome.CloudDownload, textColor: UIColor.grayColor(), size: iconSize), title: NSLocalizedString("Tải về", comment: ""), selectedAction: { (Void) -> Void in
                CSN.showDownloadVideoQualityOptions(cell.music!, viewController: self.tabBarController!)
            }))
            items.append(BottomMenuViewItem(icon: UIImage.fontAwesomeIconWithName(FontAwesome.Trash, textColor: UIColor.grayColor(), size: iconSize), title: NSLocalizedString("Xoá", comment: ""), selectedAction: { (Void) -> Void in
                self.deleteData(cell.music!)
            }))
            items.append(BottomMenuViewItem(icon: UIImage.fontAwesomeIconWithName(FontAwesome.AngleDown, textColor: UIColor.grayColor(), size: iconSize), title: NSLocalizedString("Thôi", comment: ""), selectedAction: { (Void) -> Void in
                //
            }))
            
            let bottomMenu = BottomMenuView(items: items, didSelectedHandler: nil)
            let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
            bottomMenu.showInViewController((appDelegate.window?.rootViewController)!)
        }

        return videoListVC as UIViewController
    }

    func loadVideoData(videoListVC: VideosListTableViewController){
        let musicList = CSN.uiRealm.objects(Musics).filter("name = 'Favorites'")[0].musics.filter("musicWidth != '0'").sorted("createdAtDate", ascending: false)
        videoListVC.videosList = Array(musicList)
        videoListVC.tableView.reloadData()
    }
    
    func deleteData(music: Music) {
        let musicsList = CSN.uiRealm.objects(Musics).filter("name = 'Favorites'")[0].musics
        for i in 0...musicsList.count - 1 {
            if musicsList[i].musicId == music.musicId {
                dispatch_async(dispatch_get_main_queue()) {
                    autoreleasepool {
                        try! CSN.uiRealm.write({ () -> Void in
                            musicsList.removeAtIndex(i)
                            NSNotificationCenter.defaultCenter().postNotificationName(CSN.Notification.deleteDone, object: nil)
                        })
                    }
                }
            }
        }
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation

}
