//
//  CSN.swift
//  ChiaSeNhac
//
//  Created by Vũ Trung Thành on 12/13/15.
//  Copyright © 2015 V2T Multimedia. All rights reserved.
//

import UIKit
import RealmSwift
import SwiftyJSON
import Alamofire
import SSSnackbar
import FontAwesome_swift

public class CSN: NSObject {
    static let API = CSNAPI()
    static let musicPlayer = AudioPlayer()
    static let videoPlayer = VideoPlayer()
    static let uiRealm = try! Realm()
    
    public enum MusicQuality: String {
        case Lossless = "lossless"
        case M4A = "m4a"
        case k320 = "320k"
        case k128 = "128k"
        case k32 = "32k"
    }
    
    public enum VideoQuality: String {
        case k1080p = "1080p"
        case k720p = "720p"
        case k480p = "480p"
        case k320p = "320p"
        case k240p = "240p"
    }
    
    public enum SortBy: String {
        case musicTitle = "musicTitle"
        case createdAtDate = "createdAtDate"

    }
    
    public class Notification {
        // Notification
        static let showExtraOptionMusicItem = "showExtraOptionMusicItemNotification"
        static let showExtraOptionVideoItem = "showExtraOptionVideoItemNotification"
        static let downloadDone = "downloadDoneNotification"
        static let favoriteDone = "favoriteDoneNotification"
        static let saveHistoryDone = "saveHistoryDoneNotification"
        static let updateTrackList = "updateTrackListNotification"
        static let deleteDone = "deleteDoneNotification"
        static let selectSortDone = "selectSortDoneNotification"
        static let getAppConfigDone = "getAppConfigDoneNotification"
        static let showDownloadMusic = "showDownloadMusicNotification"
        static let showDownloadVideo = "showDownloadVideoNotification"

        // Music player notification
        static let startPlayingSong = "startPlayingSongNotification"
        static let musicPlayerStateChanged = "musicPlayerStateChangedNotification"
        
        // Video player notification
        static let startPlayingVideo = "startPlayingVideoNotification"
        static let videoPlayerStateChanged = "videoPlayerStateChangedNotification"
        static let videoPlayerEnterFullScreen = "videoPlayerEnterFullScreenNotification"
        static let videoPlayerExitFullScreen = "videoPlayerExitFullScreenNotification"
        
        static let searchButtonDidTapped = "searchButtonDidTappedNotification"
    }
    
    class Setting {
        static let playlistLimitCount = 13
        static var isCreatePlaylist = false
        
//        static var sortBy = "createdAtDate"

        static var sortBy: SortBy {
            get {
                let defaults = NSUserDefaults.standardUserDefaults()
                if let sortBy = defaults.stringForKey("sortDownloadListSetting")
                {
                    return SortBy(rawValue: sortBy)!
                } else {
                    return SortBy.createdAtDate
                }
            }
            
            set {
                let defaults = NSUserDefaults.standardUserDefaults()
                defaults.setObject(newValue.rawValue, forKey: "sortDownloadListSetting")
            }
        }
        
        static var videoQuality: VideoQuality {
            get {
                let defaults = NSUserDefaults.standardUserDefaults()
                if let quality = defaults.stringForKey("videoQualitySetting")
                {
                    return VideoQuality(rawValue: quality)!
                } else {
                    return VideoQuality.k480p
                }
            }
            
            set {
                let defaults = NSUserDefaults.standardUserDefaults()
                defaults.setObject(newValue.rawValue, forKey: "videoQualitySetting")
            }
        }
        static var musicQuality: MusicQuality {
            get {
                let defaults = NSUserDefaults.standardUserDefaults()
                if let quality = defaults.stringForKey("musicQualitySetting") {
                    return MusicQuality(rawValue: quality)!
                } else {
                    return MusicQuality.k320
                }
            }
            
            set {
                let defaults = NSUserDefaults.standardUserDefaults()
                defaults.setObject(newValue.rawValue, forKey: "musicQualitySetting")
            }
        }
        static var isPauseVideoInBackground: Bool {
            get {
                let defaults = NSUserDefaults.standardUserDefaults()
                return defaults.boolForKey("videoPlayInBackgroundSetting")
            }
            
            set {
                let defaults = NSUserDefaults.standardUserDefaults()
                defaults.setObject(newValue, forKey: "videoPlayInBackgroundSetting")
            }
        }
        static var shakeToNextSong: Bool {
            get {
            let defaults = NSUserDefaults.standardUserDefaults()
            return defaults.boolForKey("shakeToNextSongSetting")
            }
            
            set {
                let defaults = NSUserDefaults.standardUserDefaults()
                defaults.setObject(newValue, forKey: "shakeToNextSongSetting")
            }
        }
        
        static var globalJSON: JSON?
        static var shareVideoLink: String {
            if let json = globalJSON {
                return json["share_csn_link"].stringValue
            }
            return "https://csn.apps.v2t.mobi/?link="
        }
        
        static var isShowSearchFunction: Bool {
            if let json = globalJSON {
                return json["show_search_function"].boolValue
            }
            return false
        }
        
        static var isShowDownloadFunction: Bool {
                if let json = globalJSON {
                    return json["show_download_function"].boolValue
                }
                return false
        }
        
        static var isShowThirdPartyContent: Bool {
            if let json = globalJSON {
                return json["show_third_party_content"].boolValue
            }
            return false
        }
        
    }
    
    class func formatTimeFromSeconds(totalSeconds: Double) -> String {
        let seconds: Int = Int(totalSeconds % 60)
        let minutes: Int = Int((totalSeconds / 60) % 60)
        return (minutes < 10 ? "0" + String(minutes) : String(minutes)) + ": " + (seconds < 10 ? "0" + String(seconds) : String(seconds))
    }
    
    class func playMusic(music: Music) {
        var music = music
        if  music.path != "" {
            CSN.musicPlayer.playItem(music)
        } else {
            CSN.API.getDetailsMusic(music: music, success: { (result) -> Void in
                if result["music_info"] != nil {
                    music = Music.fromDictionary(result["music_info"].dictionaryObject!)
                    CSN.musicPlayer.playItem(music)
                }
                
                }) { (error) -> Void in
                    //
            }
        }
    }
    
    
    class func saveFavorite(music: Music) {
        let musicList = CSN.uiRealm.objects(Musics).filter("name = 'Favorites'")[0].musics
        for item: Music in musicList {
            if item.musicId == music.musicId {
                return
            }
        }
        do {
            try CSN.uiRealm.write({ () -> Void in
                musicList.append(music)
                NSNotificationCenter.defaultCenter().postNotificationName(CSN.Notification.favoriteDone, object: music)
            })
            
        } catch let error {
            debugPrint(error)
        }
    }

    class func showDownloadVideoQualityOptions(video: Music, viewController: UIViewController) {
        var music = video
        var items: [BottomMenuViewItem] = []
        CSN.API.getDetailsMusic(music: music, success: { (result) -> Void in
            if result["music_info"] != nil {
                music = Music.fromDictionary(result["music_info"].dictionaryObject!)
                if let losslessUrl = music.fileLosslessUrl {
                    if losslessUrl != "" {
                        items.append(BottomMenuViewItem(title: "1080p", selectedAction: { (Void) -> Void in
                            self.downloadMusic(music, urlString: losslessUrl)
                        }))
                    }
                }
                if let m4aUrl = music.fileM4aUrl {
                    if m4aUrl != "" {
                        items.append(BottomMenuViewItem(title: "720p", selectedAction: { (Void) -> Void in
                            self.downloadMusic(music, urlString: m4aUrl)
                        }))
                    }
                }
                
                if let k320 = music.file320Url {
                    if k320 != "" {
                        items.append(BottomMenuViewItem(title: "480p", selectedAction: { (Void) -> Void in
                            self.downloadMusic(music, urlString: k320)
                        }))
                    }
                }
                
                if let k128 = music.fileUrl {
                    if k128 != "" {
                        items.append(BottomMenuViewItem(title: "320p", selectedAction: { (Void) -> Void in
                            self.downloadMusic(music, urlString: k128)
                        }))
                    }
                }
                
                if let k32 = music.file32Url {
                    if k32 != "" {
                        items.append(BottomMenuViewItem(title: "240p", selectedAction: { (Void) -> Void in
                            self.downloadMusic(music, urlString: k32)
                        }))
                    }
                }
                let bottomMenu = BottomMenuView(items: items, didSelectedHandler: nil)
                bottomMenu.showInViewController(viewController)
            }
            
            }) { (error) -> Void in
                //
        }
    }

    class func showDownloadMusicQualityOptions(music: Music, viewController: UIViewController) {
        var music = music
        var items: [BottomMenuViewItem] = []
        CSN.API.getDetailsMusic(music: music, success: { (result) -> Void in
            if result["music_info"] != nil {
                music = Music.fromDictionary(result["music_info"].dictionaryObject!)
                if let losslessUrl = music.fileLosslessUrl {
                    if losslessUrl != "" {
                        items.append(BottomMenuViewItem(title: "Lossless", selectedAction: { (Void) -> Void in
                            self.downloadMusic(music, urlString: losslessUrl)
                        }))
                    }
                }
                if let m4aUrl = music.fileM4aUrl {
                    if m4aUrl != "" {
                        items.append(BottomMenuViewItem(title: "M4A", selectedAction: { (Void) -> Void in
                            self.downloadMusic(music, urlString: m4aUrl)
                        }))
                    }
                }
                
                if let k320 = music.file320Url {
                    if k320 != "" {
                        items.append(BottomMenuViewItem(title: "320K", selectedAction: { (Void) -> Void in
                            self.downloadMusic(music, urlString: k320)
                        }))
                    }
                }
                
                if let k128 = music.fileUrl {
                    if k128 != "" {
                        items.append(BottomMenuViewItem(title: "128K", selectedAction: { (Void) -> Void in
                            self.downloadMusic(music, urlString: k128)
                        }))
                    }
                }
                
                if let k32 = music.file32Url {
                    if k32 != "" {
                        items.append(BottomMenuViewItem(title: "32K", selectedAction: { (Void) -> Void in
                            self.downloadMusic(music, urlString: k32)
                        }))
                    }
                }
                let bottomMenu = BottomMenuView(items: items, didSelectedHandler: nil)
                bottomMenu.showInViewController(viewController)
            }
            
            }) { (error) -> Void in
                //
        }
    }
    
    class func downloadMusic(music: Music, urlString: String) {
        
        if sharedV2TAds.shouldShowInterstitialOnDownload {
            let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
            let rootViewController = appDelegate.window?.rootViewController
            sharedV2TAds.showInterstitialAdsFromRootViewController(rootViewController!)
        }
        
        let snackBar = SSSnackbar.init(message: NSLocalizedString("Bắt đầu tải " + music.musicTitle, comment: ""), actionText: NSLocalizedString("OK", comment: ""), duration: 2.5, actionBlock: nil, dismissalBlock: nil)
        snackBar.show()
        
        let fileManager = NSFileManager.defaultManager()
        let directoryURL = fileManager.URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask)[0]
        var typePath = "Musics"
        if let musicWidth = music.musicWidth {
            if Int(musicWidth) != 0 {
                typePath = "Videos"
            }
        }
        let dataPath = directoryURL.URLByAppendingPathComponent(typePath)
        var fileName = ""
        
        if !fileManager.fileExistsAtPath(dataPath.path!) {
            do {
                try fileManager.createDirectoryAtPath(dataPath.path!, withIntermediateDirectories: false, attributes: nil)
            } catch let error as NSError {
                debugPrint(error.localizedDescription)
            }
        }
        
        Alamofire.download(.GET, urlString, destination: { temporaryURL, response in
            if let name = response.suggestedFilename {
                fileName = name
                if fileManager.fileExistsAtPath(dataPath.URLByAppendingPathComponent(fileName).path!) {
                    let tempName: NSString = fileName
                    fileName = tempName.stringByDeletingPathExtension + "_copy." + tempName.pathExtension
                }
            }
            return dataPath.URLByAppendingPathComponent(fileName)
        })
            .progress { bytesRead, totalBytesRead, totalBytesExpectedToRead in
                debugPrint(totalBytesRead)
                // This closure is NOT called on the main queue for performance
                // reasons. To update your ui, dispatch to the main queue.
                dispatch_async(dispatch_get_main_queue()) {
                    debugPrint("Total bytes read on main queue: \(totalBytesRead)")
                }
            }
            .response { _, response, _, error in
                if let error = error {
                    debugPrint("Failed with error: \(error)")
                } else {
                    debugPrint("Downloaded file successfully")
                    debugPrint(dataPath.URLByAppendingPathComponent(fileName))
                    
                    music.path = typePath + "/" + fileName
                    music.createdAtDate = NSDate()
                    let musicsList = CSN.uiRealm.objects(Musics).filter("name = 'Downloads'")[0].musics
                    
                    do {
                        try CSN.uiRealm.write({ () -> Void in
                            musicsList.append(music)
                            music.downloadStatus = 1
                            NSNotificationCenter.defaultCenter().postNotificationName(CSN.Notification.downloadDone, object: music)
                            
                        })
                        
                    } catch let error {
                        debugPrint(error)
                        // Handling write error,
                        // e.g. Delete the Realm file, etc.
                    }
                }
        }
    }

    class func addDataToPlaylist(music: Music, viewController: UIViewController) {
        var items: [BottomMenuViewItem] = []
        let playlists = CSN.uiRealm.objects(Musics).filter("name != 'Favorites' && name != 'Histories' && name != 'Downloads'")
        for playlist in playlists {
            items.append(BottomMenuViewItem(icon:
                UIImage.fontAwesomeIconWithName(FontAwesome.List, textColor: UIColor.grayColor(), size: CGSize(width: 20, height: 20)), title: NSLocalizedString("\(playlist.name)", comment: ""), selectedAction: { (Void) -> Void in
                    try! CSN.uiRealm.write({
                        playlist.musics.append(music)
                    })
            }))
        }
        if playlists.count < (CSN.Setting.playlistLimitCount - 3) { // if playlist count equal limited playlist count, user can't create new playlist
            items.append(BottomMenuViewItem(icon:
                UIImage.fontAwesomeIconWithName(FontAwesome.Plus, textColor: UIColor.grayColor(), size: CGSize(width: 20, height: 20)), title: NSLocalizedString("Tạo playlsit mới...", comment: ""), selectedAction: { (Void) -> Void in
                    let popup = PopUpViewController(size: CGSize(width: 280, height: 120))
                    let storyboard = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle())
                    let createNewPlaylistVC = storyboard.instantiateViewControllerWithIdentifier("CreateNewPlaylist") as! CreateNewPlaylistViewController
                    //set Size popUp in iphone 4S:
                    if UIScreen.mainScreen().bounds.height == 480 {
                        popup.heightMultiplierViewContainer = 0.6
                    }
                    //                   
                    popup.addChildViewController(createNewPlaylistVC)
                    popup.viewContainer.addSubview(createNewPlaylistVC.view)
                    createNewPlaylistVC.view.translatesAutoresizingMaskIntoConstraints = false
                    popup.viewContainer.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:|[createNewPlaylistView]|", options: NSLayoutFormatOptions(), metrics: nil, views: ["createNewPlaylistView": createNewPlaylistVC.view]))
                    popup.viewContainer.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|[createNewPlaylistView]|", options: NSLayoutFormatOptions(), metrics: nil, views: ["createNewPlaylistView": createNewPlaylistVC.view]))
                    createNewPlaylistVC.cancelButtonTappedHandler = {
                        popup.hide()
                    }
                    createNewPlaylistVC.createdPlaylistHandler = { playlist in
                        playlist.musics.append(music)
                        popup.hide()
                    }
                    popup.showInViewController(viewController)
            }))
        }
        items.append(BottomMenuViewItem(icon:
            UIImage.fontAwesomeIconWithName(FontAwesome.AngleDown, textColor: UIColor.grayColor(), size: CGSize(width: 20, height: 20)), title: NSLocalizedString("Thôi", comment: ""), selectedAction: { (Void) -> Void in
                
        }))
        let bottomMenu = BottomMenuView(items: items, didSelectedHandler: nil)
        bottomMenu.showInViewController(viewController)
    }
    
}

enum UIUserInterfaceIdiom : Int {
    case Unspecified
    
    case Phone // iPhone and iPod touch style UI
    case Pad // iPad style UI
}
