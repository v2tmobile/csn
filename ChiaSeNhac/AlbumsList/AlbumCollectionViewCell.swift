//
//  AlbumCollectionViewCell.swift
//  ChiaSeNhac
//
//  Created by Vũ Trung Thành on 12/4/15.
//  Copyright © 2015 V2T Multimedia. All rights reserved.
//

import UIKit
import Haneke
import SwiftyJSON

class AlbumCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var coverImage: UIImageView!

    @IBOutlet weak var albumLabel: UILabel!
    
    @IBOutlet weak var yearLabel: UILabel!
    
    @IBOutlet weak var artistLabel: UILabel!
    
    var album: Album? {
        didSet {
            if let album = album {
                coverImage.hnk_setImageFromURL(NSURL(string: album.coverImg)!)
                albumLabel.text = album.musicAlbum
                artistLabel.text = album.musicArtist
                yearLabel.text = NSLocalizedString("Năm ", comment: "") + album.musicYear
            }
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        let radius: CGFloat = 1
        layer.cornerRadius = radius
        let shadowPath = UIBezierPath(roundedRect: bounds, cornerRadius: radius)
        
        layer.masksToBounds = false
        layer.shadowColor = UIColor.blackColor().CGColor
        layer.shadowOffset = CGSize(width: 0, height: 1);
        layer.shadowOpacity = 0.5
        layer.shadowPath = shadowPath.CGPath
    }
    
    
    @IBAction func playButtonTapped(sender: AnyObject) {
        CSN.API.getDetailsMusic(album: album!, success: { (result) -> Void in
            if result["music_info"] != nil {
                let music = Music.fromDictionary(result["music_info"].dictionaryObject!)
                var trackList: [Music] = []
                if result["track_list"] != nil {
                    if let musicList = result["track_list"].array {
                        for music: SwiftyJSON.JSON in musicList{
                            trackList.append(Music.fromDictionary(music.dictionaryObject!))
                        }
                    }
                }
                CSN.musicPlayer.playItem(music)
                CSN.musicPlayer.addItemsToQueue(trackList)
            }
            }) { (error) -> Void in
                //
        }
    }
}
