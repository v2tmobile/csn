//
//  AlbumsSectionHeaderView.swift
//  ChiaSeNhac
//
//  Created by Vũ Trung Thành on 12/4/15.
//  Copyright © 2015 V2T Multimedia. All rights reserved.
//

import UIKit

class AlbumsSectionHeaderView: UICollectionReusableView {
        
    @IBOutlet weak var titleLabel: UILabel!
}
