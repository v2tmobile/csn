//
//  AlbumsCollectionViewController.swift
//  ChiaSeNhac
//
//  Created by Vũ Trung Thành on 12/4/15.
//  Copyright © 2015 V2T Multimedia. All rights reserved.
//

import UIKit
import SwiftyJSON
import CarbonKit
import MBProgressHUD

class AlbumsCollectionViewController: V2TCollectionViewController {
    
    private let reuseIdentifier = "Cell"
    private let reuseHeaderViewIdentifier = "HeaderView"
    private let reuseFooterViewIdentifier = "FooterView"
    
    var albumsData: JSON?
    var albumsList: [Album] = []
    
    var catUrlString = ""
    var listType = "home"
    
    var swipeRefreshControl: CarbonSwipeRefresh!

    var isSearchVC = false
    var currentPage = 1
    
    var loadMoreHandler: (Void -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        swipeRefreshControl = CarbonSwipeRefresh.init(scrollView: self.collectionView)
        swipeRefreshControl.colors = [UIColor(rgba: "#d32f2f")]
        swipeRefreshControl.addTarget(self, action: "refreshList:", forControlEvents: UIControlEvents.ValueChanged)
        self.view.addSubview(swipeRefreshControl)
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Register cell classes
//        self.collectionView!.registerClass(AlbumCollectionViewCell.self, forCellWithReuseIdentifier: reuseIdentifier)

        // Do any additional setup after loading the view.
        if listType == "home" {
            MBProgressHUD.showHUDAddedTo(self.view, animated: true)
            if CSN.Setting.isShowThirdPartyContent {
                swipeRefreshControl.startRefreshing()
                CSN.API.getAllMusic(success: { (result) -> () in
                    if result["album"] != nil {
                        self.albumsData = result["album"]
                        self.collectionView?.reloadData()
                        MBProgressHUD.hideHUDForView(self.view, animated: true)
                    }
                    self.swipeRefreshControl.endRefreshing()
                    }) { (error) -> Void in
                        MBProgressHUD.hideHUDForView(self.view, animated: true)
                }
            } else {
                swipeRefreshControl.startRefreshing()
                CSN.API.getCategory(name: "vietnam", page: 1, loadCache: true, success: { (result) -> Void in
                    if result["album"] != nil {
                        self.albumsData = result["album"]
                        self.collectionView?.reloadData()
                        MBProgressHUD.hideHUDForView(self.view, animated: true)
                    }
                    self.swipeRefreshControl.endRefreshing()
                    }, failure: { (error) -> Void in
                        MBProgressHUD.hideHUDForView(self.view, animated: true)
                })
            }
        }
        if listType == "search" {
            swipeRefreshControl.startRefreshing()
            CSN.API.search(query: catUrlString, category: CategoryType.Album, page: 1, loadCache: true, success: {
                (result) -> Void in
                self.albumsList = []
                if result["album_list"] != nil {
                    if let albumArray = result["album_list"].array {
                        for item: JSON in albumArray {
                            self.albumsList.append(Album.fromDictionary(item.dictionaryObject!))
                        }
                        self.collectionView?.reloadData()
                    }
                }
                self.swipeRefreshControl.endRefreshing()
                }, failure: { (error) -> Void in
                    
            })

        } else {
            swipeRefreshControl.startRefreshing()
            CSN.API.getCategory(name: catUrlString, page: 1, loadCache: true, success: { (result) -> Void in
                if result["album"] != nil {
                    self.albumsData = result["album"]
                    self.collectionView?.reloadData()
                }
                self.swipeRefreshControl.endRefreshing()
                }, failure: { (error) -> Void in
                    
            })
        }
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        let tracker = GAI.sharedInstance().defaultTracker
        tracker.set(kGAIScreenName, value: "Album list: " + catUrlString)
        
        let builder = GAIDictionaryBuilder.createScreenView()
        tracker.send(builder.build() as [NSObject : AnyObject])
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
    }
    */

    // MARK: UICollectionViewDataSource

    override func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        if isSearchVC == false {
            return 2
        } else {
            return 1
        }
    }

    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if isSearchVC == true {
            return self.albumsList.count
        } else {
            if (self.albumsData != nil) {
                switch section {
                case 0:
                    if let albumNewCount = self.albumsData!["album_new"].array?.count {
                        return albumNewCount
                    } else {
                        return 0
                    }
                case 1:
                    if let albumNewCount = self.albumsData!["album_old"].array?.count {
                        return albumNewCount
                    } else {
                        return 0
                    }
                default: return 0
                }
            } else {
                return 0 
            }

        }
    }

    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell: AlbumCollectionViewCell = collectionView.dequeueReusableCellWithReuseIdentifier(reuseIdentifier, forIndexPath: indexPath) as! AlbumCollectionViewCell
        // Configure the cell
        if isSearchVC == false {
            if indexPath.section == 0 {
                let albumNew: Array<JSON> = self.albumsData!["album_new"].arrayValue
                cell.album = Album.fromDictionary(albumNew[indexPath.row].dictionaryObject!)
            } else {
                let albumOld: Array<JSON> = self.albumsData!["album_old"].arrayValue
                cell.album = Album.fromDictionary(albumOld[indexPath.row].dictionaryObject!)
            }
        }  else {
            cell.album = self.albumsList[indexPath.row]
        }
        
        return cell
    }
    
    override func collectionView(collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, atIndexPath indexPath: NSIndexPath) -> UICollectionReusableView {
        if isSearchVC == false {
            var reuseableView: UICollectionReusableView!
            if kind == UICollectionElementKindSectionHeader {
                let headerView: AlbumsSectionHeaderView = collectionView.dequeueReusableSupplementaryViewOfKind(UICollectionElementKindSectionHeader, withReuseIdentifier: reuseHeaderViewIdentifier, forIndexPath: indexPath) as! AlbumsSectionHeaderView
                if indexPath.section == 0 {
                    headerView.titleLabel.text = NSLocalizedString("Album mới", comment: "")
                } else {
                    headerView.titleLabel.text = NSLocalizedString("Album cũ", comment: "")
                }
                reuseableView = headerView
            }
            
            if kind == UICollectionElementKindSectionFooter {
                let footerView: AlbumsSectionFooterView = collectionView.dequeueReusableSupplementaryViewOfKind(UICollectionElementKindSectionFooter, withReuseIdentifier: reuseFooterViewIdentifier, forIndexPath: indexPath) as! AlbumsSectionFooterView
                
                // Ads settig
                sharedV2TAds.showBannerAdsInView(sharedV2TAds.bannerReuseableCellAdsNetwork, view: footerView, viewController: self, bannerViewDidLoad: { (banner) -> Void in
                    //
                })
                reuseableView = footerView
            }
            return reuseableView
        } else {
            var reuseableView: UICollectionReusableView!
            if kind == UICollectionElementKindSectionHeader {
                let headerView: AlbumsSectionHeaderView = collectionView.dequeueReusableSupplementaryViewOfKind(UICollectionElementKindSectionHeader, withReuseIdentifier: reuseHeaderViewIdentifier, forIndexPath: indexPath) as! AlbumsSectionHeaderView
                
                // Ads settig
                sharedV2TAds.showBannerAdsInView(sharedV2TAds.bannerReuseableCellAdsNetwork, view: headerView, viewController: self, bannerViewDidLoad: { (banner) -> Void in
                    //
                })
                reuseableView = headerView
            }
            if kind == UICollectionElementKindSectionFooter {
                let footerView: AlbumsSectionFooterView = collectionView.dequeueReusableSupplementaryViewOfKind(UICollectionElementKindSectionFooter, withReuseIdentifier: reuseFooterViewIdentifier, forIndexPath: indexPath) as! AlbumsSectionFooterView
                
                reuseableView = footerView
            }

            return reuseableView
        }
        
    }
    
    func collectionView(collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        referenceSizeForHeaderInSection section: Int) -> CGSize {
            if isSearchVC == true {
                return CGSize(width: collectionView.frame.height, height: 50)
            }
            return CGSize(width: collectionView.frame.height, height: 32)
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        switch UIDevice.currentDevice().userInterfaceIdiom {
        case .Phone:
            // It's an iPhone
            let width = self.view.bounds.width/2 - 18
            return CGSize(width: width, height: width + 44);
        case .Pad:
            // It's an iPad
            let width = self.view.bounds.width/3 - 18
            return CGSize(width: width, height: width + 44);
        default:
            let width = self.view.bounds.width/2 - 18
            return CGSize(width: width, height: width + 44);
            // Uh, oh! What could it be?
        }
    }
    

    // MARK: UICollectionViewDelegate

    /*
    // Uncomment this method to specify if the specified item should be highlighted during tracking
    override func collectionView(collectionView: UICollectionView, shouldHighlightItemAtIndexPath indexPath: NSIndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment this method to specify if the specified item should be selected
    override func collectionView(collectionView: UICollectionView, shouldSelectItemAtIndexPath indexPath: NSIndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
    override func collectionView(collectionView: UICollectionView, shouldShowMenuForItemAtIndexPath indexPath: NSIndexPath) -> Bool {
        return false
    }

    override func collectionView(collectionView: UICollectionView, canPerformAction action: Selector, forItemAtIndexPath indexPath: NSIndexPath, withSender sender: AnyObject?) -> Bool {
        return false
    }

    override func collectionView(collectionView: UICollectionView, performAction action: Selector, forItemAtIndexPath indexPath: NSIndexPath, withSender sender: AnyObject?) {
    
    }
    */
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "showAlbumDetails" {
            let albumCell: AlbumCollectionViewCell = sender as! AlbumCollectionViewCell
            let albumDetailsVC: DetailsAlbumViewController = segue.destinationViewController as! DetailsAlbumViewController
            albumDetailsVC.albumData = albumCell.album
        }
    }
    
    // MARK: loadMore
    
    override func scrollViewDidEndDragging(scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        let currentOffset = scrollView.contentOffset.y
        let maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height
        if (maximumOffset - currentOffset <= 100) {
            loadMoreHandler?()
        }
    }

    // MARK: - SwipeRefreshControl delegate
    
    func refreshList(swipeRefreshControl: CarbonSwipeRefresh) {
        if listType == "home" {
            if CSN.Setting.isShowThirdPartyContent {
                CSN.API.getAllMusic(loadCache: false, success: { (result) -> () in
                    if result["album"] != nil {
                        self.albumsData = result["album"]
                        self.collectionView?.reloadData()
                        swipeRefreshControl.endRefreshing()
                        sharedV2TAds.showInterstitialAdsFromRootViewController(self)
                    }
                    }) { (error) -> Void in
                        swipeRefreshControl.endRefreshing()
                }
            } else {
                CSN.API.getCategory(name: "vietnam", page: 1, loadCache: false, success: { (result) -> Void in
                    if result["album"] != nil {
                        self.albumsData = result["album"]
                        self.collectionView?.reloadData()
                        swipeRefreshControl.endRefreshing()
                        sharedV2TAds.showInterstitialAdsFromRootViewController(self)
                    }
                    }, failure: { (error) -> Void in
                        swipeRefreshControl.endRefreshing()
                })
            }
        }
        if listType == "search" {
            CSN.API.search(query: catUrlString, category: CategoryType.Album, page: 1, loadCache: true, success: {
                (result) -> Void in
                self.albumsList = []
                if result["album_list"] != nil {
                    if let albumArray = result["album_list"].array {
                        for item: JSON in albumArray {
                            self.albumsList.append(Album.fromDictionary(item.dictionaryObject!))
                        }
                        self.collectionView?.reloadData()
                        swipeRefreshControl.endRefreshing()
                        sharedV2TAds.showInterstitialAdsFromRootViewController(self)
                    }                }
                }, failure: { (error) -> Void in
                    swipeRefreshControl.endRefreshing()
            })
            
        } else {
            CSN.API.getCategory(name: catUrlString, page: 1, loadCache: false, success: { (result) -> Void in
                if result["album"] != nil {
                    self.albumsData = result["album"]
                    self.collectionView?.reloadData()
                    swipeRefreshControl.endRefreshing()
                    sharedV2TAds.showInterstitialAdsFromRootViewController(self)
                }
                }, failure: { (error) -> Void in
                    swipeRefreshControl.endRefreshing()
            })
        }
    }

}
