//
//  V2TAds.swift
//  ChiaSeNhac
//
//  Created by Vũ Trung Thành on 1/8/16.
//  Copyright © 2016 V2T Multimedia. All rights reserved.
//

import UIKit
import GoogleMobileAds

let sharedV2TAds = V2TAds()

public protocol V2TAdsDeletage {
    func v2tAdsDidGetAdsInfo(v2tAds: V2TAds)
}

public class V2TAds: NSObject, GADInterstitialDelegate,
                        FBInterstitialAdDelegate,
                        FBAdViewDelegate,
                        STABannerDelegateProtocol,
                        GADBannerViewDelegate,
                        AdColonyDelegate,
                        AdColonyAdDelegate {
    
    private var admobBannerId = ""
    private var admobInterstitialId = ""
    
    private var facebookBannerId = ""
    private var facebookInterstitialId = ""
    private var facebookNativeId = ""
    
    private var startAppAppId = ""
    private var startAppDevId = ""
    
    private var adcolonyAppId = ""
    private var adcolonyZoneId = ""
    
    private var timeInterstitialAdsCanShowAgain = 30.0
    
    /// The interstitial ad.
    private var interstitialAdmob: GADInterstitial?
    
    private var interstitialStartApp: STAStartAppAd?
    
    private var interstitialFacebook: FBInterstitialAd?
    private var timeIntervalDidShowinterstitialFBAds = NSTimer()
    private var canShowInterstitialFBAds = true
    
    // The video ad
    private var videoAdAttemptFinished: (Void ->Void)?
    
    // The banner ad
    private var bannerViewDidLoad: (UIView ->Void)?
    private var startAppBanner: STABannerView?
    
    private let v2tAPI = V2TAPI()
    
    public private(set) var defaultAdsNetwork: AdsNetwork = .Admob
    public private(set) var interstitialAdsNetwork: AdsNetwork = .StartApp
    public private(set) var bannerAdsNetwork: AdsNetwork = .Admob
    public private(set) var nativeAdsNetwork: AdsNetwork = .Facebook
    public private(set) var bannerVideoPlayerAdsNetwork: AdsNetwork = .Admob
    public private(set) var bannerReuseableCellAdsNetwork: AdsNetwork = .Admob
    public private(set) var videoAdsNetwork: AdsNetwork = .AdColony
    
    public private(set) var shouldShowAds = false
    public private(set) var shouldShowSplash = false
    public private(set) var shouldShowBanner = false
    public private(set) var shouldShowInterstitial = false
    public private(set) var shouldSHowInterstitalOnPlay = false
    public private(set) var shouldShowInterstitialOnPause = false
    public private(set) var shouldShowInterstitialOnDownload = false
    public private(set) var shouldShowBannerInVideo = false
    public private(set) var shouldShowVideoAds = false
    public private(set) var shouldShowVideoAdsOnFullScreen = false
    public private(set) var shouldShowVideoAdsOnPause = false
    public private(set) var shouldShowVideoAdsOnStop = false
    public private(set) var shouldShowHomeBanner = false
    
    public private(set) var delayShowBannerInVideoPlayer = 30.0
    public private(set) var delayHideBannerInVideoPlayer = 5.0
    
    public var delegate: V2TAdsDeletage?
    
    override init() {
        super.init()
        v2tAPI.initialize(organization: "v2t", application: "chiasenhac")
    }
    
    public func getAdsInfo(completionHandler: (Void -> Void)?) {
        v2tAPI.request(V2TAPI.Router.getEntity(identifier: "ios_ads_v2.0", collection: "configs")) { (json) -> Void in
            if let entity = json?["entities"][0] {
                if let adsNetwork = AdsNetwork(rawValue: entity["active_ads"]["default"].stringValue) {
                    self.defaultAdsNetwork = adsNetwork
                }
                if let adsNetwork = AdsNetwork(rawValue: entity["active_ads"]["interstitial"].stringValue) {
                    self.interstitialAdsNetwork = adsNetwork
                }
                if let adsNetwork = AdsNetwork(rawValue: entity["active_ads"]["banner"].stringValue) {
                    self.bannerAdsNetwork = adsNetwork
                }
                if let adsNetwork = AdsNetwork(rawValue: entity["active_ads"]["native"].stringValue) {
                    self.nativeAdsNetwork = adsNetwork
                }
                if let adsNetwork = AdsNetwork(rawValue: entity["active_ads"]["banner_in_videoplayer"].stringValue) {
                    self.bannerVideoPlayerAdsNetwork = adsNetwork
                }
                if let adsNetwork = AdsNetwork(rawValue: entity["active_ads"]["banner_in_reuseable_cell"].stringValue) {
                    self.bannerReuseableCellAdsNetwork = adsNetwork
                }
                if let adsNetwork = AdsNetwork(rawValue: entity["active_ads"]["video"].stringValue) {
                    self.videoAdsNetwork = adsNetwork
                }
                
                self.shouldShowAds = entity["status"]["status"].boolValue
                self.shouldShowSplash = entity["status"]["show_splash_screen"].boolValue
                self.shouldShowBanner = entity["status"]["show_banner"].boolValue
                self.shouldShowInterstitial = entity["status"]["show_interstitial"].boolValue
                self.shouldShowInterstitialOnPause = entity["status"]["show_interstitial_on_pause"].boolValue
                self.shouldSHowInterstitalOnPlay = entity["status"]["show_interstitial_on_play"].boolValue
                self.shouldShowInterstitialOnDownload = entity["status"]["show_interstitial_on_download"].boolValue
                self.shouldShowBannerInVideo = entity["status"]["show_banner_in_video"].boolValue
                self.shouldShowVideoAds = entity["status"]["show_video"].boolValue
                self.shouldShowVideoAdsOnFullScreen = entity["status"]["show_video_ads_on_fullscreen"].boolValue
                self.shouldShowVideoAdsOnPause = entity["status"]["show_video_ads_on_pause"].boolValue
                self.shouldShowVideoAdsOnStop = entity["status"]["show_video_ads_on_stop"].boolValue
                
                self.shouldShowHomeBanner = entity["status"]["show_home_banner"].boolValue
                
                if let time = entity["time_interval"]["interstitial_can_show_again"].double {
                    self.timeInterstitialAdsCanShowAgain = time
                }
                
                if let time = entity["time_interval"]["show_banner_in_videoplayer"].double {
                    self.delayShowBannerInVideoPlayer = time
                }
                if let time = entity["time_interval"]["hide_banner_in_videoplayer"].double {
                    self.delayHideBannerInVideoPlayer = time
                }
                
                let ads = entity["ads"]
                
                self.admobBannerId = ads["admob"]["banner"].stringValue
                self.admobInterstitialId = ads["admob"]["interstitial"].stringValue
                
                self.facebookBannerId = ads["facebook"]["banner"].stringValue
                self.facebookInterstitialId = ads["facebook"]["interstitial"].stringValue
                self.facebookNativeId = ads["facebook"]["native"].stringValue
                
                self.startAppAppId = ads["startapp"]["app_id"].stringValue
                self.startAppDevId = ads["startapp"]["dev_id"].stringValue
                
                self.adcolonyAppId = ads["adcolony"]["app_id"].stringValue
                self.adcolonyZoneId = ads["adcolony"]["zone_id"].stringValue
                
                if self.delegate != nil {
                    self.delegate?.v2tAdsDidGetAdsInfo(self)
                }
                
                dispatch_async(dispatch_get_main_queue()) {
                    
                    //Initialize AdColony only once, on initial launch
                    AdColony.configureWithAppID(self.adcolonyAppId, zoneIDs: [self.adcolonyZoneId], delegate: nil, logging: true)
                    
                    // Admob
                    self.interstitialAdmob = GADInterstitial(adUnitID: self.admobInterstitialId)
                    self.interstitialAdmob!.delegate = self
                    
                    // Request test ads on devices you specify. Your test device ID is printed to the console when
                    // an ad request is made. GADInterstitial automatically returns test ads when running on a
                    // simulator.
                    let request = GADRequest()
                    #if DEBUG
                        request.testDevices = ["43acf86a624ab4a24f11decc51dec5c8", kGADSimulatorID]
                    #endif
                    self.interstitialAdmob!.loadRequest(request)
                    
                    // Facebook
                    self.interstitialFacebook = FBInterstitialAd(placementID: self.facebookInterstitialId)
                    self.interstitialFacebook?.delegate = self
                    self.interstitialFacebook?.loadAd()
                    
                    // init startapp ads
                    let sdk: STAStartAppSDK = STAStartAppSDK.sharedInstance()
                    sdk.appID = self.startAppAppId
                    sdk.devID = self.startAppDevId
                    
                    self.interstitialStartApp = STAStartAppAd()
                    self.interstitialStartApp!.loadAd()
                    
                    completionHandler?()
                }
            }
        }
    }
    
    public func showInterstitialAdsFromRootViewController(viewController: UIViewController) {
        if !shouldShowAds {
            return
        }
        dispatch_async(dispatch_get_main_queue()) {
            if (self.shouldShowInterstitial) {
                switch self.interstitialAdsNetwork {
                case .Admob:
                    if (self.interstitialAdmob!.isReady) {
                        self.interstitialAdmob!.presentFromRootViewController(viewController)
                    }
                    break
                case .Facebook:
                    if self.canShowInterstitialFBAds {
                        self.interstitialFacebook?.showAdFromRootViewController(viewController)
                        self.canShowInterstitialFBAds = false
                        self.timeIntervalDidShowinterstitialFBAds.invalidate()
                        self.timeIntervalDidShowinterstitialFBAds =
                            NSTimer.scheduledTimerWithTimeInterval(self.timeInterstitialAdsCanShowAgain,
                                target: self,
                                selector: "setCanshowFBInterstitialAds", userInfo: nil, repeats: false)
                    }
                    break
                case .StartApp:
                    self.interstitialStartApp?.showAd()
                    break
                case .AdColony:
                    break
                }
            }
        }
    }
    
    public func showSplashScreenAds() {
        if !shouldShowAds {
            return
        }
        if (self.shouldShowSplash) {
            STAStartAppSDK.sharedInstance().showSplashAd()
        }
    }
    
    public func showBannerAdsInView(view: UIView, viewController: UIViewController, bannerViewDidLoad: (UIView ->Void)?) {
        self.showBannerAdsInView(nil, view: view, viewController: viewController, bannerViewDidLoad: bannerViewDidLoad)
    }
    
    
    public func showBannerAdsInView(adsNetwork: AdsNetwork?, view: UIView, viewController: UIViewController, bannerViewDidLoad: (UIView ->Void)?) {
        if !shouldShowAds {
            return
        }
        dispatch_async(dispatch_get_main_queue()) {
        if self.shouldShowBanner {
            self.bannerViewDidLoad = bannerViewDidLoad
            var adsN: AdsNetwork = self.bannerAdsNetwork
            if adsNetwork != nil {
                adsN = adsNetwork!
            }
            switch adsN {
            case .Admob:
                let bannerView = GADBannerView(frame: CGRect(x: 0, y: 0, width: viewController.view.frame.width, height: 50))
                bannerView.adUnitID = self.admobBannerId
                bannerView.rootViewController = viewController
                let request = GADRequest()
                #if DEBUG
                request.testDevices = ["43acf86a624ab4a24f11decc51dec5c8", kGADSimulatorID]
                #endif
                bannerView.delegate = self
                bannerView.loadRequest(request)
                view.addSubview(bannerView)
                break
            case .Facebook:
                let adView = FBAdView(placementID: self.facebookBannerId, adSize: kFBAdSizeHeight50Banner, rootViewController: viewController)
                adView.delegate = self
                view.addSubview(adView)
                adView.loadAd()
                break
            case .StartApp:
                if (self.startAppBanner == nil) {
                    self.startAppBanner = STABannerView(size: STA_AutoAdSize, autoOrigin: STAAdOrigin_Top, withView: viewController.view, withDelegate: self)
                    self.startAppBanner?.setOrigin(view.frame.origin)
                    view.addSubview(self.startAppBanner!)
                }
                break
            case .AdColony:
                break
            }
        }
        }
    }
    
    public func showVideoAds(videoAdAttemptFinished: (Void -> Void)?) {
        if !shouldShowAds {
            return
        }
        if self.shouldShowVideoAds {
            switch self.videoAdsNetwork {
            case .AdColony:
                AdColony.playVideoAdForZone(self.adcolonyZoneId, withDelegate: self, withV4VCPrePopup: false, andV4VCPostPopup: false)
                self.videoAdAttemptFinished = videoAdAttemptFinished
                break
            default:
                break
            }
        }
    }
    
    // MARK: - GADInterstitialDelegate implementation
    
    public func interstitialDidFailToReceiveAdWithError (
        interstitial: GADInterstitial,
        error: GADRequestError) {
            print("interstitialDidFailToReceiveAdWithError: %@" + error.localizedDescription)
    }
    
    public func interstitialDidDismissScreen (interstitial: GADInterstitial) {
        print("interstitialDidDismissScreen")
        self.interstitialAdmob = GADInterstitial(adUnitID: self.admobInterstitialId)
        self.interstitialAdmob!.delegate = self
        let request = GADRequest()
        #if DEBUG
            request.testDevices = ["43acf86a624ab4a24f11decc51dec5c8", kGADSimulatorID]
        #endif
        self.interstitialAdmob!.loadRequest(GADRequest())
    }
    
    // MARKL - GADBannerViewDelegate {
    public func adViewDidReceiveAd(bannerView: GADBannerView!) {
        self.bannerViewDidLoad?(bannerView)
    }
    
    public func adViewDidDismissScreen(bannerView: GADBannerView!) {
        //
    }
    
    public func adViewWillDismissScreen(bannerView: GADBannerView!) {
        //
    }
    
    public func adViewWillLeaveApplication(bannerView: GADBannerView!) {
        //
    }
    
    public func adView(bannerView: GADBannerView!, didFailToReceiveAdWithError error: GADRequestError!) {
        //
    }
    
    // MARK: - FBIntersititalDelegate
    public func interstitialAdDidClick(interstitialAd: FBInterstitialAd) {
        //
    }
    
    public func interstitialAdDidLoad(interstitialAd: FBInterstitialAd) {
        //
    }
    
    public func interstitialAdWillClose(interstitialAd: FBInterstitialAd) {
        //
    }
    
    public func interstitialAdDidClose(interstitialAd: FBInterstitialAd) {
        self.interstitialFacebook = nil
        self.interstitialFacebook = FBInterstitialAd(placementID: self.facebookInterstitialId)
        self.interstitialFacebook?.delegate = self
        self.interstitialFacebook?.loadAd()
    }
    
    func setCanshowFBInterstitialAds() {
        self.canShowInterstitialFBAds = true
    }
    
    // MARK: - FBAdViewDelegate
    public func adView(adView: FBAdView, didFailWithError error: NSError) {
        //
    }
    
    public func adViewDidLoad(adView: FBAdView) {
        self.bannerViewDidLoad?(adView)
    }
    
    // MARK: - StartAppBannerDelegate
    
    public func didDisplayBannerAd(banner: STABannerView!) {
        self.bannerViewDidLoad?(banner)
    }
    
    public func didClickBannerAd(banner: STABannerView!) {
        //
    }
    
    public func didCloseBannerInAppStore(banner: STABannerView!) {
        //
    }
    
    
    //=============================================
    // MARK:- AdColony V4VC
    //=============================================
    
    /**
    Callback activated when a V4VC currency reward succeeds or fails
    This implementation is designed for client-side virtual currency without a server
    It uses NSUserDefaults for persistent client-side storage of the currency balance
    For applications with a server, contact the server to retrieve an updated currency balance
    On success, posts an NSNotification so the rest of the app can update the UI
    On failure, posts an NSNotification so the rest of the app can disable V4VC UI elements
    */
    public func onAdColonyV4VCReward(success: Bool, currencyName: String, currencyAmount amount: Int32, inZone zoneID: String)
    {
        NSLog("AdColony zone: %@ reward: %@ amount: %i", zoneID, success ? "YES" : "NO", amount)
        
        if success {
            
        }
    }
    
    //=============================================
    // MARK:- AdColony Ad Fill
    //=============================================
    
    /**
    Callback triggered when the state of ad readiness changes
    If the AdColony SDK tells us our zone either has, or doesn't have, ads, we
    need to tell our view controller to update its UI elements appropriately
    */
    public func onAdColonyAdAvailabilityChange(available: Bool, inZone zoneID: String)
    {
        if available {
            
        } else {
            
        }
    }
    
    public func onAdColonyAdAttemptFinished(shown: Bool, inZone zoneID: String) {
        self.videoAdAttemptFinished?()
    }
    
    public enum AdsNetwork: String{
        case Admob = "admob"
        case Facebook = "facebook"
        case StartApp = "startapp"
        case AdColony = "adcolony"
    }
    
    public enum Type {
        case Banner
        case Interstitial
        case Native
        case Video
        case Splash
    }
}
