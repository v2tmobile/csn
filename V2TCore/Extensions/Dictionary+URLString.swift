//
//  Dictionary+URLString.swift
//  V2TCore
//
//  Created by Vũ Trung Thành on 1/5/16.
//  Copyright © 2016 V2T Multimedia. All rights reserved.
//
import Foundation

extension Dictionary {
    
    func toURLString() -> String? {
        
        var urlString = ""
        
        for (paramNameObject, paramValueObject) in self {
            var paramNameEncoded: String?
            var paramValueEncoded: String?
            if let paramNameObject = paramNameObject as? String {
                paramNameEncoded = paramNameObject
            }
            if let paramValueObject = paramValueObject as? String {
                paramValueEncoded = paramValueObject
            } else if let paramValueObject = paramValueObject as? Int {
                paramValueEncoded = String(paramValueObject)
            } else if let paramValueObject = paramValueObject as? Bool {
                paramValueEncoded = String(paramValueObject)
            }
            if paramNameEncoded != nil && paramValueEncoded != nil  {
                let oneUrlPiece = paramNameEncoded! + "=" + paramValueEncoded!
                urlString = urlString + (urlString == "" ? "" : "&") + oneUrlPiece
            }
        }
        if urlString == "" {
            return nil
        }
        return urlString
    }
}