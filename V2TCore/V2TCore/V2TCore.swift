//
//  V2TCore.swift
//  ChiaSeNhac
//
//  Created by Vũ Trung Thành on 1/8/16.
//  Copyright © 2016 V2T Multimedia. All rights reserved.
//

import UIKit
import SwiftyJSON

public class V2TCore: NSObject {
    
    init(appName: String!) {
        super.init()
        sharedV2TAPI.initialize(organization: "v2t", application: appName)
    }
    
    public func registerDevice() {
        
        var deviceUuid = UIDevice.currentDevice().identifierForVendor!.UUIDString
        
        if let deviceUID = DeviceUID.valueForKeychainKey("UID", service: "deviceInfo") {
            deviceUuid = deviceUID
        } else if let deviceUID = DeviceUID.valueForUserDefaultsKey("deviceUID") {
            deviceUuid = deviceUID
        } else {
            DeviceUID.setValue(UIDevice.currentDevice().identifierForVendor!.UUIDString, forKeychainKey: "UID", inService: "deviceInfo")
            DeviceUID.setValue(UIDevice.currentDevice().identifierForVendor!.UUIDString, forUserDefaultsKey: "deviceUID")
        }
        
        let deviceUuid1 = deviceUuid.stringByReplacingOccurrencesOfString("-", withString: "")
        
        sharedV2TAPI.request(V2TAPI.Router.updateEntity(identifier: deviceUuid1, value: ["device_uuid": deviceUuid,
            "name": deviceUuid1,
            "os_version":   UIDevice().systemVersion,
            "model_name":   UIDevice().modelName],
            collection: "devices")) { (json) -> Void in
                debugPrint("Update device")
                debugPrint(json)
        }
    }
    
    public func getConfigs(completionHandler: (JSON -> Void)?) {
        sharedV2TAPI.request(V2TAPI.Router.getEntity(identifier: "ios_app_v2.0", collection: "configs")) { (json) -> Void in
            if let entity = json?["entities"][0] {
                completionHandler?(entity)
            }
        }
    }
}