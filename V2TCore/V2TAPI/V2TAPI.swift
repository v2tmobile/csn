//
//  V2T.swift
//  V2TCore
//
//  Created by Vũ Trung Thành on 1/4/16.
//  Copyright © 2016 V2T Multimedia. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

let sharedV2TAPI = V2TAPI()

public class V2TAPI: NSObject {
    
    // Read only properties
    public var isAuthenticated: Bool {
        return (self.accessToken != nil &&
        self.accessTokenExpires != nil &&
        self.accessTokenExpires?.compare(NSDate()) == NSComparisonResult.OrderedDescending)
    }
    
    // Allow private set only properties
    public private(set) var accessToken: String? {
        didSet {
            Router.accessToken = self.accessToken
        }
    }
    public private(set) var accessTokenExpires: NSDate?
    
    // Private properties
    private var manager: Alamofire.Manager
    
    override init() {
        // Create a shared URL cache
        let memoryCapacity = 500 * 1024 * 1024; // 500 MB
        let diskCapacity = 500 * 1024 * 1024; // 500 MB
        let cache = NSURLCache(memoryCapacity: memoryCapacity, diskCapacity: diskCapacity, diskPath: "shared_cache")
        
        // Create a custom configuration
        let configuration = NSURLSessionConfiguration.defaultSessionConfiguration()
        let defaultHeaders = Alamofire.Manager.sharedInstance.session.configuration.HTTPAdditionalHeaders
        configuration.HTTPAdditionalHeaders = defaultHeaders
        configuration.requestCachePolicy = .UseProtocolCachePolicy // this is the default
        configuration.URLCache = cache
        
        // Create your own manager instance that uses your custom configuration
        manager = Alamofire.Manager(configuration: configuration)
    }

    public func initialize(organization organization: String!, application: String!) {
        Router.organization = organization
        Router.application = application
        Router.applicationPath = "/" + organization + "/" + application
    }
    
    public func getAccessTokenForApplication(username username: String!, password: String!, completionHandler: (String? -> Void)?) {
        // Make your request with your custom manager that is caching your requests by default
        let paramaters = ["grant_type": "password",
        "username": username,
        "password": password]
        self.manager.request(.POST, Router.baseURL + Router.applicationPath + "/token", parameters: paramaters, encoding: .JSON)
            .responseJSON { (response) -> Void in
                if response.result.isSuccess {
                    debugPrint(response.result.value)
                    let json = JSON(response.result.value!)
                    self.accessToken = json["access_token"].string
                    self.accessTokenExpires = NSDate(timeIntervalSinceNow: NSTimeInterval(json["expires_in"].int!))
                    debugPrint("Login status: \(self.isAuthenticated)")
                }
                if response.result.isFailure {
                    debugPrint(response.result.error)
                }
                completionHandler?(self.accessToken)
        }
    }
    
    public func request(router: Router, completionHandler: (JSON? -> Void)?) {
        self.manager.request(router).responseJSON { (response) -> Void in
            debugPrint("Request: \(response.request?.allHTTPHeaderFields)")
            debugPrint(response)
            if response.result.isSuccess {
                completionHandler?(JSON(response.result.value!))
            }
            if response.result.isFailure {
                completionHandler?(JSON(response.result.error!))
            }
        }
    }
    
    public enum Router: URLRequestConvertible {
        static private let baseURL = "http://api.v2t.mobi"
//        static private let baseURL = "http://10.0.0.5:8080" // demo server
        static private var applicationPath = ""
        static private var accessToken: String?
        static private var organization: String?
        static private var application: String?
        
        // Collection
        case createEntity(entity: Dictionary<String, AnyObject>, collection: String!)
        case getEntity(identifier: String, collection: String!)
        case updateEntity(identifier: String!, value: Dictionary<String, AnyObject>, collection: String!)
        case deleteEntity(identifier: String!, collection: String!)
        
        case getEntities(collection: String!, query: Dictionary<String, AnyObject>?)
        case updateEntities(collection: String!, query: Dictionary<String, AnyObject>?, values: Dictionary<String, AnyObject>!)
        case deleteEntities(collection: String!, query: Dictionary<String, AnyObject>?)
        
        var method: Alamofire.Method {
            switch self {
            case .createEntity:
                return .POST
            case .getEntity:
                return .GET
            case .updateEntity:
                return .PUT
            case .deleteEntity:
                return .DELETE
            case .getEntities:
                return .GET
            case .updateEntities:
                return .PUT
            case .deleteEntities:
                return .DELETE
            }
        }
        
        var path: String {
            switch self {
            case .createEntity(_, let collection):
                return "/\(collection)"
            case .getEntity(let identifier, let collection):
                return "/\(collection)/\(identifier)"
            case .updateEntity(let identifier, _, let collection):
                return "/\(collection)/\(identifier)"
            case .deleteEntity(let identifier, let collection):
                return "/\(collection)/\(identifier)"
            case .getEntities(let collection, _):
                return "/\(collection)"
            case .updateEntities(let collection, _, _):
                return "/\(collection)"
            case .deleteEntities(let collection, _):
                return "/\(collection)"
            }
        }
        
        var query: String? {
            switch self {
            case .updateEntities(_, let query, _):
                return query?.toURLString()
            default:
                return nil
            }
        }
        
        // MARK: URLRequestConvertible
        
        public var URLRequest: NSMutableURLRequest {
            let urlComponent = NSURLComponents(string: Router.baseURL)!
            urlComponent.path = Router.applicationPath.stringByAppendingString(path)
            if query != nil && query != "" {
                urlComponent.query = query
            }
            
            let mutableURLRequest = NSMutableURLRequest(URL: urlComponent.URL!)
            mutableURLRequest.HTTPMethod = method.rawValue
            
            if let token = Router.accessToken {
                mutableURLRequest.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
            }
            
            switch self {
            case .createEntity(let entity, _):
                return Alamofire.ParameterEncoding.JSON.encode(mutableURLRequest, parameters: entity).0
            case .updateEntity(_, let values, _):
                return Alamofire.ParameterEncoding.JSON.encode(mutableURLRequest, parameters: values).0
            case .getEntities(_, let query):
                return Alamofire.ParameterEncoding.URL.encode(mutableURLRequest, parameters: query).0
            case .updateEntities(_, _, let values):
                return Alamofire.ParameterEncoding.JSON.encode(mutableURLRequest, parameters: values).0
            case .deleteEntities(_, let query):
                return Alamofire.ParameterEncoding.URL.encode(mutableURLRequest, parameters: query).0
            default:
                return mutableURLRequest
            }
        }
    }
    
}
