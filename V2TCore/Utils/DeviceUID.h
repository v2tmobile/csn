//
//  DeviceUID.h
//  ChiaSeNhac
//
//  Created by Vũ Trung Thành on 1/12/16.
//  Copyright © 2016 V2T Multimedia. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DeviceUID : NSObject

@property(nonatomic, strong, readonly) NSString *uidKey;
@property(nonatomic, strong, readonly) NSString *uid;

+ (NSString *)uid;

+ (NSMutableDictionary *)keychainItemForKey:(NSString *)key service:(NSString *)service;
+ (OSStatus)setValue:(NSString *)value forKeychainKey:(NSString *)key inService:(NSString *)service;
+ (NSString *)valueForKeychainKey:(NSString *)key service:(NSString *)service;

+ (BOOL)setValue:(NSString *)value forUserDefaultsKey:(NSString *)key;
+ (NSString *)valueForUserDefaultsKey:(NSString *)key;

+ (NSString *)appleIFA;
+ (NSString *)appleIFV;
+ (NSString *)randomUUID;

@end
